//
//  UITextField+Offset.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/8/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "UITextField+Offset.h"

@implementation UITextField (Offset)

- (void)addOffset {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

@end
