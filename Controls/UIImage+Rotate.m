//
//  UITextField+Offset.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/8/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "UIImage+Rotate.h"

@implementation UIImage (Offset)

- (UIImage*)rotateInAngle:(int)angle; {
    UIGraphicsBeginImageContext(self.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM( context, 0.5f * self.size.width, 0.5f * self.size.height ) ;
    CGContextRotateCTM (context, -(angle* M_PI / 180));
    
    [self drawInRect:(CGRect){ { -self.size.width * 0.5f, -self.size.height * 0.5f }, self.size } ] ;
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end

