//
//  UITextField+Offset.h
//  HomeHalo
//
//  Created by naumov.kirill on 1/8/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Offset)

- (UIImage*)rotateInAngle:(int) angle;

@end
