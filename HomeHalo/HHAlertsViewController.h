//
//  HHAlertsViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 2/6/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@interface HHAlertsViewController : HHBaseViewController

@property (nonatomic) BOOL returnToHome;

@end
