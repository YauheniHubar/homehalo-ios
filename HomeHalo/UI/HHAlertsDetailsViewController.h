//
//  HHAlertsDetailsViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 3/13/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@class HHNotification;

@interface HHAlertsDetailsViewController : HHBaseViewController

@property (nonatomic) HHNotification *notification;

@end
