//
//  HHMoreViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 1/13/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"
#import <MessageUI/MessageUI.h>

@interface HHMoreViewController : HHBaseViewController <MFMailComposeViewControllerDelegate>

@end
