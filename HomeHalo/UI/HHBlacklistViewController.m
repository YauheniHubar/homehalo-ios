//
//  HHBlacklistViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 2/3/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import <AFNetworking/UIKit+AFNetworking.h>
#import "HHBlacklistViewController.h"
#import "UITextField+Offset.h"
#import "MGSwipeTableCell.h"
#import "MGSwipeButton.h"
#import "HHAPI.h"

static NSString *const kBlacklistCellIdentifier = @"kBlacklistCellIdentifier";
static NSString *const kPopularWebsiteCellIdentifier = @"kPopularWebsiteCellIdentifier";

@interface HHBlacklistViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, MGSwipeTableCellDelegate> {
    BOOL keyboardShow;
    NSInteger prevConstraint;
    NSIndexPath *firstResponderIndexPath;
    BOOL isEditing;
    BOOL isPopularView;
}

@property (nonatomic) NSMutableArray *blackListUrlArray;
@property (nonatomic) NSMutableArray *cellStateArray;

@property (nonatomic, weak) IBOutlet UILabel *navigationLabel;
@property (nonatomic, weak) IBOutlet UITextField *addNewUrlTextField;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UITableView *popularTableView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableBottomOffset;

@property (nonatomic, weak) IBOutlet UIView *popularWebsitesView;
@property (nonatomic, weak) IBOutlet UIView *manualBlacklistView;

@property (nonatomic, weak) IBOutlet UIButton *sendButton;
@property (nonatomic) IBOutletCollection(UIButton) NSArray *barButtons;

@property (nonatomic) NSArray *categoriesArray;
@property (nonatomic) NSMutableArray *selectedCategory;
@property (nonatomic) UIView *separatorView;

@end

@implementation HHBlacklistViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    isEditing = NO;
    keyboardShow = NO;
    
    self.sendButton.hidden = YES;
    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoBlacklistScreen") : LS(@"BlacklistScreen");
    
    self.navigationLabel.text = self.userSettings.currentDeviceOwner[@"title"];
    self.blackListUrlArray = [NSMutableArray arrayWithArray:self.userSettings.currentDeviceOwner[@"urlBlacklists"]];
    
    self.cellStateArray = [NSMutableArray new];
    [self.addNewUrlTextField addOffset];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    self.tableView.allowsSelectionDuringEditing = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeDidShowKeyboard:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestGetBlacklistCategoryWithSuccess:^(NSDictionary *responseObject) {

        weakSelf.categoriesArray = responseObject[@"categories"];
        weakSelf.selectedCategory = [NSMutableArray new];

        for (NSDictionary *dict in weakSelf.categoriesArray) {
            NSArray *blacklistedCategories = weakSelf.userSettings.currentDeviceOwner[@"blacklistedCategories"];

            for (NSDictionary *obj in blacklistedCategories) {
                if ([dict[@"id"] integerValue] == [obj[@"contentCategory"] integerValue]) {
                    [weakSelf.selectedCategory addObject:dict];
                }
            }
        }

        [weakSelf.popularTableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    isPopularView = YES;
}

- (void)noticeDidShowKeyboard:(NSNotification *)inNotification {
    CGRect keyboardRect = [inNotification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    prevConstraint = self.tableBottomOffset.constant;
    self.tableBottomOffset.constant = keyboardRect.size.height;
    
    CGFloat offset = firstResponderIndexPath.row * 44.f;

    if (offset > (self.tableView.contentOffset.y + keyboardRect.size.height))
        offset = self.tableView.contentOffset.y + keyboardRect.size.height;

    [self.tableView setContentOffset:CGPointMake(0, offset) animated:YES];
}

- (void)noticeWillHideKeyboard:(NSNotification *)inNotification
{
    self.tableBottomOffset.constant = prevConstraint;
}

#pragma mark -
#pragma mark Actions

- (IBAction)barBtnTouch:(id)sender
{
    UIButton *btn = sender;
    for (UIButton *button in self.barButtons) {
        if (button.tag == btn.tag) {
            button.backgroundColor = RGB(110, 96, 91);
            
            [self.separatorView removeFromSuperview];
            self.separatorView = nil;
            if (button.tag != 0) {
                self.separatorView = [[UIView alloc] initWithFrame:CGRectMake(button.frame.origin.x,
                                                                         button.frame.origin.y + button.frame.size.height,
                                                                         button.frame.size.width, 10)];
                self.separatorView.backgroundColor = RGB(110, 96, 91);
                [self.view addSubview:self.separatorView];
                [self.view sendSubviewToBack:self.separatorView];
                self.popularWebsitesView.hidden = YES;
                self.manualBlacklistView.hidden = NO;
                isPopularView = NO;
                [self.tableView reloadData];
            }
            else {
                self.popularWebsitesView.hidden = NO;
                self.manualBlacklistView.hidden = YES;
                isPopularView = YES;
                [self.popularTableView reloadData];
            }
        } else {
            [button setBackgroundColor:RGB(130, 115, 110)];
        }
    }
}

- (IBAction)addNewUrlBtnTouch:(id)sender
{
    NSString *urlStr = self.addNewUrlTextField.text;
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestAddToBlacklist:urlStr success:^(NSDictionary *responseObject) {

        [weakSelf.blackListUrlArray addObject:responseObject[@"urlBlacklist"]];
        [weakSelf.tableView reloadData];
        weakSelf.addNewUrlTextField.text = @"";

        NSMutableDictionary *newCurrentDeviceOwner = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
        NSMutableArray *urlBL = [NSMutableArray new];
        [urlBL addObjectsFromArray:newCurrentDeviceOwner[@"urlBlacklists"]];
        [urlBL addObject:responseObject[@"urlBlacklist"]];
        [newCurrentDeviceOwner removeObjectForKey:@"urlBlacklists"];
        newCurrentDeviceOwner[@"urlBlacklists"] = urlBL;
        weakSelf.userSettings.currentDeviceOwner = newCurrentDeviceOwner;
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)updateUrl:(NSString *)newUrl urlID:(NSInteger)indexRow {
    isEditing = NO;
    NSString *urlStr = newUrl;
    NSDictionary *urlInfo = self.blackListUrlArray[indexRow];
    NSInteger urlID = [urlInfo[@"id"] integerValue];
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestUpdateBlacklist:urlStr urlID:urlID success:^(NSDictionary *responseObject) {

        weakSelf.cellStateArray[indexRow] = @YES;
        weakSelf.blackListUrlArray[indexRow] = responseObject[@"urlBlacklist"];
        [weakSelf.tableView reloadData];
        weakSelf.addNewUrlTextField.text = @"";

        NSMutableDictionary *newCurrentDeviceOwner = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
        NSMutableArray *urlBL = [NSMutableArray new];
        [urlBL addObjectsFromArray:newCurrentDeviceOwner[@"urlBlacklists"]];

        for (NSInteger i = 0; i < urlBL.count; i++) {
            NSDictionary *dict = urlBL[i];

            if ([dict[@"id"] integerValue] == [responseObject[@"urlBlacklist"][@"id"] integerValue]) {
                urlBL[i] = responseObject[@"urlBlacklist"];

                break;
            }
        }

        [newCurrentDeviceOwner removeObjectForKey:@"urlBlacklists"];
        newCurrentDeviceOwner[@"urlBlacklists"] = urlBL;
        weakSelf.userSettings.currentDeviceOwner = newCurrentDeviceOwner;
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)deleteUrl:(NSInteger)indexRow {
    NSDictionary *urlInfo = self.blackListUrlArray[indexRow];
    NSInteger urlID = [urlInfo[@"id"] integerValue];
    
    __weak __typeof(self) weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestDeleteFromBlacklist:urlID success:^(NSDictionary *responseObject) {

        [weakSelf.blackListUrlArray removeObjectAtIndex:indexRow];
        [weakSelf.tableView reloadData];

        NSMutableDictionary *newCurrentDeviceOwner = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
        NSMutableArray *urlBL = [NSMutableArray new];
        [urlBL addObjectsFromArray:newCurrentDeviceOwner[@"urlBlacklists"]];
        
        for (NSInteger i = 0; i < urlBL.count; i++) {
            NSDictionary *dict = urlBL[i];
            
            if ([dict[@"id"] integerValue] == urlID) {
                [urlBL removeObjectAtIndex:i];
                
                break;
            }
        }
        
        [newCurrentDeviceOwner removeObjectForKey:@"urlBlacklists"];
        newCurrentDeviceOwner[@"urlBlacklists"] = urlBL;
        weakSelf.userSettings.currentDeviceOwner = newCurrentDeviceOwner;
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isPopularView)
        return self.categoriesArray.count;

    if (self.cellStateArray.count != self.blackListUrlArray.count) {
        self.cellStateArray = [NSMutableArray new];

        for (int i = 0; i < self.blackListUrlArray.count; i++) {
            [self.cellStateArray addObject:@YES];
        }
    }

    return self.blackListUrlArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isPopularView) {
        CGFloat cellHeight = self.popularTableView.frame.size.height/self.categoriesArray.count;
        if (cellHeight > 44.f)
            return cellHeight;
        else {
            [self.popularTableView setScrollEnabled:YES];
            return 44.f;
        }
    }
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isPopularView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPopularWebsiteCellIdentifier];
        
        NSDictionary *dict = self.categoriesArray[indexPath.row];
        
        UIImageView *arrow = [cell.contentView viewWithTag:1];
        NSString *str = [NSString stringWithFormat:@"%@%@", kBaseURL, dict[@"icon"]];
        [arrow setImageWithURL:[NSURL URLWithString:str]];
        
        UILabel *title = [cell.contentView viewWithTag:2];
        title.text = dict[@"title"];
        
        UISwitch *switchButton = [cell.contentView viewWithTag:3];
        switchButton.backgroundColor = [UIColor redColor];
        switchButton.layer.cornerRadius = 16.0;
        
        NSArray *blacklistedCategories = [HHUserSettings sharedSettings].currentDeviceOwner[@"blacklistedCategories"];
        
        for (NSDictionary *obj in blacklistedCategories) {
            if ([dict[@"id"] integerValue] == [obj[@"contentCategory"] integerValue]) {
                [switchButton setOn:NO];
                if ([obj[@"isParentBlocked"] boolValue]) {
                    switchButton.enabled = NO;
                }
            }
        }
        switchButton.accessibilityIdentifier = [NSString stringWithFormat:@"%d", (int)indexPath.row];
        [switchButton addTarget:self action:@selector(switchBtnTouch:) forControlEvents:UIControlEventValueChanged];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    MGSwipeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:kBlacklistCellIdentifier];
    
    NSDictionary *rowInfo = self.blackListUrlArray[indexPath.row];
    
    NSString *siteAdress = @"";
    siteAdress = rowInfo[@"url"];
    if ([rowInfo[@"url"] containsString:@"http://"] || [rowInfo[@"url"] containsString:@"https://"] || [rowInfo[@"url"] containsString:@"www."])
    {
        siteAdress = [siteAdress stringByReplacingOccurrencesOfString:@"https://" withString:@""];
        siteAdress = [siteAdress stringByReplacingOccurrencesOfString:@"http://" withString:@""];
        siteAdress = [siteAdress stringByReplacingOccurrencesOfString:@"www." withString:@""];
    }
    
    UILabel *title = (UILabel *)[cell.contentView viewWithTag:1];
    title.text = siteAdress;
    
    UIView *view = (UIView *)[cell.contentView viewWithTag:2];
    view.hidden = [self.cellStateArray[indexPath.row] boolValue];
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    cell.rightExpansion.buttonIndex = 2;
    cell.rightExpansion.fillOnTrigger = YES;
    cell.rightButtons = [self createRightButtons];
    cell.delegate = self;
    
    UIImageView *arrow = (UIImageView *)[cell.contentView viewWithTag:10];
    arrow.image = [UIImage imageNamed:@"left_circle"];
    
    UITextField *editTextField = (UITextField *)[cell.contentView viewWithTag:3];
    [editTextField addOffset];
    editTextField.text = rowInfo[@"url"];
    
    if (![self.cellStateArray[indexPath.row] boolValue] && !keyboardShow)
    {
        keyboardShow = YES;
        editTextField.delegate = self;
        firstResponderIndexPath = indexPath;
        [editTextField becomeFirstResponder];
        [editTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
    }
    
    UIButton *okButton = (UIButton *)[cell.contentView viewWithTag:5];
    [okButton addTarget:self
                 action:@selector(okBtnTouch:)
       forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *cancelButton = (UIButton *)[cell.contentView viewWithTag:6];
    [cancelButton addTarget:self
                     action:@selector(cancelBtnTouch:)
           forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isPopularView) {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState) state gestureIsActive:(BOOL) gestureIsActive
{
    if (state == MGSwipeStateNone)
    {
        if (!keyboardShow)
            [self.tableView reloadData];
    }
}

-(BOOL)swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction
{
    NSNumber* row = nil;
    
    UIImageView *arrow = (UIImageView *)[cell.contentView viewWithTag:10];
    arrow.image = [UIImage imageNamed:@"rigth_circle"];
    for (int i = 0; i < self.cellStateArray.count; i++)
    {
        if (![self.cellStateArray[i] boolValue])
        {
            self.cellStateArray[i] = @YES;
            row = @(i);
        }
    }
    if (row != nil)
    {
        UITableViewCell* tableCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row.intValue inSection:0]];
        
        [self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:tableCell]] withRowAnimation:UITableViewRowAnimationNone];
    }
    return YES;
}

- (BOOL)swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion  {
    NSIndexPath * path = [_tableView indexPathForCell:cell];
    
    if ([self.cellStateArray[path.row] boolValue])
    {
        if (direction == MGSwipeDirectionRightToLeft && index == 0)
        {
            //delete
            isEditing = NO;
            [self deleteUrl:path.row];
        }
        else if (direction == MGSwipeDirectionRightToLeft && index == 1)
        {
            //edit
            isEditing = YES;
            self.cellStateArray[path.row] = @NO;
            [self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    
    return YES;
}


#pragma mark - Configure cell

-(NSArray *)createRightButtons
{
    NSMutableArray * result = [NSMutableArray array];
    NSString *titles[2] = {LS(@"Delete"), LS(@"Edit")};
    UIColor * colors[2] = {[UIColor redColor], [UIColor lightGrayColor]};
    for (int i = 0; i < 2; ++i)
    {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:titles[i] backgroundColor:colors[i] callback:^BOOL(MGSwipeTableCell * sender){
            return YES;
        }];
        button.tag = i;
        [result addObject:button];
    }
    return result;
}


#pragma mark - Actions

- (void)switchBtnTouch:(UISwitch *)sender {
    if (isPopularView) {
        if (!sender.enabled)
            return;
        [self.sendButton setHidden:NO];
        NSDictionary *dict = self.categoriesArray[sender.accessibilityIdentifier.intValue];
        
        if (!sender.isOn) {
            [self.selectedCategory addObject:dict];
        }
        else {
            for (NSDictionary *obj in self.selectedCategory) {
                if ([dict[@"categoryId"] integerValue] == [obj[@"categoryId"] integerValue]) {
                    [self.selectedCategory removeObject:obj];
                    break;
                }
            }
        }
    }
}


- (IBAction)sendBtnTouch:(id)sender {
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestSendBlacklistCategories:self.selectedCategory success:^(NSDictionary *responseObject) {

        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:weakSelf.userSettings.currentDeviceOwner];
        [dict removeObjectForKey:@"blacklistedCategories"];
        dict[@"blacklistedCategories"] = responseObject[@"categories"];
        weakSelf.userSettings.currentDeviceOwner = dict;
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)okBtnTouch:(id)sender {
    NSInteger row = 0;

    for (int i = 0; i < self.cellStateArray.count; i++) {
        if (![self.cellStateArray[i] boolValue]) {
            row = i;
        }
    }

    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
    UITextField *editTextField = [cell.contentView viewWithTag:3];
    
    [self updateUrl:editTextField.text urlID:row];
}

- (void)cancelBtnTouch:(id)sender {
    NSUInteger row = 0;

    for (NSUInteger i = 0; i < self.cellStateArray.count; i++) {
        if (![self.cellStateArray[i] boolValue]) {
            row = i;
        }
    }

    self.cellStateArray[row] = @YES;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    if (isEditing && textField.tag != 0) {
        isEditing = NO;
        NSInteger row = 0;

        for (NSUInteger i = 0; i < self.cellStateArray.count; i++) {
            if (![self.cellStateArray[i] boolValue]) {
                row = i;
            }
        }

        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
        UITextField *editTextField = [cell.contentView viewWithTag:3];
        
        [self updateUrl:editTextField.text urlID:row];

        return YES;
    }

    [self addNewUrlBtnTouch:nil];

    return YES;
}

@end
