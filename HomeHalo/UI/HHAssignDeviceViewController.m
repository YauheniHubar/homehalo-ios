//
//  HHAssignDeviceViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 3/23/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHAssignDeviceViewController.h"
#import "HHAPI+Devices.h"
#import "HHAPI+Alerts.h"

static NSString *const kAssignDeviceCell = @"kAssignDeviceCell";

@interface HHAssignDeviceViewController () <UITableViewDelegate, UITableViewDataSource> {
    NSInteger selectedUser;
}

@property (nonatomic, weak) IBOutlet UILabel *deviceLabel;
@property (nonatomic, weak) IBOutlet UILabel *mfrLabel;
@property (nonatomic, weak) IBOutlet UILabel *macLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation HHAssignDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = [HHUserSettings sharedSettings].isDemoMode ? LS(@"DemoAssignDeviceScreen") : LS(@"AssignDeviceScreen");

    selectedUser = 0;
    if (!self.userSettings.deviceOwnerArray || self.userSettings.deviceOwnerArray.count == 0) {
        __weak __typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestDeviceOwnersWithSuccess:^(NSDictionary *responseObject) {

            [weakSelf.tableView reloadData];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }
    
    selectedUser = [self.deviceInfo[@"deviceOwner"][@"id"] integerValue];
    
    self.deviceLabel.text = self.deviceInfo[@"title"];

    if ([[self.deviceInfo[@"manufacturerTitle"] class] isSubclassOfClass:NSNull.class]) {
        self.mfrLabel.text = [NSString stringWithFormat:@"mfr: n/a"];
    } else {
        NSString *manufacture = self.deviceInfo[@"manufacturerTitle"];
        NSString *manufacture2 = self.deviceInfo[@"manufacturer_title"];

        if (([manufacture isKindOfClass:NSNull.class] || manufacture.length == 0) &&
            ([manufacture2 isKindOfClass:NSNull.class] || manufacture2.length == 0)) {
            self.mfrLabel.text = [NSString stringWithFormat:@"mfr: n/a"];
        } else {
            self.mfrLabel.text = [NSString stringWithFormat:@"mfr: %@", manufacture?manufacture:manufacture2];
        }
    }

    self.macLabel.text = [NSString stringWithFormat:@"mac: %@", self.deviceInfo[@"macAddress"] ? self.deviceInfo[@"macAddress"] : self.deviceInfo[@"mac_address"]];
}


#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.userSettings.deviceOwnerArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAssignDeviceCell];
    NSDictionary *owner = self.userSettings.deviceOwnerArray[(NSUInteger) indexPath.row];
    UILabel *title = [cell.contentView viewWithTag:1];
    title.text = owner[@"title"];
    
    UIImageView *check = [cell.contentView viewWithTag:2];
    check.image = [UIImage imageNamed:@"check.png"];

    if ([owner[@"id"] integerValue] == selectedUser) {
        check.image = [UIImage imageNamed:@"check_selected"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *owner = self.userSettings.deviceOwnerArray[(NSUInteger) indexPath.row];
    selectedUser = [owner[@"id"] integerValue];
    [self.tableView reloadData];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Actions

- (IBAction)assignBtnTouch:(id)sender {
    if (selectedUser == 0) {
        [HHUtils alertErrorMessage:LS(@"PleaseSelectUser")];

        return;
    }

    [self updateDeviceRequest:self.deviceInfo];
}

- (void)updateDeviceRequest:(NSDictionary *)deviceInfo {
    __weak __typeof(self) weakSelf = self;
    NSMutableDictionary *requestData = [NSMutableDictionary new];
    requestData[@"id"] = deviceInfo[@"id"];
    requestData[@"title"] = deviceInfo[@"title"];
    requestData[@"isEnabled"] = [NSString stringWithFormat:@"%@", deviceInfo[@"isEnabled"]];
    requestData[@"isOnline"] = [NSString stringWithFormat:@"%@", deviceInfo[@"isOnline"]];
    requestData[@"isFavorite"] = [NSString stringWithFormat:@"%@", deviceInfo[@"isFavorite"]];

    if (!deviceInfo[@"macAddress"]) {
        requestData[@"macAddress"] = deviceInfo[@"mac_address"];
    } else {
        requestData[@"macAddress"] = deviceInfo[@"macAddress"];
    }

    requestData[@"deviceOwner"] = [NSString stringWithFormat:@"%ld", (long) selectedUser];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestUpdateDevice:requestData deviceID:[deviceInfo[@"id"] integerValue] success:^(NSDictionary *responseObject) {

        [weakSelf updateAlerts];
        [weakSelf.navigationController popViewControllerAnimated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)updateAlerts {
    if (self.alertData != nil && self.alertID != nil) {
        __weak __typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestUpdateAlerts:self.alertData notificationID:self.alertID.integerValue success:^(id response) {

            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }
}

@end
