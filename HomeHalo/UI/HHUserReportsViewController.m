//
//  HHUserReportsViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 4/4/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHUserReportsViewController.h"
#import "UUChart.h"
#import "HHAPI.h"

typedef NS_ENUM(unsigned int, SelectType) {
    SelectTypeUsage = 0,
    SelectTypeVisited,
    SelectTypeBlocked
};

static NSString *const kDay = @"86400";
static NSString *const k7day = @"604800";
static NSString *const k30d = @"2592000";
static NSString *const kUserReportsIdentifier = @"kUserReportsIdentifier";


@interface HHUserReportsViewController () <UUChartDataSource, UITableViewDelegate, UITableViewDataSource>
{
    SelectType type;
    NSIndexPath* selectIndex;
    UIView *separatorView;
    
    NSArray *usageDayX;
    NSArray *usageWeekX;
    NSArray *usageMonthX;
    NSArray *usageDayY;
    NSArray *usageWeekY;
    NSArray *usageMonthY;
    
    NSArray *visitedDay;
    NSArray *visitedWeek;
    NSArray *visitedMonth;
    NSArray *blockedDay;
    NSArray *blockedWeek;
    NSArray *blockedMonth;
}

@property (nonatomic, weak) IBOutlet UILabel *navigationLabel;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UILabel *noDataLabel;

@property(nonatomic, strong) NSString *currentConstant;

@property (nonatomic) IBOutletCollection(UIButton) NSArray *timeButtons;
@property (nonatomic) IBOutletCollection(UIButton) NSArray *barButtons;

@property (nonatomic, strong) NSMutableArray *xLableArray;
@property (nonatomic, strong) NSMutableArray *yValueArray;
@property (nonatomic, strong) NSArray *dataArray;

@property (nonatomic, strong) UUChart *chartView;

@end

@implementation HHUserReportsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoUserReportsScreen");
    } else {
        self.screenName = LS(@"UserReportsScreen");
    }

    selectIndex = nil;
    
    self.currentConstant = kDay;
    
    self.navigationLabel.text = [HHUserSettings sharedSettings].currentDeviceOwner[@"title"];

    [self.chartView setHidden:YES];
    [self.noDataLabel setHidden:NO];
    [self.tableView setHidden:NO];
    
    type = SelectTypeVisited;
    
    [self timeBtnTouch:nil];
    [self.timeButtons[0] setBackgroundColor:[UIColor whiteColor]];
    
    [self.barButtons[1] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.barButtons[1] setBackgroundColor:RGB(44, 130, 218)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *button = self.barButtons[1];
    
    separatorView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3,
                                                             button.frame.origin.y + button.frame.size.height,
                                                             self.view.frame.size.width/3, 10)];
    [separatorView setBackgroundColor:RGB(44, 130, 218)];
    [self.view addSubview:separatorView];
    [self.view sendSubviewToBack:separatorView];
}

- (IBAction)barBtnTouch:(id)sender
{
    self.noDataLabel.text = LS(@"NoDataToDisplay");
    selectIndex = nil;
    UIButton *btn = sender;
    for (UIButton *button in self.barButtons) {
        if (button.tag == btn.tag)
        {
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button setBackgroundColor:RGB(44, 130, 218)];
            
            [separatorView removeFromSuperview];
            separatorView = nil;
            separatorView = [[UIView alloc] initWithFrame:CGRectMake(button.frame.origin.x,
                                                   button.frame.origin.y + button.frame.size.height,
                                                   button.frame.size.width, 10)];
            [separatorView setBackgroundColor:RGB(44, 130, 218)];
            [self.view addSubview:separatorView];
            [self.view sendSubviewToBack:separatorView];
        }
        else
        {
            [button setTitleColor:RGB(44, 130, 218) forState:UIControlStateNormal];
            [button setBackgroundColor:RGB(162, 219, 252)];
        }
    }
    
    switch (btn.tag) {
        case 0:
        {
            [self.chartView setHidden:NO];
            [self.tableView setHidden:YES];
            [self.noDataLabel setHidden:YES];
            type = SelectTypeUsage;
            
            [self timeBtnTouch:nil];
            self.currentConstant = kDay;
            [self.timeButtons[0] setBackgroundColor:[UIColor whiteColor]];
            [self getInfoForOwnerWithTime:self.currentConstant];
            break;
        }
        case 1:
        {
            [self.chartView setHidden:YES];
            [self.tableView setHidden:NO];
            type = SelectTypeVisited;
            
            [self timeBtnTouch:nil];
            [self.timeButtons[0] setBackgroundColor:[UIColor whiteColor]];
            break;
        }
        case 2:
        {
            [self.chartView setHidden:YES];
            [self.tableView setHidden:NO];
            type = SelectTypeBlocked;
            
            [self timeBtnTouch:nil];
            [self.timeButtons[0] setBackgroundColor:[UIColor whiteColor]];
            break;
        }
    }
}

- (IBAction)timeBtnTouch:(id)sender
{
    UIButton *btn = sender;
    for (UIButton *button in self.timeButtons) {
        if (button.tag == btn.tag)
        {
            button.backgroundColor = [UIColor whiteColor];
        }
        else
        {
            [button setBackgroundColor:RGB(175, 225, 255)];
        }
    }
    
    PeriodType period = PeriodTypeDay;
    switch (btn.tag) {
        case 5:
            self.currentConstant = kDay;
            period = PeriodTypeDay;
            break;
        case 6:
            self.currentConstant = k7day;
            period = PeriodTypeWeek;
            break;
        case 7:
            self.currentConstant = k30d;
            period = PeriodTypeMonth;
            break;
    }
    
    if (type != SelectTypeUsage)
        [self getBrowsingTrafficsInfo:period status:type == SelectTypeVisited ? StatusTypeNotBlocked : StatusTypeCatBlock];
    else
        [self getInfoForOwnerWithTime:self.currentConstant];
}

- (void)checkTableViewData
{
    if (self.dataArray.count == 0)
    {
        [self.noDataLabel setHidden:NO];
        [self.tableView setHidden:YES];
    }
    else
    {
        [self.noDataLabel setHidden:YES];
        [self.tableView setHidden:NO];
        [self.tableView reloadData];
    }
}

- (void)getBrowsingTrafficsInfo:(PeriodType)period status:(StatusType)status
{
//    if (status != StatusTypeNotBlocked && ![[HHBaseWebServiceClient instance] isDemoMod])
//    {
//        [self.tableView setHidden:YES];
//        [self.noDataLabel setHidden:NO];
//        self.noDataLabel.text = LS(@"ComingEarly2016");
//        return;
//    }
    NSString *statusStr = status == StatusTypeNotBlocked ? @"0" : @"1,2";
    __weak __typeof(self) weakSelf = self;
    __weak __typeof(NSString*) weakStatusStr = statusStr;
    __weak __typeof(NSNumber*) weakPeriod = [NSNumber numberWithInt:period];
    
    
    switch (period) {
        case 0:
        {
            if ([statusStr isEqualToString:@"0"] && visitedDay)
            {
                self.dataArray = visitedDay;
                [self checkTableViewData];
                return;
            }
            else if (blockedDay)
            {
                self.dataArray = blockedDay;
                [self checkTableViewData];
                return;
            }
            break;
        }
        case 1:
        {
            if ([statusStr isEqualToString:@"0"] && visitedWeek)
            {
                self.dataArray = visitedWeek;
                [self checkTableViewData];
                return;
            }
            else if (blockedWeek)
            {
                self.dataArray = blockedWeek;
                [self checkTableViewData];
                return;
            }
            break;
        }
        case 2:
        {
            if ([statusStr isEqualToString:@"0"] && visitedMonth)
            {
                self.dataArray = visitedMonth;
                [self checkTableViewData];
                return;
            }
            else if (blockedMonth)
            {
                self.dataArray = blockedMonth;
                [self checkTableViewData];
                return;
            }
            break;
        }
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestGetTraffics:period status:statusStr success:^(NSDictionary *responseObject) {

        NSMutableArray *responseArray = [NSMutableArray new];

        for (NSDictionary *dict in responseObject[@"results"]) {
            if (dict[@"url"]) {
                [responseArray addObject:dict];
            }
        }

        weakSelf.dataArray = responseArray;

        if (weakSelf.dataArray.count == 0) {
            weakSelf.noDataLabel.hidden = NO;
            weakSelf.tableView.hidden = YES;
        } else {
            weakSelf.noDataLabel.hidden = YES;
            weakSelf.tableView.hidden = NO;
            [weakSelf.tableView reloadData];
        }


        switch (weakPeriod.intValue) {
            case 0: {
                if ([weakStatusStr isEqualToString:@"0"]) {
                    visitedDay = responseArray;
                } else {
                    blockedDay = responseArray;
                }

                break;
            } case 1: {
                if ([weakStatusStr isEqualToString:@"0"]) {
                    visitedWeek = responseArray;
                } else {
                    blockedWeek = responseArray;
                }

                break;
            } case 2: {
                if ([weakStatusStr isEqualToString:@"0"]) {
                    visitedMonth = responseArray;
                } else {
                    blockedMonth = responseArray;
                }

                break;
            }
        }

        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)getInfoForOwnerWithTime:(NSString*)time
{
//    if (![[HHBaseWebServiceClient instance] isDemoMod])
//    {
//        [self.chartView setHidden:YES];
//        [self.noDataLabel setHidden:NO];
//        self.noDataLabel.text = LS(@"ComingEarly2016");
//        return;
//    }
    
    [self.chartView removeFromSuperview];
    if (time == kDay && usageDayX)
    {
        self.xLableArray = [NSMutableArray arrayWithArray:usageDayX];
        self.yValueArray = [NSMutableArray arrayWithArray:usageDayY];
        self.chartView = [[UUChart alloc] initwithUUChartDataFrame:CGRectMake(10, 150, [UIScreen mainScreen].bounds.size.width-20, [UIScreen mainScreen].bounds.size.height-190)
                                                            withSource:self
                                                             withStyle:UUChartLineStyle];
        [self.chartView showInView:self.view];
        return;
    }
    else if (time == k7day && usageWeekX)
    {
        self.xLableArray = [NSMutableArray arrayWithArray:usageWeekX];
        self.yValueArray = [NSMutableArray arrayWithArray:usageWeekY];
        self.chartView = [[UUChart alloc] initwithUUChartDataFrame:CGRectMake(10, 150, [UIScreen mainScreen].bounds.size.width-20, [UIScreen mainScreen].bounds.size.height-190)
                                                        withSource:self
                                                         withStyle:UUChartLineStyle];
        [self.chartView showInView:self.view];
        return;
    }
    else if (time == k30d && usageMonthX)
    {
        self.xLableArray = [NSMutableArray arrayWithArray:usageMonthX];
        self.yValueArray = [NSMutableArray arrayWithArray:usageMonthY];
        self.chartView = [[UUChart alloc] initwithUUChartDataFrame:CGRectMake(10, 150, [UIScreen mainScreen].bounds.size.width-20, [UIScreen mainScreen].bounds.size.height-190)
                                                        withSource:self
                                                         withStyle:UUChartLineStyle];
        [self.chartView showInView:self.view];
        return;
    }
    
    __weak __typeof(self) weakSelf = self;
    __weak __typeof(NSString*) weakTime = time;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestGetBrowsingHistoryForOwner:time success:^(NSDictionary *responseObject) {

        weakSelf.xLableArray = [NSMutableArray new];
        weakSelf.yValueArray = [NSMutableArray new];
        NSArray *allObjects = responseObject[@"traffic"];

        for (NSInteger i = allObjects.count; i > 0 ; i--) {
            NSDictionary *dict = allObjects[i-1];
            NSDate *dateFromString = [[HHUtils dateFormatter] dateFromString:dict[@"period"][@"start"][@"dateTime"]];

            NSString *newStr = [[HHUtils getDateStringForReports:weakTime] stringFromDate:dateFromString];
            [weakSelf.xLableArray addObject:newStr];
            [weakSelf.yValueArray addObject:[dict[@"traffic"] stringValue]];
        }

        [weakSelf.chartView removeFromSuperview];
        weakSelf.chartView = [[UUChart alloc] initwithUUChartDataFrame:CGRectMake(10, 150, [UIScreen mainScreen].bounds.size.width-20, [UIScreen mainScreen].bounds.size.height-190)
                                                            withSource:weakSelf
                                                             withStyle:UUChartLineStyle];
        [weakSelf.chartView showInView:weakSelf.view];

        if (weakTime == kDay) {
            usageDayX = [NSArray arrayWithArray:weakSelf.xLableArray];
            usageDayY = [NSArray arrayWithArray:weakSelf.yValueArray];
        } else if (weakTime == k7day) {
            usageWeekX = [NSArray arrayWithArray:weakSelf.xLableArray];
            usageWeekY = [NSArray arrayWithArray:weakSelf.yValueArray];
        } else if (weakTime == k30d) {
            usageMonthX = [NSArray arrayWithArray:weakSelf.xLableArray];
            usageMonthY = [NSArray arrayWithArray:weakSelf.yValueArray];
        }

        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark - @required

- (NSArray *)UUChart_xLableArray:(UUChart *)chart
{
    if (self.xLableArray.count > 10)
    {
        for (NSInteger i = 0; i < self.xLableArray.count; i++) {
            if (i % 3 != 0) {
                self.xLableArray[i] = @"";
            }
        }
    }
    return self.xLableArray;
}

- (NSArray *)UUChart_yValueArray:(UUChart *)chart
{
    return @[self.yValueArray];
}

#pragma mark - @optional

- (NSArray *)UUChart_ColorArray:(UUChart *)chart
{
    return @[UULightBlue,UURed,UUBrown];
}

- (CGRange)UUChartChooseRangeInLineChart:(UUChart *)chart
{
    return CGRangeZero;
}

#pragma mark UUChartDelegate

- (CGRange)UUChartMarkRangeInLineChart:(UUChart *)chart
{
    return CGRangeZero;
}

- (BOOL)UUChart:(UUChart *)chart ShowHorizonLineAtIndex:(NSInteger)index
{
    return NO;
}

- (BOOL)UUChart:(UUChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return 0;
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    if(selectIndex.row == indexPath.row && selectIndex != nil)
        return 288.f;
    return 44.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.dataArray)
        return 0;
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kUserReportsIdentifier];
    
    NSDictionary *rowInfo = self.dataArray[indexPath.row];
    UILabel *title = (UILabel *)[cell.contentView viewWithTag:1];
    [title setHidden:NO];
    title.text = rowInfo[@"url"];
    
    UILabel *trafficLabel = (UILabel *)[cell.contentView viewWithTag:2];
    
    if (![HHUserSettings sharedSettings].isDemoMode)
        [trafficLabel setHidden:YES];
    
    int traffic = [rowInfo[@"traffic"] intValue];
    NSString *text = @"";
    if (traffic < 1024)
        text = [NSString stringWithFormat:@"%d B", traffic];
    else if (traffic > 1024 && traffic < 1024*1024)
        text = [NSString stringWithFormat:@"%.2f KB", (float)traffic/1024];
    else if (traffic > 1024*1024)
        text = [NSString stringWithFormat:@"%.2f MB", (float)traffic/(1024*1024)];
    trafficLabel.text = text;
    
    
    UIView *firstView = (UIView *)[cell.contentView viewWithTag:9];
    UIView *secondView = (UIView *)[cell.contentView viewWithTag:10];
    
    UILabel *title2 = (UILabel *)[cell.contentView viewWithTag:3];
    [title2 setHidden:YES];
    
    UILabel *categoryLabel = (UILabel *)[cell.contentView viewWithTag:4];
    [categoryLabel setHidden:YES];
    
    if (selectIndex.row == indexPath.row && selectIndex != nil)
    {
        [firstView setBackgroundColor:RGB(44, 130, 218)];
        [title setHidden:YES];
        [secondView setHidden:NO];
        title2.text = rowInfo[@"url"];
        [title2 setHidden:NO];
        categoryLabel.text = [rowInfo[@"categories"] firstObject][@"title"];
        [categoryLabel setHidden:NO];
        trafficLabel.textColor = [UIColor whiteColor];
        
        UIWebView *webView = (UIWebView *)[cell.contentView viewWithTag:11];
        [webView loadHTMLString:@"" baseURL:nil];
        
        NSString *urlString = @"";
        if ([rowInfo[@"url"] containsString:@"http://"] || [rowInfo[@"url"] containsString:@"https://"])
            urlString = rowInfo[@"url"];
        else
            urlString = [NSString stringWithFormat:@"http://%@", rowInfo[@"url"]];
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [webView loadRequest:requestObj];
        
        
        UIButton *leftButton = (UIButton *)[cell.contentView viewWithTag:12];
        leftButton.accessibilityValue = [NSString stringWithFormat:@"%d", (int)indexPath.row];
        
        UIButton *rightButton = (UIButton *)[cell.contentView viewWithTag:13];
        rightButton.accessibilityValue = [NSString stringWithFormat:@"%d", (int)indexPath.row];
        
        if (type == SelectTypeVisited)
        {
            [leftButton setTitle:LS(@"BlockCategory") forState:UIControlStateNormal];
            [leftButton addTarget:self action:@selector(blockCategoryBtnTouch:) forControlEvents:UIControlEventTouchUpInside];
            
            [rightButton setTitle:LS(@"BlacklistWebsite") forState:UIControlStateNormal];
            [rightButton addTarget:self action:@selector(blacklistBtnTouch:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            [leftButton setTitle:LS(@"AllowCategory") forState:UIControlStateNormal];
            [leftButton addTarget:self action:@selector(allowCategoryBtnTouch:) forControlEvents:UIControlEventTouchUpInside];
            
            [rightButton setTitle:LS(@"WhitelistWebsite") forState:UIControlStateNormal];
            [rightButton addTarget:self action:@selector(whitelistBtnTouch:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else
    {
        [firstView setBackgroundColor:RGB(236, 235, 233)];
        [secondView setHidden:YES];
        title.textColor = [UIColor blackColor];
        trafficLabel.textColor = [UIColor blackColor];
        
//        UIWebView *webView = (UIWebView *)[cell.contentView viewWithTag:11];
//        [webView loadHTMLString:@"" baseURL:nil];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath* prevIndex = selectIndex;
    if (indexPath.row == selectIndex.row && selectIndex != nil)
        selectIndex = nil;
    else
        selectIndex = indexPath;
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    if (prevIndex)
        [self.tableView reloadRowsAtIndexPaths:@[prevIndex] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionNone
                                  animated:YES];
}

#pragma mark -
#pragma mark - Actions

- (void)allowCategoryBtnTouch:(UIButton *)btn
{
    NSInteger row = btn.accessibilityValue.integerValue;
    [self sendAddCategory:row remove:NO];
}

- (void)whitelistBtnTouch:(UIButton *)btn {
    NSInteger row = btn.accessibilityValue.integerValue;
    NSString *urlStr = self.dataArray[row][@"url"];
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestAddUrl:urlStr success:^(NSDictionary *responseObject) {

        selectIndex = nil;
        NSMutableDictionary *currentDeviceOwner = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
        NSMutableArray *whitelist = [NSMutableArray arrayWithArray:currentDeviceOwner[@"urlWhitelists"]];
        [currentDeviceOwner removeObjectForKey:@"urlWhitelists"];
        [whitelist addObject:responseObject[@"urlWhitelist"]];
        currentDeviceOwner[@"urlWhitelists"] = whitelist;
        weakSelf.userSettings.currentDeviceOwner = currentDeviceOwner;
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)blockCategoryBtnTouch:(UIButton *)btn
{
    NSInteger row = btn.accessibilityValue.integerValue;
    [self sendAddCategory:row remove:YES];
}

- (void)blacklistBtnTouch:(UIButton *)btn
{
    NSInteger row = btn.accessibilityValue.integerValue;
    NSString *urlStr = self.dataArray[row][@"url"];
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestAddToBlacklist:urlStr success:^(NSDictionary *responseObject) {

        selectIndex = nil;
        NSMutableDictionary *currentDeviceOwner = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
        NSMutableArray *blacklist = [NSMutableArray arrayWithArray:currentDeviceOwner[@"urlBlacklists"]];
        [currentDeviceOwner removeObjectForKey:@"urlBlacklists"];
        [blacklist addObject:responseObject[@"urlBlacklist"]];
        currentDeviceOwner[@"urlBlacklists"] = blacklist;
        weakSelf.userSettings.currentDeviceOwner = currentDeviceOwner;
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)sendAddCategory:(NSInteger)row remove:(BOOL)remove
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:remove ? @"true" : @"false" forKey:@"remove"];
    NSMutableArray *idArray = [NSMutableArray new];
    for (NSDictionary *data in self.dataArray[row][@"categories"])
    {
        [idArray addObject:data[@"id"]];
    }
    [dict setValue:idArray forKey:@"categories"];
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestAddCategories:dict success:^(NSDictionary *responseObject) {

        selectIndex = nil;
        [weakSelf.tableView reloadData];
        NSString *title = remove ? LS(@"TheCategoryHasBeenRemoved") : LS(@"TheCategoryHasBeenAdded");
        [HHUtils alertWithTitle:title message:nil];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

@end
