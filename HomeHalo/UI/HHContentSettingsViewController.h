//
//  HHContentSettingsViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 1/20/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@interface HHContentSettingsViewController : HHBaseViewController

@property (nonatomic) NSString *titleContent;
@property (nonatomic) NSMutableSet* selectedCategory;
@property (nonatomic) NSDictionary *categoryInfo;

@end
