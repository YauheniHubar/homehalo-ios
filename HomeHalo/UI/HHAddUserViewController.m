//
//  HHAddUserViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 4/13/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHAddUserViewController.h"
#import "UITextField+Offset.h"
#import "HHTimeProfileOverviewViewController.h"
#import "HHAPI.h"

static NSString *const kContentCellIdentifier = @"kContentCellIdentifier";
static NSString *const kTimeProfileSegue = @"kTimeProfileSegue";

@interface HHAddUserViewController () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITextField *usernameTextField;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic) NSArray *contentProfileArray;
@property (nonatomic) NSMutableDictionary *currentContentProfile;

@end

@implementation HHAddUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoAddUserScreen") : LS(@"AddUserScreen");

    self.currentContentProfile = [NSMutableDictionary new];
    [self.usernameTextField addOffset];
    self.usernameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestContentProfileWithSuccess:^(NSDictionary *responseObject) {

        weakSelf.contentProfileArray = [NSArray arrayWithArray:responseObject[@"profiles"]];
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
    [self.usernameTextField becomeFirstResponder];
    [self.usernameTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
}


#pragma mark - Actions

- (IBAction)createBtnTouch:(id)sender {
    if (self.usernameTextField.text.length == 0) {
        [HHUtils alertWithTitle:LS(@"PleaseEnterYourUsername") message:nil];

        return;
    }
    
    for (NSDictionary *dict in self.userSettings.deviceOwnerArray) {
        NSString *title = dict[@"title"];

        if ([title.lowercaseString isEqualToString:self.usernameTextField.text.lowercaseString]) {
            [HHUtils alertWithTitle:[NSString stringWithFormat:@"%@ %@ %@", LS(@"User"), self.usernameTextField.text, LS(@"alreadyExists")] message:nil];

            return;
        }
    }

    if (!self.currentContentProfile[@"contentProfile"]) {
        [HHUtils alertWithTitle:LS(@"SelectContentProfile") message:nil];

        return;
    }
    
    NSDictionary *userDict = @{
                               @"status": @"resume",
                               @"title": self.usernameTextField.text,
                               @"contentProfile": self.currentContentProfile[@"contentProfile"]
                               };
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestAddUser:userDict success:^(NSDictionary *responseObject) {

        weakSelf.userSettings.currentDeviceOwner = responseObject[@"deviceOwner"];
        HHTimeProfileOverviewViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:kTimeProfileSegue];
        vc.isReturnToUsers = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}


#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contentProfileArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = self.tableView.frame.size.height/self.contentProfileArray.count;

    return (cellHeight > 44.f) ? cellHeight : 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kContentCellIdentifier];

    UILabel *titleLabel = [cell.contentView viewWithTag:1];
    titleLabel.text = self.contentProfileArray[indexPath.row][@"title"];

    if (indexPath.row == self.contentProfileArray.count - 1) {
        UIView *dividerView = [cell.contentView viewWithTag:2];
        dividerView.hidden = YES;
    }

    UIImageView *image = [cell.contentView viewWithTag:3];

    if ([self.contentProfileArray[indexPath.row][@"id"] integerValue] == [self.currentContentProfile[@"contentProfile"] integerValue]) {
        image.image = [UIImage imageNamed:@"content_dot_selected"];
    } else {
        image.image = [UIImage imageNamed:@"content_dot"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.currentContentProfile[@"contentProfile"] = self.contentProfileArray[indexPath.row][@"id"];
    [self.tableView reloadData];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    return YES;
}

@end
