//
//  HHDevicesViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 2/4/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHDevicesViewController.h"
#import "MGSwipeTableCell.h"
#import "MGSwipeButton.h"
#import "UITextField+Offset.h"
#import "HHAssignDeviceViewController.h"
#import "HHAPI+Devices.h"

static NSString *const kDeviceCell = @"kDeviceCell";
static NSString *const kAssignSegue = @"kAssignSegue";

@interface HHDevicesViewController () <UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate, UITextFieldDelegate>
{
    BOOL keyboardShow;
    NSInteger prevConstraint;
    NSIndexPath* firstResponderIndexPath;
    NSIndexPath* selectIndex;
}

@property (nonatomic) NSMutableArray *devices;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *noAssignedDeviceLabel;
@property (nonatomic) NSMutableArray *cellStateArray;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableBottomOffset;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *allDeviceHeight;
@property (nonatomic, weak) IBOutlet UIView *allDeviceView;
@property (nonatomic, weak) IBOutlet UILabel *navigationLabel;


@end

@implementation HHDevicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    prevConstraint = 0;

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoDevicesScreen");
    } else {
        self.screenName = LS(@"DevicesScreen");
    }

    keyboardShow = NO;
    selectIndex = nil;
    
    self.navigationLabel.text = [HHUserSettings sharedSettings].currentDeviceOwner[@"title"];
    self.cellStateArray = [NSMutableArray new];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    self.tableView.allowsSelectionDuringEditing = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeDidShowKeyboard:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestGetDevicesWithSuccess:^(NSDictionary *responseObject) {

        weakSelf.devices = [NSMutableArray arrayWithArray:responseObject[@"devices"]];
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
    
    if (![HHUserSettings sharedSettings].deviceOwnerArray || [HHUserSettings sharedSettings].deviceOwnerArray.count == 0) {
        __weak __typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestDeviceOwnersWithSuccess:^(NSDictionary *responseObject) {

            [weakSelf.tableView reloadData];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }
    
    [self.tableView reloadData];
    if (!self.isAllUsersShow)
    {
        [self.allDeviceView setHidden:YES];
        self.allDeviceHeight.constant = 0;
    }
}

- (void)noticeDidShowKeyboard:(NSNotification *)inNotification
{
    CGRect keyboardRect = [inNotification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    prevConstraint = self.tableBottomOffset.constant;
    self.tableBottomOffset.constant = keyboardRect.size.height;

    NSInteger offset = firstResponderIndexPath.row*86.f;
    if (selectIndex != nil && selectIndex.row < firstResponderIndexPath.row)
        offset += 61;
    if (offset > (self.tableView.contentOffset.y + keyboardRect.size.height))
        offset = self.tableView.contentOffset.y + keyboardRect.size.height;
    [self.tableView setContentOffset:CGPointMake(0, offset) animated:YES];
}

- (void)noticeWillHideKeyboard:(NSNotification *)inNotification
{
    self.tableBottomOffset.constant = prevConstraint;
    keyboardShow = NO;
}

- (void)updateDeviceRequest:(NSDictionary*)deviceInfo newTitle:(NSString*)title
{
    __weak __typeof(self) weakSelf = self;
    __weak __typeof(NSDictionary *) weakDeviceInfo = deviceInfo;
    NSMutableDictionary *requestData = [NSMutableDictionary new];
    requestData[@"id"] = deviceInfo[@"id"];
    requestData[@"title"] = title;
    requestData[@"isEnabled"] = deviceInfo[@"isEnabled"];
    requestData[@"isOnline"] = deviceInfo[@"isOnline"];
    requestData[@"isFavorite"] = deviceInfo[@"isFavorite"];
    requestData[@"macAddress"] = deviceInfo[@"macAddress"];
    requestData[@"createdAt"] = deviceInfo[@"createdAt"];
    requestData[@"deviceOwner"] = deviceInfo[@"deviceOwner"][@"id"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestUpdateDevice:requestData deviceID:[deviceInfo[@"id"] integerValue] success:^(NSDictionary *responseObject) {

        weakSelf.devices[[weakSelf.devices indexOfObject:weakDeviceInfo]] = responseObject[@"device"];
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    if(selectIndex.row == indexPath.row && selectIndex != nil)
        return 147.f;
    return 86.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.cellStateArray.count != self.devices.count)
    {
        self.cellStateArray = [NSMutableArray new];
        for (int i = 0; i < self.devices.count; i++) {
            [self.cellStateArray addObject:@YES];
        }
    }
    
    if (!self.isAllUsersShow)
    {
        NSMutableArray *deviceArray = [NSMutableArray new];
        for (NSDictionary *dict in self.devices)
        {
            if ([dict[@"deviceOwner"][@"id"] integerValue] == [[HHUserSettings sharedSettings].currentDeviceOwner[@"id"] integerValue])
                [deviceArray addObject:dict];
        }
        self.devices = nil;
        self.devices = [NSMutableArray arrayWithArray:deviceArray];
    }
    if (self.devices.count > 0)
        [self.noAssignedDeviceLabel setHidden:YES];
    else
        [self.noAssignedDeviceLabel setHidden:NO];
    return self.devices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MGSwipeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:kDeviceCell];
    
    NSDictionary *rowInfo = self.devices[indexPath.row];
    
    UIImageView *view = (UIImageView *)[cell.contentView viewWithTag:1];
    if ([rowInfo[@"isOnline"] boolValue])
        view.image = [UIImage imageNamed:@"ok_icon.png"];
    else
        view.image = [UIImage imageNamed:@"close_red_icon.png"];
    
    NSDictionary *deviceOwner;
    
    for (NSDictionary *owner in [HHUserSettings sharedSettings].deviceOwnerArray) {
        if ([rowInfo[@"deviceOwner"][@"id"] integerValue] == [owner[@"id"] integerValue]) {
            deviceOwner = owner;
            break;
        }
    }
    
    UILabel *title = (UILabel *)[cell.contentView viewWithTag:2];
    title.text = rowInfo[@"title"];
    UILabel *descr = (UILabel *)[cell.contentView viewWithTag:3];
    descr.text = deviceOwner[@"title"];
    UILabel *mac = (UILabel *)[cell.contentView viewWithTag:100];
    mac.text = [NSString stringWithFormat:@"MAC: %@", rowInfo[@"macAddress"] ? rowInfo[@"macAddress"] : @"n/a"];
    UILabel *mfr = (UILabel *)[cell.contentView viewWithTag:200];
    mfr.text = [NSString stringWithFormat:@"MFR: %@", rowInfo[@"manufacturerTitle"] ? rowInfo[@"manufacturerTitle"] : @"n/a"];
    UILabel *firstSeen = (UILabel *)[cell.contentView viewWithTag:300];
    NSDate* date = [[HHUtils dateFormatterUTC] dateFromString:deviceOwner[@"createdAt"][@"dateTime"]];
    firstSeen.text = [NSString stringWithFormat:@"First seen: %@", [self getFirstSeenString:date]];
    if (selectIndex.row == indexPath.row && selectIndex != nil)
    {
        [mac setHidden:NO];
        [mfr setHidden:NO];
        [firstSeen setHidden:NO];
    }
    else
    {
        [mac setHidden:YES];
        [mfr setHidden:YES];
        [firstSeen setHidden:YES];
    }
    
    UILabel *macAndMF = (UILabel *)[cell.contentView viewWithTag:22];
    macAndMF.text = LS(@"LastSeen:Never");
    
    if (rowInfo[@"lastOnlineAt"])
    {
        NSDate* dateFromString = [[HHUtils dateFormatter] dateFromString:rowInfo[@"lastOnlineAt"][@"dateTime"]];
        macAndMF.text = [NSString stringWithFormat:@"%@ %@", LS(@"LastSeen:"), [HHUtils getTimeAgo:dateFromString]];
    }
    UIImageView *arrow = (UIImageView *)[cell.contentView viewWithTag:4];
    arrow.image = [UIImage imageNamed:@"alert_left_button"];
    
    
    UIView *secondView = (UIView *)[cell.contentView viewWithTag:5];
    secondView.hidden = [self.cellStateArray[indexPath.row] boolValue];
    
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    cell.rightExpansion.buttonIndex = 2;
    cell.rightExpansion.fillOnTrigger = YES;
    cell.rightButtons = [self createRightButtons];
    cell.delegate = self;
    
    
    UITextField *editTextField = (UITextField *)[cell.contentView viewWithTag:6];
    [editTextField addOffset];
    editTextField.text = rowInfo[@"title"];
    
    if (![self.cellStateArray[indexPath.row] boolValue] && !keyboardShow)
    {
        keyboardShow = YES;
        editTextField.delegate = self;
        firstResponderIndexPath = indexPath;
        [editTextField becomeFirstResponder];
        [editTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
    }
    
    UIButton *okButton = (UIButton *)[cell.contentView viewWithTag:7];
    [okButton addTarget:self
                 action:@selector(okBtnTouch:)
       forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *cancelButton = (UIButton *)[cell.contentView viewWithTag:8];
    [cancelButton addTarget:self
                     action:@selector(cancelBtnTouch:)
           forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == selectIndex.row && selectIndex != nil)
        selectIndex = nil;
    else
        selectIndex = indexPath;
    [self.tableView beginUpdates];
    [self.tableView reloadData];
    [self.tableView endUpdates];
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction
{
    NSNumber* row = nil;
    
    UIImageView *arrow = (UIImageView *)[cell.contentView viewWithTag:4];
    arrow.image = [UIImage imageNamed:@"alert_right_button"];
    if (self.cellStateArray.count != self.devices.count)
    {
        self.cellStateArray = [NSMutableArray new];
        for (int i = 0; i < self.devices.count; i++) {
            [self.cellStateArray addObject:@YES];
        }
    }
    for (int i = 0; i < self.cellStateArray.count; i++)
    {
        if (![self.cellStateArray[i] boolValue])
        {
            self.cellStateArray[i] = @YES;
            row = @(i);
        }
    }
    if (row != nil)
    {
        UITableViewCell* tableCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row.intValue inSection:0]];
        
        [self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:tableCell]] withRowAnimation:UITableViewRowAnimationNone];
    }
    return YES;
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState) state gestureIsActive:(BOOL) gestureIsActive
{
    if (state == MGSwipeStateNone)
    {
        if (!keyboardShow)
            [self.tableView reloadData];
    }
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion {
    NSIndexPath * path = [_tableView indexPathForCell:cell];
    
    if ([self.cellStateArray[path.row] boolValue]) {
        if (direction == MGSwipeDirectionRightToLeft && index == 0) {
            //forget
            __weak __typeof(self) weakSelf = self;
            __weak __typeof(NSIndexPath*) weakPath = path;
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[HHAPI sharedAPI] requestDeleteDevice:[self.devices[path.row][@"id"] integerValue] success:^(NSDictionary *responseObject) {

                [weakSelf.devices removeObjectAtIndex:weakPath.row];
                [weakSelf.tableView reloadData];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            } failure:^(NSError *error) {

                [HHUtils alertErrorMessage:error.localizedDescription];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            }];
        } else if (direction == MGSwipeDirectionRightToLeft && index == 1) {
            //edit
            self.cellStateArray[path.row] = @NO;
            [self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationNone];
        } else if (direction == MGSwipeDirectionRightToLeft && index == 2) {
            //assign
            HHAssignDeviceViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:kAssignSegue];
            vc.deviceInfo = self.devices[path.row];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
    return YES;
}

#pragma mark -
#pragma mark - Configure cell

-(NSArray *) createRightButtons
{
    NSMutableArray * result = [NSMutableArray array];
    NSString *titles[3] = {LS(@"ForgetDevice"), LS(@"Edit"), LS(@"Assign")};
    UIColor * colors[3] = {[UIColor redColor], RGB(115, 180, 6), RGB(115, 180, 6)};
    for (int i = 0; i < 3; ++i)
    {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:titles[i] backgroundColor:colors[i] callback:^BOOL(MGSwipeTableCell * sender){
            return YES;
        }];
        button.tag = i;
        [result addObject:button];
    }
    return result;
}

#pragma mark -
#pragma mark - Actions

- (void)okBtnTouch:(id)sender
{
    NSInteger row = 0;
    for (int i = 0; i < self.cellStateArray.count; i++)
    {
        if (![self.cellStateArray[i] boolValue])
            row = i;
    }
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
    UITextField *editTextField = (UITextField *)[cell.contentView viewWithTag:6];
    
    NSDictionary *rowInfo = self.devices[row];
    NSString *title = rowInfo[@"title"];
    self.cellStateArray[row] = @YES;
    [self.tableView reloadData];
    if ([editTextField.text isEqualToString:@""])
    {
        [HHUtils alertErrorMessage:LS(@"DeviceNameMustBeMoreThan3Letters")];
    }
    else if (editTextField.text.length <= 2)
    {
        [HHUtils alertErrorMessage:LS(@"DeviceNameMustBeMoreThan3Letters")];
    }
    else if (editTextField.text.length > 100)
    {
        [HHUtils alertErrorMessage:LS(@"WeShouldJustStopTypingAt100Charectors")];
    }
    else if ([editTextField.text.lowercaseString isEqualToString:title.lowercaseString])
    {
        [self.tableView reloadData];
    }
    else
    {
        [self updateDeviceRequest:self.devices[row] newTitle:editTextField.text];
    }
}

- (void)cancelBtnTouch:(id)sender
{
    NSInteger row = 0;
    for (int i = 0; i < self.cellStateArray.count; i++)
    {
        if (![self.cellStateArray[i] boolValue])
            row = i;
    }
    self.cellStateArray[row] = @YES;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -
#pragma mark - UITextFieldDelegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark - Helpers method

- (NSString*)getFirstSeenString:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM.dd.yyyy";
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}

@end
