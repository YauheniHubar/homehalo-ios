//
//  HHContentViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 2/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHContentViewController.h"
#import "HHAPI+Devices.h"

static NSString *const kProfilesKey = @"profiles";
static NSString *const kContentCellIdentifier = @"kContentCellIdentifier";

@interface HHContentViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *contentProfileArray;
@property (nonatomic) NSMutableDictionary *currentContentProfile;

@end

@implementation HHContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoContentScreen");
    } else {
        self.screenName = LS(@"ContentScreen");
    }
    
    NSDictionary *data = [HHUserSettings sharedSettings].currentDeviceOwner;
    
    self.titleLabel.text = data[@"title"];
    
    self.currentContentProfile = [NSMutableDictionary new];
    
    [self.currentContentProfile setValue:data[@"title"] forKey:@"title"];
    [self.currentContentProfile setValue:data[@"id"] forKey:@"id"];
    [self.currentContentProfile setValue:data[@"status"] forKey:@"status"];
    [self.currentContentProfile setValue:data[@"createdAt"] forKey:@"createdAt"];
    
    NSMutableArray *timeProfilesArray = [NSMutableArray new];

    for (NSDictionary *timeData in data[@"timeProfiles"]) {
        NSMutableDictionary *obj = [NSMutableDictionary new];
        [obj setValue:timeData[@"dayOfWeek"] forKey:@"dayOfWeek"];
        [obj setValue:timeData[@"startTime"][@"time"] forKey:@"startTime"];
        [obj setValue:timeData[@"endTime"][@"time"] forKey:@"endTime"];
        [timeProfilesArray addObject:obj];
    }

    [self.currentContentProfile setValue:timeProfilesArray forKey:@"timeProfiles"];
    [self.currentContentProfile setValue:data[@"contentProfile"][@"id"] forKey:@"contentProfile"];
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestContentProfileWithSuccess:^(NSDictionary *responseObject) {

        weakSelf.contentProfileArray = [NSArray arrayWithArray:responseObject[kProfilesKey]];
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (IBAction)saveChanges:(id)sender {
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestBlockUser:self.currentContentProfile success:^(NSDictionary *responseObject) {

        [weakSelf.navigationController popViewControllerAnimated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestDeviceOwnersWithSuccess:^(NSDictionary *responseObject) {

        NSArray *array = responseObject[@"deviceOwners"];

        for (NSDictionary *dict in array) {
            if ([weakSelf.userSettings.currentDeviceOwner[@"id"] integerValue] == [dict[@"id"] integerValue]) {
                weakSelf.userSettings.currentDeviceOwner = dict;
            }
        }

        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.contentProfileArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = self.tableView.frame.size.height/self.contentProfileArray.count;
    if (cellHeight > 44.f)
        return cellHeight;
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kContentCellIdentifier];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    titleLabel.text = self.contentProfileArray[indexPath.row][@"title"];
    if (indexPath.row == self.contentProfileArray.count - 1)
    {
        UIView *dividerView = (UIView *)[cell.contentView viewWithTag:2];
        [dividerView setHidden:YES];
    }
    
    UIImageView *image = (UIImageView *)[cell.contentView viewWithTag:3];
    if ([self.contentProfileArray[indexPath.row][@"id"] integerValue] == [self.currentContentProfile[@"contentProfile"] integerValue])
        image.image = [UIImage imageNamed:@"content_dot_selected"];
    else
        image.image = [UIImage imageNamed:@"content_dot"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    (self.currentContentProfile)[@"contentProfile"] = self.contentProfileArray[indexPath.row][@"id"];
    [self.tableView reloadData];
}

@end
