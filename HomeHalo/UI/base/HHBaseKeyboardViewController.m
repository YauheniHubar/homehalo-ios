//
//  HHBaseKeyboardViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseKeyboardViewController.h"

@interface HHBaseKeyboardViewController ()  <UITextFieldDelegate>

@property (nonatomic) NSMutableArray *scrollViewTapRecognizers;

@property (nonatomic) CGRect startFrame;

@end

@implementation HHBaseKeyboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(noticeDidShowKeyboard:) name:UIKeyboardDidShowNotification object:nil];
    [center addObserver:self selector:@selector(noticeWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];

    self.startFrame = self.view.frame;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChange:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Private

- (void)keyboardWillChange:(NSNotification *)notification {
    self.view.frame = self.startFrame;
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat delta = self.view.superview.bounds.size.height - keyboardRect.origin.y;

    CGRect frame = self.view.frame;
    frame.size.height -= delta;
    self.view.frame = frame;
}

- (void)noticeDidShowKeyboard:(NSNotification *)inNotification {
    _keyboardVisible = YES;

    if(_firstResponder)
        [self performSelector:@selector(scrollToFirstResponder) withObject:nil afterDelay:0];

    [self addTapGestureRecognizer];
}

- (void)scrollToFirstResponder {
    if(_firstResponder)
        [_scrollView scrollRectToVisible:_firstResponder.frame animated:NO];
}

- (void)noticeWillHideKeyboard:(NSNotification *)inNotification {
    _keyboardVisible = NO;
    CGRect frame = self.view.frame;
    frame.size.height = self.view.superview.bounds.size.height - frame.origin.y;
    self.view.frame = frame;
    
    [self removeTapGestureRecognizer];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    UITextField *nextView = [self nextEnabledTextViewFor:textField];

    if (nextView != nil) {
        [nextView becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return NO;
}

- (UITextField *)nextEnabledTextViewFor:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIView *nextView = [textField.superview viewWithTag:nextTag];

    if (nextView != nil) {
        if(!nextView.hidden) {
            return (UITextField *)nextView;
        } else {
            return [self nextEnabledTextViewFor:(UITextField *)nextView];
        }
    } else {
        return nil;
    }
}

#pragma mark - Gesture Recognizer

- (void)addTapGestureRecognizer {
    if(_scrollView) {
        self.scrollViewTapRecognizers = [NSMutableArray new];

        for (UIView *view in _tapHandleViews) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
            [_scrollViewTapRecognizers addObject:recognizer];
            [view addGestureRecognizer:recognizer];
        }
    }
}

- (void)removeTapGestureRecognizer {
    if(_scrollViewTapRecognizers.count > 0) {
        for (UITapGestureRecognizer *recognizer in _scrollViewTapRecognizers) {
            [recognizer.view removeGestureRecognizer:recognizer];
        }
        
        self.scrollViewTapRecognizers = nil;
    }
}

- (void)tapHandler:(UIGestureRecognizer *)recognizer {
    for (UIView *view in _tapHandleViews) {
        [self resignFirstResponderAllSubViewsForView:view];
    }
    
    [self resignFirstResponderAllSubViewsForView:self.scrollView];
}

- (void)resignFirstResponderAllSubViewsForView:(UIView *)v {
    if (v.subviews.count > 0) {
        for (UIView *subView in v.subviews) {
            [subView resignFirstResponder];
            [self resignFirstResponderAllSubViewsForView:subView];
        }
    }
}

@end
