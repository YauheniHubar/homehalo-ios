//
//  HHBaseViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "GAITrackedViewController.h"
#import "HHUserSettings.h"

@interface HHBaseViewController : GAITrackedViewController

@property (nonatomic) HHUserSettings *userSettings;

- (void)logout;
- (void)goToPinScreen;
- (IBAction)backButtonTouch:(id)sender;
- (IBAction)homeButtonTouch:(id)sender;
- (void)unwindToViewControllerOfClass:(Class)vcClass animated:(BOOL)animated;

@end
