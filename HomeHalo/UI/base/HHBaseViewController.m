//
//  HHBaseViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseKeyboardViewController.h"
#import "HHHomeViewController.h"

static NSString *const kHomeSegue = @"kHomeSegue";

@interface HHBaseViewController () <UIGestureRecognizerDelegate>

@property (nonatomic) IBOutletCollection(UIButton) NSArray *disableButtonCollection;

@end

@implementation HHBaseViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    self.userSettings = [HHUserSettings sharedSettings];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


#pragma mark - Actions

- (IBAction)backButtonTouch:(id)sender {
    if ([self.navigationController isKindOfClass:HHHomeViewController.class]) {
        return;
    }

    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonTouch:(id)sender {
    HHBaseViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:kHomeSegue];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)logout {
    [[NSNotificationCenter defaultCenter] postNotificationName:HHLogoutNotification object:nil];
}

- (void)goToPinScreen {
    NSString *storyboardID = IS_IPHONE_4 ? @"HHEnterPinViewController4" : @"HHEnterPinViewController";
    HHBaseViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:storyboardID];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)unwindToViewControllerOfClass:(Class)vcClass animated:(BOOL)animated {
    for (NSInteger i=self.navigationController.viewControllers.count - 1; i >= 0; i--) {
        UIViewController *vc = (self.navigationController.viewControllers)[i];

        if ([vc isKindOfClass:vcClass]) {
            [self.navigationController popToViewController:vc animated:animated];

            return;
        }
    }
}

@end
