//
//  HHBaseKeyboardViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@interface HHBaseKeyboardViewController : HHBaseViewController

@property (nonatomic) BOOL keyboardVisible;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic) IBOutletCollection(UIView) NSArray *tapHandleViews;
@property (nonatomic, weak) UIView<UITextInput> *firstResponder;

- (void)scrollToFirstResponder;

@end
