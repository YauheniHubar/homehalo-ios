//
//  HHChangePinViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/8/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHChangePinViewController.h"

@interface HHChangePinViewController ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic) IBOutletCollection(UIButton) NSArray *pinIndicators;

@property (nonatomic) NSInteger pinCount;
@property (nonatomic) NSString *pinString;

@end

@implementation HHChangePinViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoChangePinScreen") : LS(@"ChangePinScreen");

    self.pinCount = 0;
    self.titleLabel.text = self.userSettings.correctPIN ? LS(@"EnterYourExistingPin") : LS(@"SetYourNew4DigitPin");
    self.navigationController.navigationBar.topItem.title = LS(@"Pin");
}


#pragma mark - Private

- (void)updatePinIndicators {
    for (NSUInteger i = 0; i < self.pinCount; i++) {
        [self.pinIndicators[i] setSelected:YES];
    }

    if (self.pinCount == 4) {
        if ([self.titleLabel.text isEqualToString:LS(@"SetYourNew4DigitPin")]) {
            self.userSettings.correctPIN = self.pinString;
            [self deleteBtnTouch:nil];
            self.titleLabel.text = LS(@"RepeatNewPin");

            return;
        } else if ([self.titleLabel.text isEqualToString:LS(@"RepeatNewPin")]) {
            NSString *correctPin = self.userSettings.correctPIN;

            if ([self.pinString isEqualToString:correctPin] || !correctPin) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [HHUtils alertErrorMessage:LS(@"PINIncorrect")];
                [self deleteBtnTouch:nil];
            }

            return;
        }
        
        NSString *correctPin = self.userSettings.correctPIN;

        if ([self.pinString isEqualToString:correctPin] || !correctPin) {
            self.userSettings.correctPIN = self.pinString;
            [self deleteBtnTouch:nil];
            self.titleLabel.text = LS(@"SetYourNew4DigitPin");
        } else {
            [HHUtils alertErrorMessage:LS(@"PINIncorrect")];
            [self deleteBtnTouch:nil];
        }
    }
}


#pragma mark - Actions

- (IBAction)deleteBtnTouch:(id)sender {
    self.pinString = nil;
    self.pinCount = 0;

    for (UIButton *btn in self.pinIndicators) {
        [btn setSelected:NO];
    }
}

- (IBAction)numberBtnTouch:(UIButton *)sender {
    if (self.pinCount == 4) {
        return;
    }

    self.pinString = [NSString stringWithFormat:@"%@%ld", self.pinString ? self.pinString : @"", (long) sender.tag];
    self.pinCount++;
    [self updatePinIndicators];
}

@end
