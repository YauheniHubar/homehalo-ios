//
//  HHForgotPasswordViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/8/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHForgotPasswordViewController.h"
#import "UITextField+Offset.h"
#import "HHAPI+Auth.h"

@interface HHForgotPasswordViewController ()

@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UIButton *resetBtn;
@property (nonatomic, weak) IBOutlet UIView *lastView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *constraint;

@end

@implementation HHForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoForgotPasswordScreen");
    }  else {
        self.screenName = LS(@"ForgotPasswordScreen");
    }
    
    [self.emailTextField addOffset];
    NSInteger constraintSize = (NSInteger) (self.view.frame.size.height - self.lastView.frame.origin.y + self.lastView.frame.size.height);
    constraintSize -= self.resetBtn.frame.size.height;
    self.constraint.constant = constraintSize;
}


#pragma mark - Actions

- (IBAction)resetBtnTouch:(id)sender {
    if (![HHUtils checkInternetConnection]) {
        [HHUtils alertErrorMessage:LS(@"NetworkErrorMessageNoInternetConnection")];

        return;
    }

    NSString *email = self.emailTextField.text;

    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestResetPasswordWithEmail:email success:^(id responseObject) {

        [HHUtils alertWithTitle:LS(@"PasswordSendToEmail") message:@""];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    return YES;
}

@end
