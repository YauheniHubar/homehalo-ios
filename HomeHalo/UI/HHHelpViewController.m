//
//  HHHelpViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/14/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHHelpViewController.h"
#import "UITextField+Offset.h"
#import "HHUser.h"
#import "HHAPI.h"

@interface HHHelpViewController () <UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UITextView* textView;
@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UIButton *sendMessageBtn;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end

@implementation HHHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoHelpScreen");
    } else {
        self.screenName = LS(@"HelpScreen");
    }

    [self.emailTextField addOffset];
    [self.textView setText:LS(@"DescribeYourProblem")];
    self.textView.textColor = [UIColor lightGrayColor];
    
    if (![HHUserSettings sharedSettings].isDemoMode)
        self.emailTextField.text = [HHUserSettings sharedSettings].currentUser.email;
    
    if ([HHUserSettings sharedSettings].isDemoMode)
        self.titleLabel.text = LS(@"FindOutMore");
    else
        self.titleLabel.text = LS(@"ContactUs");
}

#pragma mark -
#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:LS(@"DescribeYourProblem")]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = LS(@"DescribeYourProblem");
        textView.textColor = [UIColor lightGrayColor];
    }
    [textView resignFirstResponder];
}

#pragma mark -
#pragma mark - Actions

- (IBAction)sendMessageBtnTouch:(id)sender
{
    if (![HHUtils validateEmail:self.emailTextField.text])
    {
        [HHUtils alertErrorMessage:LS(@"EmailIsn'tValid")];
    }
    else
    {
        if (![HHUtils checkInternetConnection])
        {
            [HHUtils alertErrorMessage:LS(@"NetworkErrorMessageNoInternetConnection")];
            return;
        }
        __weak __typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestHelpWithEmail:self.emailTextField.text message:self.textView.text success:^(NSDictionary *responseObject) {

            [weakSelf.navigationController popViewControllerAnimated:YES];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }
}

#pragma mark -
#pragma mark - UITextFieldDelegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
