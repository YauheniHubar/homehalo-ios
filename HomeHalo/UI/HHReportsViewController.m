//
//  HHReportsViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 2/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHReportsViewController.h"
#import "XYPieChart.h"
#import "HHAPI+Devices.h"

static NSString *const kHour = @"3600";
static NSString *const kDay = @"86400";
static NSString *const k7day = @"604800";
static NSString *const k30d = @"2592000";
static NSString *const kReportsCell = @"kReportsCell";
static NSString *const kSegueIdToUserReportsScreen = @"goToUserReportsSegue";

@interface HHReportsViewController () <XYPieChartDelegate, XYPieChartDataSource, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) IBOutlet XYPieChart *pieChartRight;
@property(nonatomic, strong) NSMutableArray *slices;
@property(nonatomic, strong) NSArray *sliceColors;
@property(nonatomic, strong) NSMutableArray *useColors;
@property(nonatomic, strong) NSArray *dataArray;

@property(nonatomic, strong) NSString *currentConstant;

@property (nonatomic) IBOutletCollection(UIButton) NSArray *timeButtons;
@property (nonatomic) IBOutlet UITableView *tableView;

@end

@implementation HHReportsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.useColors = [NSMutableArray new];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoReportsScreen");
    } else {
        self.screenName = LS(@"ReportsScreen");
    }
    
    self.currentConstant = kHour;
    [self getInfoWithTime:self.currentConstant];
    
    (self.pieChartRight).delegate = self;
    (self.pieChartRight).dataSource = self;
    (self.pieChartRight).pieCenter = CGPointMake(self.view.frame.size.width/2, self.pieChartRight.frame.size.height/2);
    [self.pieChartRight setShowLabel:YES];
    [self.pieChartRight setShowPercentage:YES];
    (self.pieChartRight).labelColor = [UIColor whiteColor];

    self.sliceColors = @[
            [UIColor colorWithRed:103 / 255.0 green:58 / 255.0 blue:183 / 255.0 alpha:1],
            [UIColor colorWithRed:71 / 255.0 green:81 / 255.0 blue:181 / 255.0 alpha:1],
            [UIColor colorWithRed:33 / 255.0 green:150 / 255.0 blue:243 / 255.0 alpha:1],
            [UIColor colorWithRed:0 / 255.0 green:188 / 255.0 blue:212 / 255.0 alpha:1],
            [UIColor colorWithRed:0 / 255.0 green:150 / 255.0 blue:136 / 255.0 alpha:1],
            [UIColor colorWithRed:48 / 255.0 green:79 / 255.0 blue:254 / 255.0 alpha:1],
            [UIColor colorWithRed:98 / 255.0 green:0 / 255.0 blue:234 / 255.0 alpha:1],
            [UIColor colorWithRed:0 / 255.0 green:96 / 255.0 blue:100 / 255.0 alpha:1]
    ];
    
    [self.timeButtons[0] setSelected:YES];
    [self.timeButtons[0] setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.timeButtons[0] setBackgroundColor:RGB(0, 150, 225)];
}

- (void)viewDidUnload
{
    [self setPieChartRight:nil];
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.pieChartRight reloadData];
}

#pragma mark -
#pragma mark - XYPieChart

- (void)clearSlices {
    [_slices removeAllObjects];
    [self.pieChartRight reloadData];
}

- (void)updateSlices
{
    for(int i = 0; i < 2; i ++)
    {
        _slices[i] = @(rand()%60+20);
    }
    [self.pieChartRight reloadData];
}

#pragma mark - XYPieChart Data Source

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return self.slices.count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    return [(self.slices)[index] intValue];
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    return self.useColors[index];
}

- (NSString *)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index
{
    NSString *str = self.dataArray[index][@"deviceOwner"];
    return [str substringToIndex:3];
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kReportsCell];
    cell.backgroundColor = (self.sliceColors)[(indexPath.row % self.sliceColors.count)];
    
    UILabel *title = (UILabel *)[cell.contentView viewWithTag:1];
    title.text = self.dataArray[indexPath.row][@"deviceOwner"];
    
    UILabel *size = (UILabel *)[cell.contentView viewWithTag:2];
    int traffic = 0;
    if (self.slices.count > 0 && self.slices != nil)
        traffic = [self.slices[indexPath.row] intValue];
    NSString *text = @"";
    if (traffic < 1024)
        text = [NSString stringWithFormat:@"%d B", traffic];
    else if (traffic > 1024 && traffic < 1024*1024)
        text = [NSString stringWithFormat:@"%d KB", traffic/1024];
    else if (traffic > 1024*1024)
        text = [NSString stringWithFormat:@"%d MB", traffic/(1024*1024)];
    size.text = text;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![HHUserSettings sharedSettings].deviceOwnerArray || [HHUserSettings sharedSettings].deviceOwnerArray.count == 0) {
        __weak typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestDeviceOwnersWithSuccess:^(NSDictionary *responseObject) {

            for (NSDictionary *dict in responseObject[@"deviceOwners"]) {
                if ([dict[@"id"] integerValue] == [weakSelf.dataArray[indexPath.row][@"deviceOwnerId"] integerValue]) {
                    weakSelf.userSettings.currentDeviceOwner = dict;
                    [weakSelf performSegueWithIdentifier:kSegueIdToUserReportsScreen sender:nil];

                    break;
                }
            }

            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }
    else
    {
        for (NSDictionary *dict in [HHUserSettings sharedSettings].deviceOwnerArray) {
            if ([dict[@"id"] integerValue] == [self.dataArray[indexPath.row][@"deviceOwnerId"] integerValue])
            {
                [HHUserSettings sharedSettings].currentDeviceOwner = dict;
                [self performSegueWithIdentifier:kSegueIdToUserReportsScreen sender:nil];
                break;
            }
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark - Actions

- (void)getInfoWithTime:(NSString*)time
{
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestGetBrowsingHistory:time success:^(NSDictionary *responseObject) {

        weakSelf.slices = [NSMutableArray new];
        weakSelf.dataArray = [NSArray arrayWithArray:responseObject[@"traffics"]];

        for (NSDictionary *obj in weakSelf.dataArray) {
            NSInteger index = [weakSelf.dataArray indexOfObject:obj];
            [weakSelf.useColors addObject:weakSelf.sliceColors[index % weakSelf.sliceColors.count]];
            NSInteger value = 0;

            for (NSDictionary *dict in obj[@"traffic"]) {
                value += [dict[@"traffic"] integerValue];
            }

            [weakSelf.slices addObject:@(value)];
        }

        [weakSelf.pieChartRight reloadData];
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (IBAction)timeBtnTouch:(id)sender
{
    UIButton *btn = sender;
    for (UIButton *button in self.timeButtons) {
        if (button.tag == btn.tag)
        {
            [button setSelected:YES];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
            [button setBackgroundColor:RGB(0, 150, 225)];
        }
        else
        {
            [button setSelected:NO];
            [button setBackgroundColor:RGB(175, 225, 255)];
        }
    }
    
    switch (btn.tag) {
        case 0:
            self.currentConstant = kHour;
            break;
        case 1:
            self.currentConstant = kDay;
            break;
        case 2:
            self.currentConstant = k7day;
            break;
        case 3:
            self.currentConstant = k30d;
            break;
    }
    
    [self getInfoWithTime:self.currentConstant];
    
}

@end
