//
//  HHAlertsPreviewViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 3/17/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHAlertsPreviewViewController.h"

@interface HHAlertsPreviewViewController ()

@property (nonatomic, weak) IBOutlet UIWebView* webView;

@end

@implementation HHAlertsPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoAlertsPreviewScreen") : LS(@"AlertsPreviewScreen");
    
    NSURL *url = [NSURL URLWithString:self.url];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
}

@end
