//
//  HHExtendViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 1/27/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@interface HHExtendViewController : HHBaseViewController

@property (nonatomic) NSDictionary *alertData;
@property (nonatomic) NSNumber* alertID;

@end
