//
//  HHWhitelistViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 2/3/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHWhitelistViewController.h"
#import "UITextField+Offset.h"
#import "MGSwipeTableCell.h"
#import "MGSwipeButton.h"
#import "HHAPI.h"

static NSString *const kWhitelistCellIdentifier = @"kWhitelistCellIdentifier";

@interface HHWhitelistViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, MGSwipeTableCellDelegate>
{
    BOOL keyboardShow;
    NSInteger prevConstraint;
    NSIndexPath* firstResponderIndexPath;
    BOOL isEditing;
}

@property (nonatomic) NSMutableArray *cellStateArray;
@property (nonatomic) NSMutableArray *whiteListUrlArray;

@property (nonatomic, weak) IBOutlet UILabel *navigationLabel;
@property (nonatomic, weak) IBOutlet UITextField *addNewUrlTextField;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableBottomOffset;

@end

@implementation HHWhitelistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isEditing = NO;
    keyboardShow = NO;

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoWhitelistScreen");
    } else {
        self.screenName = LS(@"WhitelistScreen");
    }
    
    self.navigationLabel.text = [HHUserSettings sharedSettings].currentDeviceOwner[@"title"];
    self.cellStateArray = [NSMutableArray new];
    
    self.whiteListUrlArray = [NSMutableArray arrayWithArray:[HHUserSettings sharedSettings].currentDeviceOwner[@"urlWhitelists"]];
    [self.addNewUrlTextField addOffset];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    self.tableView.allowsSelectionDuringEditing = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeDidShowKeyboard:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)noticeDidShowKeyboard:(NSNotification *)inNotification
{
    CGRect keyboardRect = [inNotification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    prevConstraint = self.tableBottomOffset.constant;
    self.tableBottomOffset.constant = keyboardRect.size.height;
    
    NSInteger offset = firstResponderIndexPath.row*44.f;
    if (offset > (self.tableView.contentOffset.y + keyboardRect.size.height))
        offset = self.tableView.contentOffset.y + keyboardRect.size.height;
    [self.tableView setContentOffset:CGPointMake(0, offset) animated:YES];
}

- (void)noticeWillHideKeyboard:(NSNotification *)inNotification
{
    self.tableBottomOffset.constant = prevConstraint;
    keyboardShow = NO;
}

#pragma mark -
#pragma mark Requests

- (IBAction)addNewUrlBtnTouch:(id)sender {
    NSString *urlStr = self.addNewUrlTextField.text;
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestAddUrl:urlStr success:^(NSDictionary *responseObject) {

        [weakSelf.whiteListUrlArray addObject:responseObject[@"urlWhitelist"]];
        [weakSelf.tableView reloadData];
        weakSelf.addNewUrlTextField.text = @"";

        NSMutableDictionary *newCurrentDeviceOwner = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
        NSMutableArray *urlBL = [NSMutableArray new];
        [urlBL addObjectsFromArray:newCurrentDeviceOwner[@"urlWhitelists"]];
        [urlBL addObject:responseObject[@"urlWhitelist"]];
        [newCurrentDeviceOwner removeObjectForKey:@"urlWhitelists"];
        newCurrentDeviceOwner[@"urlWhitelists"] = urlBL;
        weakSelf.userSettings.currentDeviceOwner = newCurrentDeviceOwner;
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)updateUrl:(NSString*)newUrl urlID:(NSInteger)indexRow {
    isEditing = NO;
    NSString *urlStr = newUrl;
    NSDictionary *urlInfo = self.whiteListUrlArray[indexRow];
    NSInteger urlID = [urlInfo[@"id"] integerValue];
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestUpdateUrl:urlStr urlID:urlID success:^(NSDictionary *responseObject) {

        weakSelf.cellStateArray[indexRow] = @YES;
        weakSelf.whiteListUrlArray[indexRow] = responseObject[@"urlWhitelist"];
        [weakSelf.tableView reloadData];

        NSMutableDictionary *newCurrentDeviceOwner = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
        NSMutableArray *urlBL = [NSMutableArray new];
        [urlBL addObjectsFromArray:newCurrentDeviceOwner[@"urlWhitelists"]];

        for (NSInteger i = 0; i < urlBL.count; i++) {
            NSDictionary *dict = urlBL[i];

            if ([dict[@"id"] integerValue] == [responseObject[@"urlWhitelist"][@"id"] integerValue]) {
                urlBL[i] = responseObject[@"urlWhitelist"];

                break;
            }
        }
        [newCurrentDeviceOwner removeObjectForKey:@"urlWhitelists"];
        newCurrentDeviceOwner[@"urlWhitelists"] = urlBL;
        weakSelf.userSettings.currentDeviceOwner = newCurrentDeviceOwner;
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)deleteUrl:(NSInteger)indexRow
{
    NSDictionary *urlInfo = self.whiteListUrlArray[indexRow];
    NSInteger urlID = [urlInfo[@"id"] integerValue];
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestDeleteUrl:urlID success:^(NSDictionary *responseObject) {

        [weakSelf.whiteListUrlArray removeObjectAtIndex:indexRow];
        [weakSelf.tableView reloadData];

        NSMutableDictionary *newCurrentDeviceOwner = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
        NSMutableArray *urlBL = [NSMutableArray new];
        [urlBL addObjectsFromArray:newCurrentDeviceOwner[@"urlWhitelists"]];

        for (NSInteger i = 0; i < urlBL.count; i++) {
            NSDictionary *dict = urlBL[i];

            if ([dict[@"id"] integerValue] == urlID) {
                [urlBL removeObjectAtIndex:i];

                break;
            }
        }

        [newCurrentDeviceOwner removeObjectForKey:@"urlWhitelists"];
        newCurrentDeviceOwner[@"urlWhitelists"] = urlBL;
        weakSelf.userSettings.currentDeviceOwner = newCurrentDeviceOwner;
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.cellStateArray.count != self.whiteListUrlArray.count)
    {
        self.cellStateArray = [NSMutableArray new];
        for (int i = 0; i < self.whiteListUrlArray.count; i++) {
            [self.cellStateArray addObject:@YES];
        }
    }
    return self.whiteListUrlArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MGSwipeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:kWhitelistCellIdentifier];
    
    NSDictionary *rowInfo = self.whiteListUrlArray[indexPath.row];
    
    NSString *siteAdress = @"";
    siteAdress = rowInfo[@"url"];
    if ([rowInfo[@"url"] containsString:@"http://"] || [rowInfo[@"url"] containsString:@"https://"] || [rowInfo[@"url"] containsString:@"www."])
    {
        siteAdress = [siteAdress stringByReplacingOccurrencesOfString:@"https://" withString:@""];
        siteAdress = [siteAdress stringByReplacingOccurrencesOfString:@"http://" withString:@""];
        siteAdress = [siteAdress stringByReplacingOccurrencesOfString:@"www." withString:@""];
    }
    
    UILabel *title = (UILabel *)[cell.contentView viewWithTag:1];
    title.text = siteAdress;
    
    UIView *view = (UIView *)[cell.contentView viewWithTag:2];
    view.hidden = [self.cellStateArray[indexPath.row] boolValue];
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    cell.rightExpansion.buttonIndex = 2;
    cell.rightExpansion.fillOnTrigger = YES;
    cell.rightButtons = [self createRightButtons];
    cell.delegate = self;
    
    UIImageView *arrow = (UIImageView *)[cell.contentView viewWithTag:10];
    arrow.image = [UIImage imageNamed:@"left_circle"];
    
    UITextField *editTextField = (UITextField *)[cell.contentView viewWithTag:3];
    [editTextField addOffset];
    editTextField.text = rowInfo[@"url"];
    
    if (![self.cellStateArray[indexPath.row] boolValue] && !keyboardShow)
    {
        keyboardShow = YES;
        editTextField.delegate = self;
        firstResponderIndexPath = indexPath;
        [editTextField becomeFirstResponder];
        [editTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
    }
    
    UIButton *okButton = (UIButton *)[cell.contentView viewWithTag:5];
    [okButton addTarget:self
               action:@selector(okBtnTouch:)
     forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *cancelButton = (UIButton *)[cell.contentView viewWithTag:6];
    [cancelButton addTarget:self
                 action:@selector(cancelBtnTouch:)
       forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState) state gestureIsActive:(BOOL) gestureIsActive
{
    if (state == MGSwipeStateNone)
    {
        if (!keyboardShow)
            [self.tableView reloadData];
    }
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction
{
    if (direction == MGSwipeDirectionLeftToRight)
        return NO;
    NSNumber* row = nil;
    
    UIImageView *arrow = (UIImageView *)[cell.contentView viewWithTag:10];
    arrow.image = [UIImage imageNamed:@"rigth_circle"];
    for (int i = 0; i < self.cellStateArray.count; i++)
    {
        if (![self.cellStateArray[i] boolValue])
        {
            self.cellStateArray[i] = @YES;
            row = @(i);
        }
    }
    if (row != nil)
    {
        UITableViewCell* tableCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row.integerValue inSection:0]];
        
        [self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:tableCell]] withRowAnimation:UITableViewRowAnimationNone];
    }
    return YES;
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSIndexPath * path = [_tableView indexPathForCell:cell];
    if (path == nil)
        path = [NSIndexPath indexPathForRow:0 inSection:0];
    
    if ([self.cellStateArray[path.row] boolValue])
    {
        if (direction == MGSwipeDirectionRightToLeft && index == 0)
        {
            //delete
            isEditing = NO;
            [self deleteUrl:path.row];
        }
        else if (direction == MGSwipeDirectionRightToLeft && index == 1)
        {
            //edit
            isEditing = YES;
            self.cellStateArray[path.row] = @NO;
            [self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }

    return YES;
}

#pragma mark -
#pragma mark - Configure cell

-(NSArray *) createRightButtons
{
    NSMutableArray * result = [NSMutableArray array];
    NSString *titles[2] = {LS(@"Delete"), LS(@"Edit")};
    UIColor * colors[2] = {[UIColor redColor], [UIColor lightGrayColor]};
    for (int i = 0; i < 2; ++i)
    {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:titles[i] backgroundColor:colors[i] callback:^BOOL(MGSwipeTableCell * sender){
            return YES;
        }];
        button.tag = i;
        [result addObject:button];
    }
    return result;
}

#pragma mark -
#pragma mark - Actions

- (void)okBtnTouch:(id)sender
{
    NSInteger row = 0;
    for (int i = 0; i < self.cellStateArray.count; i++)
    {
        if (![self.cellStateArray[i] boolValue])
            row = i;
    }
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
    UITextField *editTextField = (UITextField *)[cell.contentView viewWithTag:3];
    
    [self updateUrl:editTextField.text urlID:row];
}

- (void)cancelBtnTouch:(id)sender
{
    NSInteger row = 0;
    for (int i = 0; i < self.cellStateArray.count; i++)
    {
        if (![self.cellStateArray[i] boolValue])
            row = i;
    }
    self.cellStateArray[row] = @YES;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (isEditing && textField.tag != 0)
    {
        isEditing = NO;
        NSInteger row = 0;
        for (int i = 0; i < self.cellStateArray.count; i++)
        {
            if (![self.cellStateArray[i] boolValue])
                row = i;
        }
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
        UITextField *editTextField = (UITextField *)[cell.contentView viewWithTag:3];
        
        [self updateUrl:editTextField.text urlID:row];
        return YES;
    }
    [self addNewUrlBtnTouch:nil];
    return YES;
}

@end
