//
//  HHChangePasswordViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/22/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHChangePasswordViewController.h"
#import "HHAPI.h"

@interface HHChangePasswordViewController ()

@property (nonatomic, weak) IBOutlet UITextField *currentPasswordTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;
@property (nonatomic, weak) IBOutlet UITextField *confirmPasswordTextField;

@property (nonatomic) CGRect startFrame;

@property (nonatomic, weak) IBOutlet UIView *topView;
@property (nonatomic, weak) IBOutlet UIView *secondTopView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *yConstraintLogo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomSpaceConstraint;

@end

@implementation HHChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoChangePasswordScreen") : LS(@"ChangePasswordScreen");
    
    self.startFrame = self.view.frame;
    [self.view bringSubviewToFront:self.topView];
    [self.view bringSubviewToFront:self.secondTopView];
}


#pragma mark - Private

- (void)keyboardWillChange:(NSNotification *)notification {
    self.view.frame = self.startFrame;
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat delta = self.view.superview.bounds.size.height - keyboardRect.origin.y - self.bottomSpaceConstraint.constant;
    
    self.yConstraintLogo.constant = -delta;
    
    CGRect frame = self.view.frame;
    frame.size.height -= delta;
    self.view.frame = frame;
    
    self.yConstraintLogo.constant = -delta;
}

- (void)noticeWillHideKeyboard:(NSNotification *)inNotification {
    CGRect frame = self.view.frame;
    frame.size.height = self.view.superview.bounds.size.height - frame.origin.y;
    self.view.frame = frame;
    
    self.yConstraintLogo.constant = 0;
}


#pragma mark Actions

- (IBAction)changePasswordBtnTouch:(id)sender {
    if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        [HHUtils alertWithTitle:LS(@"VerifingPasswordError") message:LS(@"PasswordAren'tSame")];
    } else {
        __weak __typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDictionary *data = @{
                @"currentPassword" : self.currentPasswordTextField.text,
                @"password" : self.passwordTextField.text
        };
        [[HHAPI sharedAPI] requestChangePassword:data success:^(NSDictionary *responseObject) {

            [HHUtils alertWithTitle:LS(@"Success") message:nil];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }
}

@end
