//
//  HHBlockViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/27/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBlockViewController.h"
#import "EFCircularSlider.h"
#import "HHUsersViewController.h"
#import "HHAPI.h"

@interface HHBlockViewController () <UIGestureRecognizerDelegate> {
    EFCircularSlider* circularSlider;
}

@property (nonatomic, weak) IBOutlet UILabel *navigationLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *blockButton;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UILabel *minutesLabel;

@property (nonatomic, weak) IBOutlet UILabel *blockActiveLabel;

@property (nonatomic, weak) IBOutlet UIButton *applyButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@property (nonatomic) NSInteger currentTime;

@end

@implementation HHBlockViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoBlockScreen") : LS(@"BlockScreen");

    self.navigationLabel.text = self.userSettings.currentDeviceOwner[@"title"];
    [self.blockButton setTitle:LS(@"BlockFor0m") forState:UIControlStateNormal];
    self.currentTime = 0;
    
    CGRect sliderFrame = CGRectMake(120, 150, 280, 280);
    circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    circularSlider.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    
    circularSlider.unfilledColor = [UIColor lightGrayColor];
    circularSlider.filledColor = [UIColor colorWithRed:115/255.0f green:180/255.0f blue:6/255.0f alpha:1.0f];
    circularSlider.handleType = CircularSliderHandleTypeBigCircle;
    circularSlider.handleColor = [UIColor blueColor];
    circularSlider.lineWidth = 40;
    
    [circularSlider addTarget:self action:@selector(valueDidChange:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:circularSlider];
    [self.view sendSubviewToBack:circularSlider];
    
    [self.view bringSubviewToFront:self.timeLabel];
    [self.view bringSubviewToFront:self.minutesLabel];
    [self.view bringSubviewToFront:self.blockActiveLabel];
    self.timeLabel.center = circularSlider.center;
    self.blockButton.frame = CGRectMake(self.blockButton.frame.origin.x, circularSlider.frame.origin.y + circularSlider.frame.size.height, self.blockButton.frame.size.width, self.blockButton.frame.size.height);

    self.blockButton.hidden = NO;
    self.applyButton.hidden = YES;
    self.cancelButton.hidden = YES;
    self.blockActiveLabel.hidden = YES;
    [self setStartValue];
    
    NSTimer *timerStartValue = [NSTimer scheduledTimerWithTimeInterval:20.0f target:self selector:@selector(setStartValue) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timerStartValue forMode:NSRunLoopCommonModes];
}

- (void)setStartValue {
    NSDate *dateFromString = [[HHUtils dateFormatterUTC] dateFromString:self.userSettings.currentDeviceOwner[@"blockStartedAt"][@"dateTime"]];
    NSNumber *durationTime = @([self.userSettings.currentDeviceOwner[@"time"] doubleValue]);
    NSDate *date = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];
    
    NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:date];
    NSInteger distanceBetweenDatesInt = distanceBetweenDates;
    
    if ([date compare:[NSDate date]] == NSOrderedAscending) {
        distanceBetweenDatesInt = 0;
    }

    if (distanceBetweenDatesInt < 0) {
        distanceBetweenDatesInt *= -1;
    }

    NSInteger minutes = distanceBetweenDatesInt / 60;

    if (distanceBetweenDatesInt % 60 > 30) {
        minutes++;
    }
    
    if (minutes > 0) {
        self.blockButton.hidden = YES;
        self.applyButton.hidden = NO;
        self.cancelButton.hidden = NO;
        
        NSString *descriptionStr;

        if (minutes > 120) {
            int hours = (int)minutes / 60;

            if (minutes % 60 > 30) {
                hours++;
            }

            descriptionStr = [NSString stringWithFormat:@"%d %@", hours, LS(@"hours")];
        } else if (minutes <= 120 && minutes > 60) {
            int hours = (int)minutes/60;
            descriptionStr = [NSString stringWithFormat:@"%d %@ %d %@", hours, LS(@"hours"), (int)minutes%60, LS(@"mins")];
        } else if (minutes == 60) {
            descriptionStr = [NSString stringWithFormat:@"1 %@", LS(@"hours")];
        } else {
            descriptionStr = [NSString stringWithFormat:@"%ld %@", (long)minutes, LS(@"minutes")];
        }
        
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@ %@ %@", LS(@"BlockActive:"), descriptionStr, LS(@"remaining")]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:RGB(115, 180, 6)
                     range:NSMakeRange(13, text.length-13)];
        self.blockActiveLabel.attributedText = text;
        self.blockActiveLabel.hidden = NO;
    }
    
    float value = 0;

    if (minutes <= 30) {
        minutes++;
        value = minutes * 1.1f;
    } else if (minutes > 30 && minutes < 120) {
        minutes++;
        minutes -= 30;
        value = 33.3f*(minutes/90.f) + 33.3f;
        
    } else {
//        minutes+= 10;
        minutes -= 120;
        value = 33.3f*(minutes/600.f) + 66.6f;
    }

    circularSlider.startValue = value;
}

#pragma mark -
#pragma mark Request

- (IBAction)cancelBtnTouch:(id)sender
{
    NSMutableDictionary *request = [NSMutableDictionary new];
    NSDictionary *data = [HHUserSettings sharedSettings].currentDeviceOwner;
    
    [request setValue:data[@"title"] forKey:@"title"];
    [request setValue:data[@"id"] forKey:@"id"];
    [request setValue:data[@"status"] forKey:@"status"];
    [request setValue:data[@"createdAt"] forKey:@"createdAt"];
    [request setValue:data[@"contentProfile"][@"id"] forKey:@"contentProfile"];
    if (data[@"homeworkModeDuration"])
        [request setValue:data[@"homeworkModeDuration"] forKey:@"homeworkModeDuration"];
    if (data[@"homeworkModeStartedAt"])
        [request setValue:data[@"homeworkModeStartedAt"][@"dateTime"] forKey:@"homeworkModeStartedAt"];
    if (data[@"homeworkCategories"])
        [request setValue:data[@"homeworkCategories"] forKey:@"homeworkCategories"];
    
    [request setValue:[HHUtils currentDateUTC] forKey:@"blockStartedAt"];
    [request setValue:@0 forKey:@"time"];
    
    NSMutableArray *timeProfilesArray = [NSMutableArray new];
    for(NSDictionary *timeData in data[@"timeProfiles"])
    {
        NSMutableDictionary *obj = [NSMutableDictionary new];
        [obj setValue:timeData[@"dayOfWeek"] forKey:@"dayOfWeek"];
        [obj setValue:timeData[@"startTime"][@"time"] forKey:@"startTime"];
        [obj setValue:timeData[@"endTime"][@"time"] forKey:@"endTime"];
        [timeProfilesArray addObject:obj];
    }
    [request setValue:timeProfilesArray forKey:@"timeProfiles"];
    
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSLog(@"%@", request);
    [[HHAPI sharedAPI] requestBlockUser:request success:^(NSDictionary *responseObject) {

        [weakSelf unwindToViewControllerOfClass:HHUsersViewController.class animated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (IBAction)blockBtnTouch:(id)sender
{
    NSMutableDictionary *request = [NSMutableDictionary new];
    NSDictionary *data = [HHUserSettings sharedSettings].currentDeviceOwner;
    
    [request setValue:data[@"title"] forKey:@"title"];
    [request setValue:data[@"id"] forKey:@"id"];
    [request setValue:data[@"status"] forKey:@"status"];
    [request setValue:data[@"createdAt"] forKey:@"createdAt"];
    [request setValue:data[@"contentProfile"][@"id"] forKey:@"contentProfile"];
    [request setValue:@"suspend" forKey:@"status"];
    if (data[@"homeworkModeDuration"])
        [request setValue:data[@"homeworkModeDuration"] forKey:@"homeworkModeDuration"];
    if (data[@"homeworkModeStartedAt"])
        [request setValue:data[@"homeworkModeStartedAt"][@"dateTime"] forKey:@"homeworkModeStartedAt"];
    if (data[@"homeworkCategories"])
        [request setValue:data[@"homeworkCategories"] forKey:@"homeworkCategories"];
    
    [request setValue:[HHUtils currentDateUTC] forKey:@"blockStartedAt"];
    [request setValue:@(self.currentTime*60) forKey:@"time"];
    
    NSMutableArray *timeProfilesArray = [NSMutableArray new];
    for(NSDictionary *timeData in data[@"timeProfiles"])
    {
        NSMutableDictionary *obj = [NSMutableDictionary new];
        [obj setValue:timeData[@"dayOfWeek"] forKey:@"dayOfWeek"];
        [obj setValue:timeData[@"startTime"][@"time"] forKey:@"startTime"];
        [obj setValue:timeData[@"endTime"][@"time"] forKey:@"endTime"];
        [timeProfilesArray addObject:obj];
    }
    [request setValue:timeProfilesArray forKey:@"timeProfiles"];
    
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestBlockUser:request success:^(NSDictionary *responseObject) {

        [weakSelf unwindToViewControllerOfClass:HHUsersViewController.class animated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark -
#pragma mark Actions

- (void)valueDidChange:(EFCircularSlider*)slider
{
    int time = 0;
    if (slider.currentValue < 33.3f)
    {
        time = (int)(slider.currentValue/1.1f);
        self.timeLabel.text = [NSString stringWithFormat:@"%d", time];
        self.minutesLabel.text = LS(@"minutes");
    }
    else if (slider.currentValue > 33.3f && slider.currentValue < 66.6f)
    {
        int division = (slider.currentValue - 33.3f)/3.6f;
        time = 30 + 10*division;
        if ((time % 60) == 0)
        {
            self.timeLabel.text = [NSString stringWithFormat:@"%d", time/60];
            self.minutesLabel.text = LS(@"hours");
        }
        else if ((time / 60) >= 1)
        {
            self.timeLabel.text = [NSString stringWithFormat:@"%d:%d", time/60, time%60];
            self.minutesLabel.text = LS(@"hours");
        }
        else
        {
            self.timeLabel.text = [NSString stringWithFormat:@"%d", time];
            self.minutesLabel.text = LS(@"minutes");
        }
    }
    else
    {
        time = ((int)((slider.currentValue-66.6f)/3.1f) + 2) * 60;
        self.timeLabel.text = [NSString stringWithFormat:@"%d", time/60];
        self.minutesLabel.text = LS(@"hours");
    }
    [self.blockButton setTitle:[NSString stringWithFormat:@"%@ %@ %@", LS(@"BlockFor"), self.timeLabel.text, self.minutesLabel.text] forState:UIControlStateNormal];
    self.currentTime = time;
}

@end
