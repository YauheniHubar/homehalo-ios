//
//  HHAssignDeviceViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 3/23/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@interface HHAssignDeviceViewController : HHBaseViewController

@property (nonatomic) NSDictionary *deviceInfo;
@property (nonatomic) NSDictionary *alertData;
@property (nonatomic) NSNumber *alertID;

@end
