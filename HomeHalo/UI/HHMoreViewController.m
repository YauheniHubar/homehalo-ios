//
//  HHMoreViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/13/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHMoreViewController.h"
#import "HHDevicesViewController.h"

static NSString *const kMoreCell = @"kMoreCell";
static NSString *const kSegueIdBuyHomeHalo = @"buyHomeHaloSegue";
static NSString *const kSegueIdHelp = @"helpSegue";
static NSString *const kSegueIdContentProfile = @"contentProfileSegue";
static NSString *const kSegueIdSettings = @"settingsSegue";
static NSString *const kSegueIdLogout = @"logoutSegue";
static NSString *const kSegueIdDevice = @"deviceSegue";

@interface HHMoreViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation HHMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoMoreScreen");
    } else {
        self.screenName = LS(@"MoreScreen");
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueIdDevice])
    {
        HHDevicesViewController* vc = segue.destinationViewController;
        vc.isAllUsersShow = YES;
    }
}

-(NSString *)segueIdentifierForIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return kSegueIdDevice;
            break;
        case 1:
            return kSegueIdContentProfile;
            break;
        case 2:
            [self launchMailAppOnDevice];
            return nil;
            break;
        case 3:
        {
//            return kSegueIdHelp;
            
            if ([HHUserSettings sharedSettings].isDemoMode)
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.homehalo.net/iosapp"]];
            else
                [self launchMailHelp];
        }
            break;
        case 4:
            return kSegueIdSettings;
            break;
        case 5:
            [self logout];
            return nil;
            break;
        default:
            break;
    }
    return nil;
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = self.tableView.frame.size.height/6;
    if (cellHeight > 44.f)
        return cellHeight;
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMoreCell];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    UIImageView *icon = (UIImageView *)[cell.contentView viewWithTag:2];
    switch (indexPath.row) {
        case 0:
        {
            [titleLabel setText:LS(@"Devices")];
            icon.image = [UIImage imageNamed:@"new_device_icon"];
        }
            break;
        case 1:
        {
            [titleLabel setText:LS(@"ContentRules")];
            icon.image = [UIImage imageNamed:@"new_content_rules_icon"];
        }
            break;
        case 2:
        {
            [titleLabel setText:LS(@"TellAFriend")];
            icon.image = [UIImage imageNamed:@"new_tell_friend_icon"];
        }
            break;
        case 3:
        {
            if ([HHUserSettings sharedSettings].isDemoMode) {
                [titleLabel setText:LS(@"FindOutMore")];
                icon.image = [UIImage imageNamed:@"new_info_icon"];
            }
            else
            {
                [titleLabel setText:LS(@"ContactUs")];
                icon.image = [UIImage imageNamed:@"new_contact_icon"];
            }
        }
            break;
        case 4:
        {
            [titleLabel setText:LS(@"Settings")];
            icon.image = [UIImage imageNamed:@"new_settings_icon"];
        }
            break;
        case 5:
        {
            [titleLabel setText:LS(@"Logout")];
            icon.image = [UIImage imageNamed:@"new_end_demo_icon"];

            if ([HHUserSettings sharedSettings].isDemoMode)
                [titleLabel setText:LS(@"EndDemoMode")];
        }
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self segueIdentifierForIndexPath:indexPath] != nil)
        [self performSegueWithIdentifier:[self segueIdentifierForIndexPath:indexPath] sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark - Private

-(void)launchMailAppOnDevice
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        
        if ([HHUserSettings sharedSettings].isDemoMode) {
            [mail setSubject:LS(@"TryToUseHomeHalo")];
            [mail setMessageBody:LS(@"IUseHomeHaloMessageDemo") isHTML:YES];
        }
        else
        {
            [mail setSubject:LS(@"TryToUseHomeHalo")];
            [mail setMessageBody:LS(@"IUseHomeHaloMessage") isHTML:YES];
        }
        
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else
        [HHUtils alertErrorMessage:LS(@"ThisDeviceCannotSendEmail")];
}

- (void)launchMailHelp
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        
        NSArray *usersTo = @[@"appenquiry@homehalo.net"];
        [mail setToRecipients:usersTo];
        [mail setSubject:LS(@"HomeHaloAppEnquiry")];
        
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else
    [HHUtils alertErrorMessage:LS(@"ThisDeviceCannotSendEmail")];
}

#pragma mark -
#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self becomeFirstResponder];
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
