//
//  HHDevicesViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 2/4/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@interface HHDevicesViewController : HHBaseViewController

@property (nonatomic) BOOL isAllUsersShow;

@end
