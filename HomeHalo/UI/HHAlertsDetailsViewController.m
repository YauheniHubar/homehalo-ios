//
//  HHAlertsDetailsViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 3/13/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHAlertsDetailsViewController.h"
#import "HHNotification.h"
#import "HHAPI.h"
#import "HHAPI+Alerts.h"

static NSString *const kAlertDetailsCell = @"kAlertDetailsCell";
static NSString *const kGoToAlertsPreviewSegue = @"kGoToAlertsPreviewSegue";

@interface HHAlertsDetailsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation HHAlertsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoAlertsDetailsScreen") : LS(@"AlertsDetailsScreen");
}


#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 4) {
        return self.tableView.frame.size.height - (4*44.f);
    }

    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 4) {
        UITableViewCell *cell = [UITableViewCell new];//[tableView cellForRowAtIndexPath:indexPath];
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height - (4 * 44.f))];
        
        NSString *urlString;

        if ([self.notification.data[@"url"] containsString:@"http://"] || [self.notification.data[@"url"] containsString:@"https://"]) {
            urlString = self.notification.data[@"url"];
        } else {
            urlString = [NSString stringWithFormat:@"http://%@", self.notification.data[@"url"]];
        }

        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [webView loadRequest:requestObj];
        [cell addSubview:webView];

        return cell;
    }

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAlertDetailsCell];
    cell.backgroundColor = [UIColor orangeColor];
    UILabel *title = [cell.contentView viewWithTag:1];
    
    switch (indexPath.row) {
        case 0:
            title.text = LS(@"Accept");
            break;
        case 1:
            title.text = LS(@"Decline");
            break;
        case 2:
            title.text = LS(@"ReviewLater");
            break;
        case 3:
            title.text = LS(@"Clear");
            break;
        case 4:
            title.text = LS(@"Preview");
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak __typeof(self) weakSelf = self;
    switch (indexPath.row) {
        case 0: {
            NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:self.notification.data];
            data[@"status"] = @"accepted";
            NSMutableDictionary *newData = [NSMutableDictionary new];
            newData[@"data"] = data;
            newData[@"isViewed"] = @"1";
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[HHAPI sharedAPI] requestUpdateAlerts:newData notificationID:(self.notification.uid).integerValue success:nil failure:^(NSError *error) {

                [HHUtils alertErrorMessage:error.localizedDescription];

            }];

            [[HHAPI sharedAPI] requestUpdateUrl:self.notification.data[@"url"] urlID:[self.notification.data[@"id"] integerValue] success:^(NSDictionary *responseObject) {

                [weakSelf.navigationController popViewControllerAnimated:YES];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            } failure:^(NSError *error) {

                [HHUtils alertErrorMessage:error.localizedDescription];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            }];

            break;
        }

        case 1: {
            NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:self.notification.data];
            data[@"status"] = @"denied";
            NSMutableDictionary *newData = [NSMutableDictionary new];
            newData[@"data"] = data;
            newData[@"id"] = @"id";
            newData[@"type"] = @"type";
            newData[@"title"] = @"title";
            newData[@"isViewed"] = @"1";
            newData[@"createdAt"] = (self.notification.createdAt).description;
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[HHAPI sharedAPI] requestUpdateAlerts:newData notificationID:(self.notification.uid).integerValue success:^(id response) {

                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];


            } failure:^(NSError *error) {

                [HHUtils alertErrorMessage:error.localizedDescription];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            }];
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[HHAPI sharedAPI] requestDeclineUrl:self.notification success:^(NSDictionary *responseObject) {

                [weakSelf.navigationController popViewControllerAnimated:YES];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            } failure:^(NSError *error) {

                [HHUtils alertErrorMessage:error.localizedDescription];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            }];
            break;
        }

        case 2:
            [self.navigationController popViewControllerAnimated:YES];
            break;

        case 3: {
            NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:self.notification.data];
            data[@"status"] = @"denied";
            NSMutableDictionary *newData = [NSMutableDictionary new];
            newData[@"data"] = data;
            newData[@"isViewed"] = @"1";
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[HHAPI sharedAPI] requestUpdateAlerts:newData notificationID:(self.notification.uid).integerValue success:^(id response) {

                [weakSelf.navigationController popViewControllerAnimated:YES];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            } failure:^(NSError *error) {

                [HHUtils alertErrorMessage:error.localizedDescription];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            }];

            break;
        }

        case 4:
            break;
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
