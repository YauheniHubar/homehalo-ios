//
//  HHSignUpViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHSignUpViewController.h"
#import "UITextField+Offset.h"
#import "HHAPI.h"
#import "HHAPI+Auth.h"
#import "HHUserResponse.h"
#import "HHUser.h"

static NSString *const kSegueIdToRegisterScreen = @"goToRegisterSegue";
static NSString *const kSegueIdToForgotPasswordScreen = @"goToForgotPasswordSegue";
static NSString *const kSegueIdToHomeScreen = @"goToHomeSegue";

@interface HHSignUpViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;

@property (nonatomic, weak) IBOutlet UIButton *loginBtn;

@property (nonatomic, weak) IBOutlet UIButton *forgotPasswordBtn;
@property (nonatomic, weak) IBOutlet UIButton *registerBtn;

@property (nonatomic, weak) IBOutlet UIView *topView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *yConstraintLogo;

@property (nonatomic) CGRect startFrame;

@end

@implementation HHSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoSignUpScreen") : LS(@"SignUpScreen");
    
    [self.emailTextField addOffset];
    [self.passwordTextField addOffset];
    
    self.startFrame = self.view.frame;
    [self.view bringSubviewToFront:self.topView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    
    [self.navigationController setNavigationBarHidden:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return NO;
}

- (void)keyboardWillChange:(NSNotification *)notification {
    self.view.frame = self.startFrame;
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat delta = self.view.superview.bounds.size.height - keyboardRect.origin.y;
    
    if(IS_IPHONE_4) {
        delta = 150;
    }
    
    self.yConstraintLogo.constant = -delta;
    
    CGRect frame = self.view.frame;
    frame.size.height -= delta;
    self.view.frame = frame;
    
    self.yConstraintLogo.constant = -delta;
}

- (void)noticeWillHideKeyboard:(NSNotification *)inNotification {
    CGRect frame = self.view.frame;
    frame.size.height = self.view.superview.bounds.size.height - frame.origin.y;
    self.view.frame = frame;
    
    self.yConstraintLogo.constant = 0;
}


#pragma mark -
#pragma mark Actions

- (IBAction)loginBtnTouch:(id)sender {
    if (![HHUtils checkInternetConnection]) {
        [HHUtils alertErrorMessage:LS(@"NetworkErrorMessageNoInternetConnection")];

        return;
    }

    NSString *username = self.emailTextField.text;
    NSString *password = self.passwordTextField.text;
    NSString *deviceToken = [HHUserSettings sharedSettings].deviceToken;

    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestLoginWithUsername:username password:password deviceToken:deviceToken success:^(HHUserResponse *userResponse) {

        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [HHUserSettings sharedSettings].demoMode = NO;
        [HHUserSettings sharedSettings].accessToken = userResponse.accessToken;
        [HHUserSettings sharedSettings].currentUser = userResponse.user;

        [UIApplication sharedApplication].applicationIconBadgeNumber = userResponse.user.notificationCount.integerValue;

        [weakSelf goToPinScreen];

    } failure:^(NSError *error) {

        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        if ([error.localizedDescription isEqualToString:@"Bad credentials"]) {
            [HHUtils alertErrorMessage:LS(@"YourUsernameOrPasswordIsIncorrect")];
        } else {
            [HHUtils alertErrorMessage:error.localizedDescription];
        }

    }];
}

- (IBAction)forgotPasswordBtnTouch:(id)sender {
    [self performSegueWithIdentifier:kSegueIdToForgotPasswordScreen sender:nil];
}

- (IBAction)registerBtnTouch:(id)sender {
    [self performSegueWithIdentifier:kSegueIdToRegisterScreen sender:nil];
}

- (IBAction)demoModBtnTouch:(id)sender {
    NSString *deviceToken = [HHUserSettings sharedSettings].deviceToken;

    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestDemoModeWithDeviceToken:deviceToken success:^(HHUserResponse *userResponse) {

        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [HHUserSettings sharedSettings].demoMode = YES;
        [HHUserSettings sharedSettings].accessToken = userResponse.accessToken;
        [HHUserSettings sharedSettings].currentUser = userResponse.user;

        [weakSelf homeButtonTouch:nil];

    } failure:^(NSError *error) {

        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [HHUtils alertErrorMessage:error.localizedDescription];

    }];
}

@end
