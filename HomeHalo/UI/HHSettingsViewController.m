//
//  HHSettingsViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/21/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHSettingsViewController.h"
#import "HHUser.h"
#import "HHWedge.h"
#import "HHAPI.h"

static NSString *const kSettingsTextCellIdentifier = @"settingsTextCellIdentifier";
static NSString *const kSettingsButtonCellIdentifier = @"settingsButtonCellIdentifier";
static NSString *const kLinkCellIdentifier = @"linkCellIdentifier";
static NSString *const kSegueIdToPinScreen = @"goToPin";
static NSString *const kSegueIdToChangePasswordScreen = @"goToChangePassword";

@interface HHSettingsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) HHUser *currentUser;

@end

@implementation HHSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoSettingsScreen") : LS(@"SettingsScreen");
    self.currentUser = self.userSettings.currentUser;
}


#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title = @"";

    switch (section) {
        case 0:
            title = LS(@"UserInformation");
            break;
        case 1:
            title = LS(@"WiFiConnectionInfo");
            break;
        case 2:
            title = LS(@"VersionInfo");
            break;
        default:
            break;
    }

    return title;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 34.f;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    view.tintColor = RGB(130, 115, 110);
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = [UIColor whiteColor];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger row = 0;

    switch (section) {
        case 0:
            row = 3;
            break;
        case 1:
            row = 2;
            break;
        case 2:
            row = 3;
            break;
        default:
            break;
    }

    return row;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;

    if (indexPath.section == 2 && indexPath.row == 2) {
        return [tableView dequeueReusableCellWithIdentifier:kLinkCellIdentifier];
    }

    if (indexPath.section == 0 && indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:kSettingsButtonCellIdentifier];
        UILabel *titleLabel = [cell.contentView viewWithTag:1];
        titleLabel.text = LS(@"ChangePassword");
        cell.selectionStyle = self.userSettings.isDemoMode ? UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleDefault;

        return cell;
    } else if (indexPath.section == 0 && indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:kSettingsButtonCellIdentifier];
        UILabel *titleLabel = [cell.contentView viewWithTag:1];
        titleLabel.text = LS(@"ChangeLoginPin");
        cell.selectionStyle = self.userSettings.isDemoMode ? UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleDefault;

        return cell;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:kSettingsTextCellIdentifier];
    }

    switch (indexPath.section) {
        case 0: {
            UILabel *titleLabel = [cell.contentView viewWithTag:2];
            titleLabel.text = LS(@"CurrentUser");
            UILabel *desctiptLabel = [cell.contentView viewWithTag:3];
            desctiptLabel.text = [HHUserSettings sharedSettings].isDemoMode ? LS(@"DEMO") : self.currentUser.email;
        }

            break;

        case 1:
            if (indexPath.row == 1) {
                UILabel *titleLabel = [cell.contentView viewWithTag:2];
                titleLabel.text = LS(@"Passkey");
                UILabel *desctiptLabel = [cell.contentView viewWithTag:3];
                desctiptLabel.text = self.currentUser.wedge.password;
            } else {
                UILabel *titleLabel = [cell.contentView viewWithTag:2];
                titleLabel.text = LS(@"HomeHaloNameSSID");
                UILabel *desctiptLabel = [cell.contentView viewWithTag:3];
                desctiptLabel.text = self.currentUser.wedge.ssid;
            }

            break;
        case 2:
            if (indexPath.row == 1) {
                UILabel *titleLabel = [cell.contentView viewWithTag:2];
                titleLabel.text = LS(@"HomeHaloFirmware");
                UILabel *desctiptLabel = [cell.contentView viewWithTag:3];
                desctiptLabel.text = self.currentUser.wedge.firmware;
            } else {
                UILabel *titleLabel = [cell.contentView viewWithTag:2];
                titleLabel.text = LS(@"AppVersion");
                UILabel *desctiptLabel = [cell.contentView viewWithTag:3];
                NSString *appVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
                desctiptLabel.text = appVersion;
            }
            break;
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2 && indexPath.row == 2) {
        [self goToWebsiteBtnTouch:nil];
    } else if (indexPath.section == 0 && indexPath.row == 1) {
        if (!self.userSettings.isDemoMode) {
            [self performSegueWithIdentifier:kSegueIdToChangePasswordScreen sender:nil];
        }
    } else if (indexPath.section == 0 && indexPath.row == 2) {
        if (!self.userSettings.isDemoMode) {
            [self performSegueWithIdentifier:kSegueIdToPinScreen sender:nil];
        }
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Action

- (IBAction)goToWebsiteBtnTouch:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://homehalo.net/webpanel/#/profile"]];
}

- (IBAction)restartBtnTouch:(id)sender {
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestRestartWithSuccess:^(NSDictionary *responseObject) {

        [HHUtils alertWithTitle:LS(@"HomeHaloRestart") message:nil];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

@end
