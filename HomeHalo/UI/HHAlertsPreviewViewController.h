//
//  HHAlertsPreviewViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 3/17/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@interface HHAlertsPreviewViewController : HHBaseViewController

@property (nonatomic) NSString *url;

@end
