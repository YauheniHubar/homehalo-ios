//
//  HHUserPageViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/26/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHUserPageViewController.h"

static NSString *const kSegueIdToBlockScreen = @"goToBlockSegue";
static NSString *const kSegueIdToExtendTimeScreen = @"goToExtendTimeSegue";
static NSString *const kSegueIdToHomeworkScreen = @"goToHomeworkSegue";
static NSString *const kSegueIdToWhitelistScreen = @"goToWhitelistSegue";
static NSString *const kSegueIdToBlacklistScreen = @"goToBlacklistSegue";
static NSString *const kSegueIdToDevicesScreen = @"goToDevicesSegue";
static NSString *const kSegueIdToContentScreen = @"goToContentSegue";
static NSString *const kSegueIdToUserReportsScreen = @"goToUserReportsSegue";
static NSString *const kSegueIdToTimeProfileScreen = @"goToTimeProfileSegue";

static NSString *const kUsersCell = @"kUsersCell";
static NSString *const kUsersTitleCell = @"kUsersTitleCell";

@interface HHUserPageViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UILabel *navigationLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation HHUserPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoUserPageScreen");
    } else {
        self.screenName = LS(@"UserPageScreen");
    }

    self.navigationLabel.text = [HHUserSettings sharedSettings].currentDeviceOwner[@"title"];
    
    NSTimer *timerStartValue = [NSTimer scheduledTimerWithTimeInterval:20.0f target:self selector:@selector(reloadTimes) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timerStartValue forMode:NSRunLoopCommonModes];
}

- (void)reloadTimes
{
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = self.tableView.frame.size.height/9;
    if (cellHeight > 48.f)
        return cellHeight;
    return 48.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row >= 3) {
        cell = [tableView dequeueReusableCellWithIdentifier:kUsersTitleCell];
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:kUsersCell];
    }
    cell.backgroundColor = [UIColor colorWithRed:115/255.0f green:180/255.0f blue:6/255.0f alpha:1.0f];
    
    UIImageView *icon = (UIImageView *)[cell.contentView viewWithTag:1];
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *descriptionLabel = (UILabel *)[cell.contentView viewWithTag:3];
    [descriptionLabel setHidden:YES];
    
    NSDate *dateFromString = [[HHUtils dateFormatterUTC] dateFromString:[HHUserSettings sharedSettings].currentDeviceOwner[@"blockStartedAt"][@"dateTime"]];
    NSNumber* durationTime = @([[HHUserSettings sharedSettings].currentDeviceOwner[@"time"] doubleValue]);
    NSDate* date = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];
    
    switch (indexPath.row) {
        case 0:
            icon.image = [UIImage imageNamed:@"block_user_icon.png"];
            titleLabel.text = LS(@"Block");
            
            if ([date compare:[NSDate date]] == NSOrderedDescending)
            {
                descriptionLabel.text = [self getTimeStringWithStartDate:date];
                [descriptionLabel setHidden:NO];
                cell.backgroundColor = [UIColor colorWithRed:255/255.0f green:175/255.0f blue:1/255.0f alpha:1.0f];
            }
            break;
        case 1:
            icon.image = [UIImage imageNamed:@"extend_user_icon.png"];
            titleLabel.text = LS(@"Extend");
            
            //time extension
            dateFromString = [[HHUtils dateFormatterUTC] dateFromString:[HHUserSettings sharedSettings].currentDeviceOwner[@"timeExtension"][@"startedAt"][@"dateTime"]];
            durationTime = @([[HHUserSettings sharedSettings].currentDeviceOwner[@"timeExtension"][@"duration"] doubleValue]);
            date = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];
            if ([date compare:[NSDate date]] == NSOrderedDescending)
            {
                if ([dateFromString compare:[[HHUtils dateFormatterUTC] dateFromString:[HHUtils currentDateUTC]]] == NSOrderedDescending)
                    descriptionLabel.text = [self getTimeStringWithStartDate:date fromDate:dateFromString];
                else
                    descriptionLabel.text = [self getTimeStringWithStartDate:date fromDate:[[HHUtils dateFormatterUTC] dateFromString:[HHUtils currentDateUTC]]];
                [descriptionLabel setHidden:NO];
                cell.backgroundColor = [UIColor colorWithRed:255/255.0f green:175/255.0f blue:1/255.0f alpha:1.0f];
            }
            break;
        case 2:
            icon.image = [UIImage imageNamed:@"homework_user_icon.png"];
            titleLabel.text = LS(@"Homework");
            
            //homework
            dateFromString = [[HHUtils dateFormatterUTC] dateFromString:[HHUserSettings sharedSettings].currentDeviceOwner[@"homeworkModeStartedAt"][@"dateTime"]];
            durationTime = @([[HHUserSettings sharedSettings].currentDeviceOwner[@"homeworkModeDuration"] doubleValue]);
            date = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];
            
            if ([date compare:[NSDate date]] == NSOrderedDescending)
            {
                descriptionLabel.text = [self getTimeStringWithStartDate:date];
                [descriptionLabel setHidden:NO];
                cell.backgroundColor = [UIColor colorWithRed:255/255.0f green:175/255.0f blue:1/255.0f alpha:1.0f];
            }
            break;
        case 3:
            icon.image = [UIImage imageNamed:@"whitelist_user_icon.png"];
            titleLabel.text = LS(@"Whitelist");
            break;
        case 4:
            icon.image = [UIImage imageNamed:@"blacklist_user_icon.png"];
            titleLabel.text = LS(@"Blacklist");
            break;
        case 5:
            icon.image = [UIImage imageNamed:@"devices_user_icon.png"];
            titleLabel.text = LS(@"Devices");
            break;
        case 6:
            icon.image = [UIImage imageNamed:@"reports_user_icon.png"];
            titleLabel.text = LS(@"Reports");
            break;
        case 7:
            icon.image = [UIImage imageNamed:@"content_user_icon.png"];
            titleLabel.text = LS(@"ContentRule");
            break;
        case 8:
            icon.image = [UIImage imageNamed:@"time_rule_icon.png"];
            titleLabel.text = LS(@"AccessSchedule");
            break;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = bgColorView;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:kSegueIdToBlockScreen sender:nil];
            break;
        case 1:
            [self performSegueWithIdentifier:kSegueIdToExtendTimeScreen sender:nil];
            break;
        case 2:
            if(IS_IPHONE_4)
            {
                HHBaseViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"HHHomeworkViewController4"];
                [self.navigationController pushViewController:vc animated:YES];
            }
            else
                [self performSegueWithIdentifier:kSegueIdToHomeworkScreen sender:nil];
            break;
        case 3:
            [self performSegueWithIdentifier:kSegueIdToWhitelistScreen sender:nil];
            break;
        case 4:
            [self performSegueWithIdentifier:kSegueIdToBlacklistScreen sender:nil];
            break;
        case 5:
            [self performSegueWithIdentifier:kSegueIdToDevicesScreen sender:nil];
            break;
        case 6:
            [self performSegueWithIdentifier:kSegueIdToUserReportsScreen sender:nil];
            break;
        case 7:
            [self performSegueWithIdentifier:kSegueIdToContentScreen sender:nil];
            break;
        case 8:
            [self performSegueWithIdentifier:kSegueIdToTimeProfileScreen sender:nil];
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString*)getTimeStringWithStartDate:(NSDate*)date
{
    NSString *descriptionStr;
    
    NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:date];
    NSInteger distanceBetweenDatesInt = distanceBetweenDates;
    if (distanceBetweenDatesInt < 0)
        distanceBetweenDatesInt *= -1;
    NSInteger minutes = distanceBetweenDatesInt / 60;
    if (distanceBetweenDatesInt%60 > 30)
        minutes++;
    
    if (minutes > 120)
    {
        int hours = (int)minutes/60;
        if (minutes % 60 > 30)
            hours++;
        descriptionStr = [NSString stringWithFormat:@"%d %@", hours, LS(@"hours")];
    }
    else if (minutes <= 120 && minutes > 60)
    {
        int hours = (int)minutes/60;
        descriptionStr = [NSString stringWithFormat:@"%d %@ %d %@", hours, LS(@"hours"), (int)minutes%60, LS(@"mins")];
    }
    else if (minutes == 60)
    {
        descriptionStr = [NSString stringWithFormat:@"1 %@", LS(@"hours")];
    }
    else
    {
        descriptionStr = [NSString stringWithFormat:@"%d %@", (int)minutes, LS(@"minutes")];
    }
    return descriptionStr;
}

- (NSString*)getTimeStringWithStartDate:(NSDate*)date fromDate:(NSDate*)fromDate
{
    date = [HHUtils toLocalTime:date];
    fromDate = [HHUtils toLocalTime:fromDate];
    
    NSString *descriptionStr;
    
    NSTimeInterval distanceBetweenDates = [fromDate timeIntervalSinceDate:date];
    NSInteger distanceBetweenDatesInt = distanceBetweenDates;
    if (distanceBetweenDatesInt < 0)
        distanceBetweenDatesInt *= -1;
    NSInteger minutes = distanceBetweenDatesInt / 60;
    if (distanceBetweenDatesInt%60 > 30)
        minutes++;
    
    if (minutes > 120)
    {
        int hours = (int)minutes/60;
        if (minutes % 60 > 30)
            hours++;
        descriptionStr = [NSString stringWithFormat:@"%d %@", hours, LS(@"hours")];
    }
    else if (minutes <= 120 && minutes > 60)
    {
        int hours = (int)minutes/60;
        descriptionStr = [NSString stringWithFormat:@"%d %@ %d %@", hours, LS(@"hours"), (int)minutes%60, LS(@"mins")];
    }
    else if (minutes == 60)
    {
        descriptionStr = [NSString stringWithFormat:@"1 %@", LS(@"hours")];
    }
    else
    {
        descriptionStr = [NSString stringWithFormat:@"%ld %@", (long)minutes, LS(@"minutes")];
    }
    return descriptionStr;
}

@end
