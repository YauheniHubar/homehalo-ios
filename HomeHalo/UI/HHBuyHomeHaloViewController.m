//
//  HHBuyHomeHaloViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/14/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBuyHomeHaloViewController.h"
#import "HHAPI.h"

@interface HHBuyHomeHaloViewController ()

@property (nonatomic, weak) IBOutlet UIWebView *webView;

@end

@implementation HHBuyHomeHaloViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoBuyHomeHaloScreen") : LS(@"BuyHomeHaloScreen");
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];

    for (NSHTTPCookie *cookie in storage.cookies) {
        [storage deleteCookie:cookie];
    }

    [self.webView loadRequest:[[HHAPI sharedAPI] requestForWebView]];
}

@end
