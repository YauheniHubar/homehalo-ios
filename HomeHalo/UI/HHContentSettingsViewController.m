//
//  HHContentSettingsViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/20/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHContentSettingsViewController.h"
#import "HHContentProfileViewController.h"
#import "HHAPI.h"

static NSString *const kCategoriesKey = @"categories";
static NSString *const kCategoryNameKey = @"categoryName";
static NSString *const kContentSettingsCellIdentifier = @"contentSettingsCellIdentifier";

@interface HHContentSettingsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *settingsArray;
@property (nonatomic) NSMutableOrderedSet* categoryArray;
@property (nonatomic) NSMutableDictionary *sortedCategory;

@end

@implementation HHContentSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoContentSettingsScreen");
    } else {
        self.screenName = LS(@"ContentSettingsScreen");
    }

    self.titleLabel.text = self.titleContent;
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestContentCategoriesProfileWithSuccess:^(NSDictionary *responseObject) {

        weakSelf.settingsArray = [NSArray arrayWithArray:responseObject[kCategoriesKey]];
        [weakSelf sotrResponseByCategory];
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark -
#pragma mark - Private

- (void)sotrResponseByCategory
{
    self.categoryArray = [NSMutableOrderedSet new];
    for (NSDictionary *dict in self.settingsArray) {
        [self.categoryArray addObject:dict[kCategoryNameKey]];
    }
    
    self.sortedCategory = [NSMutableDictionary new];
    for (NSString *categoryName in self.categoryArray) {
        NSMutableArray *newArray = [NSMutableArray new];
        for (NSDictionary *dict in self.settingsArray) {
            if ([categoryName isEqualToString:dict[kCategoryNameKey]])
                [newArray addObject:dict];
        }
        (self.sortedCategory)[categoryName] = newArray;
    }
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.categoryArray.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.categoryArray[section];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = [UIColor whiteColor];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    (header.textLabel).textColor = [UIColor blackColor];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sortedCategory[self.categoryArray[section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kContentSettingsCellIdentifier];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    NSDictionary *rowInfo = self.sortedCategory[self.categoryArray[indexPath.section]][indexPath.row];
    titleLabel.text = rowInfo[@"title"];
    UIButton *checkMark = (UIButton *)[cell.contentView viewWithTag:2];
    [checkMark setSelected:NO];
    for (NSDictionary *dict in self.selectedCategory)
    {
        if ([rowInfo[@"id"] integerValue] == [dict[@"id"] integerValue])
        {
            [checkMark setSelected:YES];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIButton *checkMark = (UIButton *)[cell.contentView viewWithTag:2];
    checkMark.selected = !checkMark.selected;
    if (checkMark.selected)
        [self.selectedCategory addObject:self.sortedCategory[self.categoryArray[indexPath.section]][indexPath.row]];
    else
        [self.selectedCategory removeObject:self.sortedCategory[self.categoryArray[indexPath.section]][indexPath.row]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark - Action

- (IBAction)saveChangesBtnTouch:(id)sender {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    dict[@"title"] = self.categoryInfo[@"title"];
    
    NSMutableArray *categoryArray = [NSMutableArray new];

    for (NSDictionary *obj in self.selectedCategory) {
        NSDictionary *newObj = @{ @"categoryId" : obj[@"categoryId"] };
        [categoryArray addObject:newObj];
    }

    dict[@"contentCategories"] = categoryArray;
    
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestSaveChanges:dict profileID:[self.categoryInfo[@"id"] integerValue] success:^(NSDictionary *responseObject) {

        [weakSelf unwindToViewControllerOfClass:HHContentProfileViewController.class animated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

@end
