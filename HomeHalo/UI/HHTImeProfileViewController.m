//
//  HHTimeProfileViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 5/20/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHTimeProfileViewController.h"
#import "HHAPI.h"

#define redColor RGB(251, 9, 8)
#define greenColor RGB(112, 182, 0)

@interface HHTimeProfileViewController () <UIGestureRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
{
    NSInteger viewTagID;
    NSArray *localizeDayArray;
    NSArray *dayArray;
    NSMutableArray *timeLineArray;
    NSMutableSet* viewArray;
    CGPoint startPoint;
    CGPoint endPoint;
    BOOL isEdit;
    NSMutableArray *viewColorArray;
    NSMutableArray *viewSelectedColorArray;
    
    NSMutableArray *noChangesViewColorArray;
    
    NSMutableSet* littleViewArray;
    UIView *profileView2;
    NSInteger collectionViewIndex;
}

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UIButton *leftArrow;
@property (nonatomic, weak) IBOutlet UIButton *rigthArrow;
@property (nonatomic, weak) IBOutlet UIView *profileView;
@property (nonatomic, weak) NSArray *timeProfiles;

@property (nonatomic, weak) IBOutlet UIButton *editBtn;
@property (nonatomic, weak) IBOutlet UIButton *copToNextBtn;
@property (nonatomic, weak) IBOutlet UIButton *clearDayBtn;
@property (nonatomic, weak) IBOutlet UIButton *clearBtn;

@property (nonatomic, weak) IBOutlet UICollectionView* dayCollection;

@end

@implementation HHTimeProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nameLabel.text = [HHUserSettings sharedSettings].currentDeviceOwner[@"title"];
    [self.clearDayBtn setHidden:YES];
    [self.clearBtn setHidden:YES];
    [self.copToNextBtn setHidden:NO];
    isEdit = NO;
    viewArray = [NSMutableSet new];
    littleViewArray = [NSMutableSet new];
    
    localizeDayArray = @[LS(@"MONDAY"), LS(@"TUESDAY"), LS(@"WEDNESDAY"), LS(@"THURSDAY"), LS(@"FRIDAY"), LS(@"SATURDAY"), LS(@"SUNDAY")];
    dayArray = @[@"MONDAY", @"TUESDAY", @"WEDNESDAY", @"THURSDAY", @"FRIDAY", @"SATURDAY", @"SUNDAY"];
    self.timeProfiles = [HHUserSettings sharedSettings].currentDeviceOwner[@"timeProfiles"];
    
    timeLineArray = [NSMutableArray new];
    for (NSInteger i = 1; i <= 96; i++) {
        [timeLineArray addObject:@(i * 15)];
    }
    
    collectionViewIndex = self.day;
    [self configureTimeLine];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

#pragma mark -
#pragma mark - Touches

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(!isEdit)
        return;
    for (UITouch* touch in touches) {
        startPoint = [touch locationInView:self.profileView];
    }
    viewColorArray = [NSMutableArray new];
    for (UIView *view in viewArray)
    {
        UIView *littleView = view.subviews[0];
        if (littleView.backgroundColor == nil)
            [littleView setBackgroundColor:greenColor];
        [viewColorArray addObject:littleView.backgroundColor];
    }
    viewSelectedColorArray = [NSMutableArray arrayWithArray:viewColorArray];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(!isEdit)
        return;
    for (UITouch* touch in touches) {
        endPoint = [touch locationInView:self.profileView];
    }
    
    CGRect rect = CGRectMake(MIN(startPoint.x, endPoint.x),
                             MIN(startPoint.y, endPoint.y),
                             fabs(startPoint.x - endPoint.x),
                             fabs(startPoint.y - endPoint.y));
    for (NSInteger i = 0; i < viewArray.count; i++)
    {
        UIView *view = viewArray.allObjects[i];
        UIView *littleView = view.subviews[0];
        if (CGRectIntersectsRect(rect, view.frame))
        {
            if ([viewColorArray[i] isEqual:redColor])
            {
                [littleView setBackgroundColor:greenColor];
                viewSelectedColorArray[i] = greenColor;
            }
            else
            {
                [littleView setBackgroundColor:redColor];
                viewSelectedColorArray[i] = redColor;
            }
        }
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(!isEdit)
        return;
    for (UITouch* touch in touches) {
        endPoint = [touch locationInView:self.profileView];
    }
    CGRect rect = CGRectMake(MIN(startPoint.x, endPoint.x),
                          MIN(startPoint.y, endPoint.y),
                          fabs(startPoint.x - endPoint.x),
                          fabs(startPoint.y - endPoint.y));
    for (NSInteger i = 0; i < viewArray.count; i++)
    {
        UIView *view = viewArray.allObjects[i];
        UIView *littleView = view.subviews[0];
        if (CGRectIntersectsRect(rect, view.frame))
        {
            if ([viewColorArray[i] isEqual:redColor])
            {
                [littleView setBackgroundColor:greenColor];
                viewSelectedColorArray[i] = greenColor;
            }
            else
            {
                [littleView setBackgroundColor:redColor];
                viewSelectedColorArray[i] = redColor;
            }
        }
    }
    viewColorArray = [NSMutableArray arrayWithArray:viewSelectedColorArray];
    for (NSInteger i = 0; i < viewArray.count; i++)
    {
        UIView *view = viewArray.allObjects[i];
        UIView *littleView = view.subviews[0];
        littleView.backgroundColor = viewColorArray[i];
    }
}

#pragma mark -
#pragma mark - Actions

- (IBAction)leftBtnTouch:(id)sender
{
    if (self.day == 0)
    {
        self.day = 6;
        collectionViewIndex = 7;
        [self.dayCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:collectionViewIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
    else
        self.day--;
    collectionViewIndex--;
    NSMutableArray *profilesArray = [NSMutableArray new];
    for (NSDictionary *timeProfile in self.timeProfiles)
    {
        if ([[timeProfile[@"dayOfWeek"] lowercaseString] isEqualToString:[dayArray[self.day] lowercaseString]])
            [profilesArray addObject:timeProfile];
    }
    self.timeColorArray = [self timeLineColor:profilesArray];
    
    viewArray = [NSMutableSet new];
    
    NSInteger y = self.profileView.frame.origin.y;
    NSInteger w = self.view.frame.size.width;
    NSInteger h = self.profileView.frame.size.height;
    
    UIGraphicsBeginImageContext(self.profileView.frame.size);
    [self.profileView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageView *img = [[UIImageView alloc] initWithImage:viewImage];
    
    [self configureTimeLine];
    profileView2 = [[UIView alloc] initWithFrame:CGRectMake(0, y, w, h)];
    self.profileView.frame = CGRectMake(-w, y, w, h);
    [self.view addSubview:profileView2];
    
    [profileView2 addSubview:img];
    [UIView animateWithDuration:0.3f
                          delay:0.f
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.profileView.frame = CGRectMake(0, y, w, h);
                         profileView2.frame = CGRectMake(w, y, w, h);
                     }
                     completion:^(BOOL finished){
                         [profileView2 removeFromSuperview];
                         profileView2 = nil;
                     }];
}

- (IBAction)rightBtnTouch:(id)sender
{
    if (self.day == dayArray.count-1)
    {
        self.day = 0;
        collectionViewIndex = 6;
        [self.dayCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:collectionViewIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
    else
        self.day++;
    collectionViewIndex++;
    NSMutableArray *profilesArray = [NSMutableArray new];
    for (NSDictionary *timeProfile in self.timeProfiles)
    {
        if ([[timeProfile[@"dayOfWeek"] lowercaseString] isEqualToString:[dayArray[self.day] lowercaseString]])
            [profilesArray addObject:timeProfile];
    }
    self.timeColorArray = [self timeLineColor:profilesArray];
    
    viewArray = [NSMutableSet new];
    
    NSInteger y = self.profileView.frame.origin.y;
    NSInteger w = self.view.frame.size.width;
    NSInteger h = self.profileView.frame.size.height;
    
    UIGraphicsBeginImageContext(self.profileView.frame.size);
    [self.profileView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageView *img = [[UIImageView alloc] initWithImage:viewImage];
    
    [self configureTimeLine];
    profileView2 = [[UIView alloc] initWithFrame:CGRectMake(0, y, w, h)];
    self.profileView.frame = CGRectMake(self.view.frame.size.width, y, w, h);
    [self.view addSubview:profileView2];
    
    [profileView2 addSubview:img];
    [UIView animateWithDuration:0.3f
                          delay:0.f
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.profileView.frame = CGRectMake(0, y, w, h);
                         profileView2.frame = CGRectMake(-w, y, w, h);
                     }
                     completion:^(BOOL finished){
                         [profileView2 removeFromSuperview];
                         profileView2 = nil;
                     }];
}

- (IBAction)editBtnTouch:(id)sender
{
    isEdit = !isEdit;
    [self.editBtn setTitle:isEdit ? LS(@"Save") : LS(@"Edit") forState:UIControlStateNormal];
        
    if (isEdit)
    {
        noChangesViewColorArray = [NSMutableArray new];
        for (UIView *view in littleViewArray)
        {
            [noChangesViewColorArray addObject:view.backgroundColor];
        }
        [self.clearDayBtn setHidden:NO];
        [self.clearBtn setHidden:NO];
        [self.copToNextBtn setHidden:YES];
        [self.leftArrow setEnabled:NO];
        [self.rigthArrow setEnabled:NO];
    }
    else
    {
        noChangesViewColorArray = nil;
        [self requestTimeProfile];
        [self.clearDayBtn setHidden:YES];
        [self.clearBtn setHidden:YES];
        [self.copToNextBtn setHidden:NO];
        [self.leftArrow setEnabled:YES];
        [self.rigthArrow setEnabled:YES];
    }
}

- (IBAction)copyBtnTouch:(id)sender
{
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
    NSMutableArray *timeProfilesArray = [NSMutableArray arrayWithArray:data[@"timeProfiles"]];
    [data removeObjectForKey:@"timeProfiles"];
    NSString *block = data[@"blockStartedAt"][@"dateTime"];
    [data removeObjectForKey:@"blockStartedAt"];
    if (block)
        data[@"blockStartedAt"] = block;
    
    NSString *homework = data[@"homeworkModeStartedAt"][@"dateTime"];
    [data removeObjectForKey:@"homeworkModeStartedAt"];
    if (homework)
        data[@"homeworkModeStartedAt"] = homework;
    
    NSString *contentProfile = data[@"contentProfile"][@"id"];
    [data removeObjectForKey:@"contentProfile"];
    data[@"contentProfile"] = contentProfile;
    NSMutableArray *copyArray = [NSMutableArray new];
    
    for (NSInteger i = 0; i < timeProfilesArray.count; i++)
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:timeProfilesArray[i]];
        if ([dict[@"dayOfWeek"] isEqualToString:[dayArray[self.day == 6 ? 0 : self.day+1] lowercaseString]])
        {
            [timeProfilesArray removeObject:dict];
            i--;
            continue;
        }
        NSString *str1 = dict[@"startTime"][@"time"];
        NSString *str2 = dict[@"endTime"][@"time"];
        [dict removeObjectForKey:@"id"];
        [dict removeObjectForKey:@"startTime"];
        [dict removeObjectForKey:@"endTime"];
        dict[@"startTime"] = str1;
        dict[@"endTime"] = str2;
        timeProfilesArray[i] = dict;
        NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dict];
        if ([newDict[@"dayOfWeek"] isEqualToString:[dayArray[self.day] lowercaseString]])
        {
            [newDict removeObjectForKey:@"dayOfWeek"];
            newDict[@"dayOfWeek"] = [dayArray[self.day == 6 ? 0 : self.day+1] lowercaseString];
            [copyArray addObject:newDict];
        }
    }
    [timeProfilesArray addObjectsFromArray:copyArray];
    [data setValue:timeProfilesArray forKey:@"timeProfiles"];
    
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestBlockUser:data success:^(NSDictionary *responseObject) {

        NSMutableDictionary *deviceOwner = [NSMutableDictionary dictionaryWithDictionary:weakSelf.userSettings.currentDeviceOwner];
        [deviceOwner removeObjectForKey:@"timeProfiles"];
        deviceOwner[@"timeProfiles"] = responseObject[@"deviceOwner"][@"timeProfiles"];
        weakSelf.userSettings.currentDeviceOwner = deviceOwner;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateTimeProfile" object:nil];
        weakSelf.timeProfiles = deviceOwner[@"timeProfiles"];
        [weakSelf rightBtnTouch:nil];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [weakSelf clearChanges:nil];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (IBAction)clearChanges:(id)sender
{
    for (NSInteger i = 0; i < littleViewArray.count; i++)
    {
        ((UIView*)littleViewArray.allObjects[i]).backgroundColor = noChangesViewColorArray[i];
    }
    
    isEdit = !isEdit;
    [self.editBtn setTitle:isEdit ? LS(@"Save") : LS(@"Edit") forState:UIControlStateNormal];
    [self.clearDayBtn setHidden:YES];
    [self.clearBtn setHidden:YES];
    [self.copToNextBtn setHidden:NO];
    [self.leftArrow setEnabled:YES];
    [self.rigthArrow setEnabled:YES];
}

- (IBAction)clearDayBtnTouch:(id)sender
{
    for (NSInteger i = 0; i < littleViewArray.count; i++)
    {
        [(UIView*)littleViewArray.allObjects[i] setBackgroundColor:greenColor];
    }
}

#pragma mark -
#pragma mark - ConfigureTimeLine

- (void)configureTimeLine
{
    viewTagID = 0;
    for (UIView *view in (self.profileView).subviews)
    {
        [view removeFromSuperview];
    }
    [self drawDayTimeLine];
    
    viewArray = [NSMutableSet new];
    littleViewArray = [NSMutableSet new];
    
    [self.dayCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:collectionViewIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    CGFloat offsetY = (self.view.frame.size.height-213)/6.f;
    CGFloat size = (self.view.frame.size.height-213)/6.f;
    
    //draw 1 line
    NSArray *timeArray = @[@"12", @"1", @"2", @"3", @"4", @"5"];
    NSMutableArray *newTimeArray = [NSMutableArray new];
    for (int i = 0; i < 24; i++) {
        [newTimeArray addObject:self.timeColorArray[i]];
    }
    [self drawLabelsLittle:offsetY timeLabels:timeArray colorArray:newTimeArray];
    
    //draw 2 line
    offsetY += size;
    timeArray = @[@"6am", @"7am", @"8am"];
    newTimeArray = [NSMutableArray new];
    for (int i = 24; i < 36; i++) {
        [newTimeArray addObject:self.timeColorArray[i]];
    }
    [self drawLabelsGreat:offsetY timeLabels:timeArray colorArray:newTimeArray];
    
    //draw 3 line
    offsetY += size;
    timeArray = @[@"9", @"10", @"11", @"12", @"1", @"2"];
    newTimeArray = [NSMutableArray new];
    for (int i = 36; i < 60; i++) {
        [newTimeArray addObject:self.timeColorArray[i]];
    }
    [self drawLabelsLittle:offsetY timeLabels:timeArray colorArray:newTimeArray];
    
    //draw 4 line
    offsetY += size;
    timeArray = @[@"3pm", @"4pm", @"5pm"];
    newTimeArray = [NSMutableArray new];
    for (int i = 60; i < 72; i++) {
        [newTimeArray addObject:self.timeColorArray[i]];
    }
    [self drawLabelsGreat:offsetY timeLabels:timeArray colorArray:newTimeArray];
    
    //draw 5 line
    offsetY += size;
    timeArray = @[@"6pm", @"7pm", @"8pm"];
    newTimeArray = [NSMutableArray new];
    for (int i = 72; i < 84; i++) {
        [newTimeArray addObject:self.timeColorArray[i]];
    }
    [self drawLabelsGreat:offsetY timeLabels:timeArray colorArray:newTimeArray];
    
    //draw 6 line
    offsetY += size;
    timeArray = @[@"9pm", @"10pm", @"11pm"];
    newTimeArray = [NSMutableArray new];
    for (int i = 84; i < 96; i++) {
        [newTimeArray addObject:self.timeColorArray[i]];
    }
    [self drawLabelsGreat:offsetY timeLabels:timeArray colorArray:newTimeArray];
}

#pragma mark -
#pragma mark - Draw methods

- (void)drawLabelsLittle:(CGFloat)offsetY timeLabels:(NSArray*)timeArray colorArray:(NSArray*)colorArray
{
    NSInteger index = 0;
    CGFloat viewSize = (self.view.frame.size.height-213)/6.f;
    CGFloat yValue = offsetY;
    for (int i = 0; i < 6; i++)
    {
        CGFloat xOffset = self.view.frame.size.width/6;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xOffset*i, offsetY-viewSize, xOffset, viewSize - (viewSize/4))];
        label.font = [UIFont boldSystemFontOfSize:27];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = timeArray[i];
        label.textColor = [UIColor colorWithRed:0.7 green:0.9 blue:0.47 alpha:1];
        label.adjustsFontSizeToFitWidth = YES;
        
        for (int j = 0; j < 4; j++)
        {
            CGFloat littleOffset = (xOffset/4)*j;
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xOffset*i+littleOffset, offsetY-viewSize, xOffset/4 - 1, viewSize)];
            view.backgroundColor = [UIColor whiteColor];
            UIView *littleView = [[UIView alloc] initWithFrame:
                                  CGRectMake(0,
                                             viewSize - (viewSize/4),
                                             view.frame.size.width,
                                             view.frame.size.height/4)];
            if ([colorArray[(j+index*4)] isEqual:redColor])
                [littleView setBackgroundColor:redColor];
            else
                [littleView setBackgroundColor:greenColor];
            view.tag = viewTagID;
            littleView.tag = viewTagID;
            viewTagID++;
            [view addSubview:littleView];
            [littleViewArray addObject:littleView];
            [viewArray addObject:view];
            [self.profileView addSubview:view];
            [self.profileView sendSubviewToBack:view];
        }
        [self.profileView addSubview:label];
        index++;
    }
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(self.view.frame.size.width, yValue-offsetY)];
    [path addLineToPoint:CGPointMake(0, yValue-offsetY)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = path.CGPath;
    shapeLayer.strokeColor = [UIColor colorWithRed:0.38 green:0.38 blue:0.38 alpha:1].CGColor;
    shapeLayer.lineWidth = 3.0;
    
    [self.profileView.layer addSublayer:shapeLayer];
}

- (void)drawLabelsGreat:(CGFloat)offsetY timeLabels:(NSArray*)timeArray colorArray:(NSArray*)colorArray
{
    NSInteger index = 0;
    CGFloat viewSize = (self.view.frame.size.height-213)/6.f;
    CGFloat yValue = offsetY;
    for (int i = 0; i < 3; i++)
    {
        CGFloat xOffset = self.view.frame.size.width/3;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xOffset*i, offsetY-viewSize, xOffset, viewSize - (viewSize/4))];
        label.font = [UIFont boldSystemFontOfSize:35];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = timeArray[i];
        label.textColor = [UIColor colorWithRed:0.7 green:0.9 blue:0.47 alpha:1];
        label.adjustsFontSizeToFitWidth = YES;
        
        for (int j = 0; j < 4; j++)
        {
            CGFloat littleOffset = (xOffset/4)*j;
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xOffset*i+littleOffset, offsetY-viewSize, xOffset/4 - 1, viewSize)];
            view.backgroundColor = [UIColor whiteColor];
            UIView *littleView = [[UIView alloc] initWithFrame:
                                  CGRectMake(0,
                                             viewSize - (viewSize/4),
                                             view.frame.size.width,
                                             view.frame.size.height/4)];
            if ([colorArray[(j+index*4)] isEqual:redColor])
                [littleView setBackgroundColor:redColor];
            else
                [littleView setBackgroundColor:greenColor];
            view.tag = viewTagID;
            littleView.tag = viewTagID;
            viewTagID++;
            [view addSubview:littleView];
            [littleViewArray addObject:littleView];
            [viewArray addObject:view];
            [self.profileView addSubview:view];
            [self.profileView sendSubviewToBack:view];
        }
        [self.profileView addSubview:label];
        index++;
    }
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(self.view.frame.size.width, yValue-offsetY)];
    [path addLineToPoint:CGPointMake(0, yValue-offsetY)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = path.CGPath;
    shapeLayer.strokeColor = [UIColor colorWithRed:0.38 green:0.38 blue:0.38 alpha:1].CGColor;
    shapeLayer.lineWidth = 3.0;
    
    [self.profileView.layer addSublayer:shapeLayer];
}

- (void)drawDayTimeLine
{
    CGFloat offsetY = (self.view.frame.size.height-213)/6.f;
    for (int j = 1; j < 7; j++)
    {
        CGFloat yValue = offsetY*j;
        for (int i = 0; i < 13; i++)
        {
            if (j == 1 || j == 3) {
                if (i % 2 != 0) continue;
            }
            else {
                if (i % 4 != 0) continue;
            }
            CGFloat offset = self.view.frame.size.width/12.f;
            CGFloat xValue = offset*i;
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:CGPointMake(xValue, yValue)];
            [path addLineToPoint:CGPointMake(xValue, yValue-offsetY)];
            
            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
            shapeLayer.path = path.CGPath;
            shapeLayer.strokeColor = [UIColor colorWithRed:0.38 green:0.38 blue:0.38 alpha:1].CGColor;
            shapeLayer.lineWidth = 3.f;
            
            [self.profileView.layer addSublayer:shapeLayer];
        }
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(self.view.frame.size.width, yValue-offsetY)];
        [path addLineToPoint:CGPointMake(0, yValue-offsetY)];
        
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer.path = path.CGPath;
        shapeLayer.strokeColor = [UIColor colorWithRed:0.38 green:0.38 blue:0.38 alpha:1].CGColor;
        shapeLayer.lineWidth = 3.0;
        
        [self.profileView.layer addSublayer:shapeLayer];
    }
    
    CGFloat yValue = offsetY*7;
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(self.view.frame.size.width, yValue-offsetY)];
    [path addLineToPoint:CGPointMake(0, yValue-offsetY)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = path.CGPath;
    shapeLayer.strokeColor = [UIColor colorWithRed:0.38 green:0.38 blue:0.38 alpha:1].CGColor;
    shapeLayer.lineWidth = 3.0;
    
    [self.profileView.layer addSublayer:shapeLayer];
}

- (void)sendSublayerToBack:(CALayer *)layer {
    CALayer *superlayer = layer.superlayer;
    [layer removeFromSuperlayer];
    [superlayer insertSublayer:layer atIndex:0];
}

- (NSArray*)timeLineColor:(NSArray*)profileArray
{
    NSMutableArray *colorsArray = [NSMutableArray new];
    for (int i = 0; i < 96; i++)
    {
        [colorsArray addObject:redColor];
    }
    
    for (NSDictionary *profile in profileArray)
    {
        NSString *startTime = profile[@"startTime"][@"time"];
        NSString *startHour= [startTime substringToIndex:2];
        NSInteger hour = startHour.integerValue;
        
        NSString *startMinutes= [[startTime substringFromIndex:startTime.length - 5] substringToIndex:2];
        NSInteger minutes = startMinutes.integerValue;
        
        NSString *endTime = profile[@"endTime"][@"time"];
        NSString *endHour= [endTime substringToIndex:2];
        NSInteger hourEnd = endHour.integerValue;
        
        NSString *endMinutes= [[endTime substringFromIndex:endTime.length - 5] substringToIndex:2];
        NSInteger minutesEnd = endMinutes.integerValue;
        
        
        NSInteger start = hour*60 + minutes;
        NSInteger end = hourEnd*60 + minutesEnd;
        
        for (NSInteger i = 0; i < timeLineArray.count; i++)
        {
            NSNumber* time = timeLineArray[i];
            if (time.integerValue > start && time.integerValue <= end)
                colorsArray[i] = greenColor;
            
            if (i == timeLineArray.count-1)
                if (time.integerValue > start && time.integerValue <= end+1)
                    colorsArray[i] = greenColor;
        }
    }
    return colorsArray;
}

#pragma mark -
#pragma mark - Request

- (void)requestTimeProfile
{
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:[HHUserSettings sharedSettings].currentDeviceOwner];
    NSMutableArray *timeProfilesArray = [NSMutableArray arrayWithArray:data[@"timeProfiles"]];
    [data removeObjectForKey:@"timeProfiles"];
    NSString *block = data[@"blockStartedAt"][@"dateTime"];
    [data removeObjectForKey:@"blockStartedAt"];
    if (block)
        data[@"blockStartedAt"] = block;
    
    NSString *homework = data[@"homeworkModeStartedAt"][@"dateTime"];
    [data removeObjectForKey:@"homeworkModeStartedAt"];
    if (homework)
        data[@"homeworkModeStartedAt"] = homework;
    
    NSString *contentProfile = data[@"contentProfile"][@"id"];
    [data removeObjectForKey:@"contentProfile"];
    data[@"contentProfile"] = contentProfile;
    
    for (NSInteger i = 0; i < timeProfilesArray.count; i++)
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:timeProfilesArray[i]];
        if ([dict[@"dayOfWeek"] isEqualToString:[dayArray[self.day] lowercaseString]])
        {
            [timeProfilesArray removeObject:dict];
            i--;
            continue;
        }
        NSString *str1 = dict[@"startTime"][@"time"];
        NSString *str2 = dict[@"endTime"][@"time"];
        [dict removeObjectForKey:@"id"];
        [dict removeObjectForKey:@"startTime"];
        [dict removeObjectForKey:@"endTime"];
        dict[@"startTime"] = str1;
        dict[@"endTime"] = str2;
        timeProfilesArray[i] = dict;
    }
    NSNumber* startValue = nil;
    NSNumber* endValue = nil;
    NSArray *sortedArray = [viewArray.allObjects sortedArrayUsingComparator:^NSComparisonResult(UIView *obj1, UIView *obj2) {
        return obj1.tag > obj2.tag;
    }];
    NSArray *sortedLittleArray = [littleViewArray.allObjects sortedArrayUsingComparator:^NSComparisonResult(UIView *obj1, UIView *obj2) {
        return obj1.tag > obj2.tag;
    }];
    
    for(NSInteger i = 0; i < sortedLittleArray.count; i++)
    {
        UIView *view = sortedLittleArray[i];
        if ([view.backgroundColor isEqual:greenColor] && startValue == nil)
        {
            startValue = @(i);
        }
        else if ([view.backgroundColor isEqual:redColor] && startValue != nil)
        {
            endValue = @(i);

            NSMutableDictionary *obj = [NSMutableDictionary new];
            [obj setValue:[dayArray[self.day] lowercaseString] forKey:@"dayOfWeek"];
            [obj setValue:[self getTime:startValue.integerValue] forKey:@"startTime"];
            [obj setValue:[self getTime:endValue.integerValue] forKey:@"endTime"];
            
            [timeProfilesArray addObject:obj];
            startValue = nil;
            endValue = nil;
        }
        
        if (i == sortedArray.count - 1 && startValue != nil)
        {
            endValue = @(i+1);
            
            NSMutableDictionary *obj = [NSMutableDictionary new];
            [obj setValue:[dayArray[self.day] lowercaseString] forKey:@"dayOfWeek"];
            [obj setValue:[self getTime:startValue.integerValue] forKey:@"startTime"];
            [obj setValue:[self getTime:endValue.integerValue] forKey:@"endTime"];
            
            [timeProfilesArray addObject:obj];
            startValue = nil;
            endValue = nil;
        }
    }
    [data setValue:timeProfilesArray forKey:@"timeProfiles"];
    
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestBlockUser:data success:^(NSDictionary *responseObject) {

        NSMutableDictionary *deviceOwner = [NSMutableDictionary dictionaryWithDictionary:weakSelf.userSettings.currentDeviceOwner];
        [deviceOwner removeObjectForKey:@"timeProfiles"];
        deviceOwner[@"timeProfiles"] = responseObject[@"deviceOwner"][@"timeProfiles"];

        weakSelf.userSettings.currentDeviceOwner = deviceOwner;
        weakSelf.timeProfiles = deviceOwner[@"timeProfiles"];

        viewColorArray = [NSMutableArray new];
        viewSelectedColorArray = [NSMutableArray new];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [weakSelf clearChanges:nil];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark -
#pragma mark - Helper method

- (NSString*)getTime:(NSInteger)value
{
    value *= 15;
    
    if (value % 60 == 0)
    {
        if (value == 1440)
            return @"23:59:59";
        else if (value/60 < 10)
            return [NSString stringWithFormat:@"0%d:00:00", (int)value/60];
        return [NSString stringWithFormat:@"%d:00:00", (int)value/60];
    }
    else if (value < 60)
        return [NSString stringWithFormat:@"00:%d:00", (int)value%60];
    else
    {
        NSInteger hour = value/60;
        NSInteger minutes = value - hour*60;
        if (value < 10)
            return [NSString stringWithFormat:@"0%d:%d:00", (int)hour, (int)minutes];
        else
            return [NSString stringWithFormat:@"%d:%d:00", (int)hour, (int)minutes];
    }
    return @"";
}

#pragma mark -
#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return dayArray.count*2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"kCollectionViewCell" forIndexPath:indexPath];
    UILabel *title = (UILabel *)[cell.contentView viewWithTag:1];
    NSInteger index = indexPath.row;
    if (index >= dayArray.count)
        index -= dayArray.count;
    title.text = localizeDayArray[index];
    return cell;
}

@end
