//
//  HHHomeViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/9/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHHomeViewController.h"
#import "MKNumberBadgeView.h"
#import "MYIntroductionView.h"
#import "HHUser.h"
#import "HHWedge.h"
#import "HHAPI+Auth.h"
#import "HHUserResponse.h"
#import "HHSubscription.h"
#import "HHAPI+Alerts.h"
#import "HHNotificationsResponse.h"
#import "HHAPI+Devices.h"

static NSString *const kSegueIdToUsersScreen = @"goToUsersSegue";
static NSString *const kSegueIdToReportsScreen = @"goToReportsSegue";
static NSString *const kSegueIdToAlertsScreen = @"goToAlertsSegue";
static NSString *const kSegueIdToMoreScreen = @"goToMoreSegue";
static NSString *const kSegueIdBuyHomeHalo = @"buyHomeHaloSegue";

@interface HHHomeViewController () <UIGestureRecognizerDelegate, UIAlertViewDelegate, MYIntroductionDelegate, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *routerStatusLabel;
@property (nonatomic, weak) IBOutlet UIImageView *routerImage;
@property (nonatomic, weak) IBOutlet UIButton *findOutMore;
@property (nonatomic, weak) IBOutlet MKNumberBadgeView *badgeOne;

@property (nonatomic) NSTimer *updateUserInfoTimer;

@end

@implementation HHHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [HHUtils checkInternetConnection];
    
    BOOL isDemoMode = self.userSettings.isDemoMode;

    self.screenName = isDemoMode ? LS(@"DemoHomeScreen") : LS(@"HomeScreen");
    
    self.badgeOne.fillColor = [UIColor whiteColor];
    self.badgeOne.hideWhenZero = YES;
    self.badgeOne.textColor = RGB(130, 115, 110);
    self.badgeOne.value = 0;

    [self updateRouterStatus];

    self.updateUserInfoTimer = [NSTimer scheduledTimerWithTimeInterval:60.0f target:self selector:@selector(updateInfo) userInfo:nil repeats:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopUpdateUserInfoTimer) name:HHLogoutNotification object:nil];

    if (isDemoMode) {
        [self configureHelp];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.navigationController.navigationBarHidden = YES;

    if (![HHUserSettings sharedSettings].currentUser || [HHUserSettings sharedSettings].currentUser.notificationCount.integerValue == 0) {
        __weak __typeof(self) weakSelf = self;

        if ([HHUserSettings sharedSettings].isDemoMode) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[HHAPI sharedAPI] requestAlertsWithSuccess:^(HHNotificationsResponse *notificationsResponse) {

                NSUInteger notificationsCount = notificationsResponse.notifications.count;
                weakSelf.badgeOne.value = notificationsCount;
                [UIApplication sharedApplication].applicationIconBadgeNumber = notificationsCount;

                if (notificationsCount > 0) {
                    weakSelf.badgeOne.hidden = NO;
                }
                
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            } failure:^(NSError *error) {

                [HHUtils alertErrorMessage:error.localizedDescription];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            }];
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[HHAPI sharedAPI] requestDeviceOwnersWithSuccess:^(NSDictionary *responseObject) {

                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            } failure:^(NSError *error) {

                [HHUtils alertErrorMessage:error.localizedDescription];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            }];
        } else {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[HHAPI sharedAPI] requestUserWithUser:[HHUserSettings sharedSettings].currentUser success:^(HHUserResponse *userResponse) {

                [HHUserSettings sharedSettings].currentUser = userResponse.user;
                NSUInteger notificationsCount = userResponse.user.notificationCount.unsignedIntegerValue;
                BOOL isSubscriptionActive = userResponse.user.subscription.isActive;
                weakSelf.badgeOne.value = notificationsCount;
                [UIApplication sharedApplication].applicationIconBadgeNumber = notificationsCount;

                if (notificationsCount > 0) {
                    weakSelf.badgeOne.hidden = NO;
                }

                [self updateRouterStatus];

                if (!isSubscriptionActive) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"YourSubscriptionHasExpired") message:@"" delegate:self cancelButtonTitle:LS(@"Buy") otherButtonTitles:LS(@"OK"), nil];
                    [alert show];
                }

                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            } failure:^(NSError *error) {

                [HHUtils alertErrorMessage:error.localizedDescription];
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

            }];
        }
    } else {
        NSUInteger notificationsCount = [HHUserSettings sharedSettings].currentUser.notificationCount.unsignedIntegerValue;

        if (notificationsCount > 0) {
            self.badgeOne.hidden = NO;
        }

        self.badgeOne.value = notificationsCount;
        [self updateRouterStatus];
    }
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self performSegueWithIdentifier:kSegueIdBuyHomeHalo sender:nil];
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return NO;
}

- (void)updateInfo {
    if (![HHUserSettings sharedSettings].isDemoMode) {
        __weak __typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestUserWithUser:[HHUserSettings sharedSettings].currentUser success:^(HHUserResponse *userResponse) {

            [HHUserSettings sharedSettings].currentUser = userResponse.user;
            NSUInteger notificationsCount = userResponse.user.notificationCount.unsignedIntegerValue;
            weakSelf.badgeOne.value = notificationsCount;
            [UIApplication sharedApplication].applicationIconBadgeNumber = notificationsCount;

            if (notificationsCount > 0) {
                weakSelf.badgeOne.hidden = NO;
            }

            [weakSelf updateRouterStatus];

            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }
}

- (void)updateRouterStatus {
    BOOL isDemoMode = self.userSettings.isDemoMode;
    HHUser *currentUser = self.userSettings.currentUser;
    BOOL shouldShowRouterLabel = currentUser && currentUser.hasFullAccess && currentUser.wedge && !isDemoMode;

    if (shouldShowRouterLabel) {
        BOOL isWedgeOnline = currentUser.wedge.isOnline;
        self.routerStatusLabel.text = isWedgeOnline ? LS(@"Active") : LS(@"DeviceDisconnected");
    }

    self.routerImage.hidden = !shouldShowRouterLabel;
    self.routerStatusLabel.hidden = !shouldShowRouterLabel;
    self.findOutMore.hidden = shouldShowRouterLabel;
}

- (void)stopUpdateUserInfoTimer {
    [self.updateUserInfoTimer invalidate];
}


#pragma mark - Help

- (void)configureHelp {
    if (![HHUserSettings sharedSettings].isHelpShown) {
        MYIntroductionPanel *panel1 = [[MYIntroductionPanel alloc] initWithimage:[UIImage imageNamed:@"1"] title:LS(@"WelcomeToHomeHalo") description:LS(@"ManageAllYourHouseholdUsers")];
        MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc] initWithimage:[UIImage imageNamed:@"2"] title:nil description:LS(@"QuicklySeeAllYourUsers")];
        MYIntroductionPanel *panel3 = [[MYIntroductionPanel alloc] initWithimage:[UIImage imageNamed:@"3"] title:nil description:LS(@"ActivateAMode")];
        MYIntroductionPanel *panel4 = [[MYIntroductionPanel alloc] initWithimage:[UIImage imageNamed:@"4"] title:nil description:LS(@"ChangeSettingsInSeconds")];
        MYIntroductionPanel *panel5 = [[MYIntroductionPanel alloc] initWithTitle:LS(@"OneLastThing") description:LS(@"TellUsYourEmailToReceive") textFieldDelegate:self];
    
        MYIntroductionView *introductionView = [[MYIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
                                                                              headerText:nil
                                                                                  panels:@[panel1, panel2, panel3, panel4, panel5]
                                                                       languageDirection:MYLanguageDirectionLeftToRight];
        introductionView.backgroundColor = [UIColor colorWithRed:130.0/255 green:115.0/255 blue:110.0/255 alpha:1.0];
        introductionView.BackgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        introductionView.HeaderImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        introductionView.HeaderLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        introductionView.HeaderView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        introductionView.PageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        introductionView.SkipButton.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        introductionView.delegate = self;
        [introductionView showInView:self.view animateDuration:0.0];
    
        if (![HHUserSettings sharedSettings].currentUser.emailStepSkipButtonFlag || ![HHUserSettings sharedSettings].currentUser.emailStepSkipButtonFlag.boolValue) {
            [introductionView.SkipButtonX removeFromSuperview];
        }

        [HHUserSettings sharedSettings].isHelpShown = YES;
    }
}


#pragma mark - Action

- (IBAction)alertsBtnTouch:(id)sender {
    [self performSegueWithIdentifier:kSegueIdToAlertsScreen sender:nil];
}

- (IBAction)usersBtnTouch:(id)sender {
    [self performSegueWithIdentifier:kSegueIdToUsersScreen sender:nil];
}

- (IBAction)moreBtnTouch:(id)sender {
    [self performSegueWithIdentifier:kSegueIdToMoreScreen sender:nil];
}

- (IBAction)reportsBtnTouch:(id)sender {
    [self performSegueWithIdentifier:kSegueIdToReportsScreen sender:nil];
}

- (IBAction)findOutMoreBtnTouch:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.homehalo.net/iosapp"]];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    return YES;
}

@end
