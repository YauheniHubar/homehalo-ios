//
//  HHTimeProfileOverviewViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 5/18/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@interface HHTimeProfileOverviewViewController : HHBaseViewController

@property (nonatomic) BOOL isReturnToUsers;

@end
