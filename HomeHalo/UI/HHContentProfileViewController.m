//
//  HHContentProfileViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/20/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHContentProfileViewController.h"
#import "HHContentSettingsViewController.h"
#import "HHAPI.h"

static NSString *const kProfilesKey = @"profiles";
static NSString *const kContentProfileCellIdentifier = @"contentProfileCellIdentifier";
static NSString *const kSegueIdToContentSettingsScreen = @"goToContentSettingsSegue";

@interface HHContentProfileViewController () <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic) NSMutableArray *contentProfileArray;
@property (nonatomic) NSString *weakString;

@end

@implementation HHContentProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoContentProfileScreen") : LS(@"ContentProfileScreen");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestContentProfileWithSuccess:^(NSDictionary *responseObject) {

        weakSelf.contentProfileArray = [NSMutableArray arrayWithArray:responseObject[kProfilesKey]];
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueIdToContentSettingsScreen])
    {
        HHContentSettingsViewController* vc = segue.destinationViewController;
        vc.selectedCategory = [[NSMutableSet alloc] initWithArray:sender[@"contentCategories"]];
        vc.categoryInfo = sender;
        vc.titleContent = sender[@"title"];
    }
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.contentProfileArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kContentProfileCellIdentifier];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    titleLabel.text = self.contentProfileArray[indexPath.row][@"title"];

    UIView *dividerView = (UIView *)[cell.contentView viewWithTag:2];
    [dividerView setHidden:NO];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:kSegueIdToContentSettingsScreen sender:self.contentProfileArray[indexPath.row]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark - Actions

- (IBAction)addContentProfile:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"AddaNewProfile")
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:LS(@"Cancel")
                                          otherButtonTitles:LS(@"Create"), nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].autocapitalizationType = UITextAutocapitalizationTypeWords;
    [alert show];
    alert.tag = 10;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10 && buttonIndex == 1)
    {
        
        for (NSDictionary *dict in self.contentProfileArray)
        {
            NSString *title = dict[@"title"];
            if ([title.lowercaseString isEqualToString:[alertView textFieldAtIndex:0].text.lowercaseString])
            {
                [HHUtils alertWithTitle:[NSString stringWithFormat:@"%@ %@ %@", LS(@"ContentRule"), [alertView textFieldAtIndex:0].text, LS(@"AlreadyExists")] message:nil];
                return;
            }
        }
        __weak __typeof(self) weakSelf = self;
        self.weakString = [alertView textFieldAtIndex:0].text;
        [[alertView textFieldAtIndex:0] resignFirstResponder];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestAddContentProfile:[alertView textFieldAtIndex:0].text success:^(NSDictionary *responseObject) {

            [weakSelf.contentProfileArray addObject:responseObject[@"contentProfile"]];
            [weakSelf.tableView reloadData];

            for (NSDictionary *dict in weakSelf.contentProfileArray) {
                if ([dict[@"title"] isEqualToString:weakSelf.weakString]) {
                    NSInteger row = [weakSelf.contentProfileArray indexOfObject:dict];
                    [weakSelf tableView:weakSelf.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];

                    break;
                }
            }

            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];

    }
}

@end
