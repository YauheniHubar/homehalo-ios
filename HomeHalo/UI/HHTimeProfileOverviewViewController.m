//
//  HHTimeProfileOverviewViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 5/18/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHTimeProfileOverviewViewController.h"
#import "HHTimeProfileViewController.h"
#import "HHUsersViewController.h"

#define redColor RGB(251, 9, 8)
#define greenColor RGB(112, 182, 0)

static NSString *const kTimeProfileTimeCell = @"kTimeProfileTimeCell";
static NSString *const kTimeProfileDaysCell = @"kTimeProfileDaysCell";
static NSString *const kGoToTimeProfileSegue = @"kGoToTimeProfileSegue";
static NSString *const kUsersPageStoryboardID = @"kUsersPageStoryboardID";

@interface HHTimeProfileOverviewViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSInteger cellHeight;
    NSArray *dayArray;
    NSArray *localizeDayArray;
    NSMutableArray *timeArray;
    
    NSInteger selectDay;
    NSMutableArray *viewArray;
    NSMutableArray *layerArray;
}

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) NSArray *timeProfiles;

@end

@implementation HHTimeProfileOverviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = [HHUserSettings sharedSettings].currentDeviceOwner[@"title"];
    cellHeight = 0;
    localizeDayArray = @[LS(@"MONDAY"), LS(@"TUESDAY"), LS(@"WEDNESDAY"), LS(@"THURSDAY"), LS(@"FRIDAY"), LS(@"SATURDAY"), LS(@"SUNDAY")];
    dayArray = @[@"MONDAY", @"TUESDAY", @"WEDNESDAY", @"THURSDAY", @"FRIDAY", @"SATURDAY", @"SUNDAY"];
    
    [self update];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update) name:@"updateTimeProfile" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self update];
}

- (void)update
{
    for (UIView *view in viewArray)
    {
        [view removeFromSuperview];
    }
    for (UIView *layer in layerArray)
    {
        [layer removeFromSuperview];
    }
    viewArray = [NSMutableArray new];
    layerArray = [NSMutableArray new];
    self.timeProfiles = [HHUserSettings sharedSettings].currentDeviceOwner[@"timeProfiles"];
    
    timeArray = [NSMutableArray new];
    for (NSInteger i = 1; i <= 96; i++) {
        [timeArray addObject:@(i * 15)];
    }
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cellHeight = self.tableView.frame.size.height/7;
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTimeProfileDaysCell];
    cell.backgroundColor = indexPath.row%2 ? [UIColor whiteColor] : RGB(231, 231, 231);
    NSMutableArray *profilesArray = [NSMutableArray new];
    for (NSDictionary *timeProfile in self.timeProfiles)
    {
        if ([[timeProfile[@"dayOfWeek"] lowercaseString] isEqualToString:[dayArray[indexPath.row] lowercaseString]])
            [profilesArray addObject:timeProfile];
    }
    [self drawDayTimeLine:cell withColorArray:[self timeLineColor:profilesArray]];
    
    UILabel *title = (UILabel *)[cell.contentView viewWithTag:1];
    [title setTextColor:greenColor];
    title.text = localizeDayArray[indexPath.row];
    UIView *separator = (UIView *)[cell.contentView viewWithTag:2];
    [cell bringSubviewToFront:title];
    [cell bringSubviewToFront:separator];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *profilesArray = [NSMutableArray new];
    for (NSDictionary *timeProfile in self.timeProfiles)
    {
        if ([[timeProfile[@"dayOfWeek"] lowercaseString] isEqualToString:[dayArray[indexPath.row] lowercaseString]])
            [profilesArray addObject:timeProfile];
    }
    selectDay = indexPath.row;
    [self performSegueWithIdentifier:kGoToTimeProfileSegue sender:[self timeLineColor:profilesArray]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark - Navigations

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kGoToTimeProfileSegue])
    {
        NSArray *array = sender;
        HHTimeProfileViewController* vc = segue.destinationViewController;
        vc.timeColorArray = array;
        vc.day = selectDay;
    }
}

#pragma mark -
#pragma mark - Draw methods

- (void)drawDayTimeLine:(UITableViewCell*)cell withColorArray:(NSArray*)colorArray
{
    NSInteger index = 0;
    for (int i = 1; i < 13; i++)
    {
        CGFloat offset = self.view.frame.size.width/12.f;
        CGFloat xValue = offset*i;
        
        UIView *shapeLayer = [[UIView alloc] initWithFrame:CGRectMake(xValue, cellHeight - cellHeight/8, 1, cellHeight/8)];
        shapeLayer.backgroundColor = cell.backgroundColor;
        
        for (int j = 0; j < 8; j++)
        {   
            CGFloat littleOffset = (offset/8)*j;
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xValue-offset+littleOffset, 0, offset/8, cellHeight)];
            view.backgroundColor = [UIColor clearColor];
            UIView *littleView = [[UIView alloc] initWithFrame:CGRectMake(0, view.frame.size.height - view.frame.size.height/8, offset/8, view.frame.size.height/8)];
            littleView.backgroundColor = colorArray[(j+index*8)];
            [viewArray addObject:littleView];
            [view addSubview:littleView];
            [cell addSubview:view];
            [cell sendSubviewToBack:view];
        }
        index++;
        
        if (i == 12)
            return;
        [layerArray addObject:shapeLayer];
        [cell addSubview:shapeLayer];
    }
}

- (NSArray*)timeLineColor:(NSArray*)profileArray
{
    NSMutableArray *colorsArray = [NSMutableArray new];
    for (int i = 0; i < 96; i++)
    {
        [colorsArray addObject:redColor];
    }
    
    for (NSDictionary *profile in profileArray)
    {
        NSString *startTime = profile[@"startTime"][@"time"];
        NSString *startHour= [startTime substringToIndex:2];
        NSInteger hour = startHour.integerValue;
        
        NSString *startMinutes= [[startTime substringFromIndex:startTime.length - 5] substringToIndex:2];
        NSInteger minutes = startMinutes.integerValue;
        
        NSString *endTime = profile[@"endTime"][@"time"];
        NSString *endHour= [endTime substringToIndex:2];
        NSInteger hourEnd = endHour.integerValue;
        
        NSString *endMinutes= [[endTime substringFromIndex:endTime.length - 5] substringToIndex:2];
        NSInteger minutesEnd = endMinutes.integerValue;
        
        
        NSInteger start = hour*60 + minutes;
        NSInteger end = hourEnd*60 + minutesEnd;
    
        for (NSInteger i = 0; i < timeArray.count; i++)
        {
            NSNumber* time = timeArray[i];
            if (time.integerValue > start && time.integerValue <= end)
                colorsArray[i] = greenColor;
            
            if (i == timeArray.count-1)
                if (time.integerValue > start && time.integerValue <= end+1)
                    colorsArray[i] = greenColor;
        }
    }
    return colorsArray;
}

#pragma mark - Actions

- (IBAction)backTimeProfileButtonTouch:(id)sender {
    if (self.isReturnToUsers) {
        HHUsersViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:kUsersPageStoryboardID];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
