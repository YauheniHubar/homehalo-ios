//
//  HHTimeProfileViewController.h
//  HomeHalo
//
//  Created by naumov.kirill on 5/20/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"

@interface HHTimeProfileViewController : HHBaseViewController

@property (nonatomic) NSInteger day;
@property (nonatomic) NSArray *timeColorArray;

@end
