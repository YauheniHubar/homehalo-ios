//
//  HHRegisterViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/6/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHRegisterViewController.h"
#import "UITextField+Offset.h"

@interface HHRegisterViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;
@property (nonatomic, weak) IBOutlet UITextField *verificationPasswordTextField;

@property (nonatomic, weak) IBOutlet UIButton *createMyAccountBtn;

@property (nonatomic, weak) IBOutlet UIView *topView;
@property (nonatomic, weak) IBOutlet UIView *secondTopView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *yConstraintLogo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomSpaceConstraint;

@property (nonatomic) CGRect startFrame;

@end

@implementation HHRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoRegisterScreen");
    }
    else {
        self.screenName = LS(@"RegisterScreen");
    }
    
    [self.emailTextField addOffset];
    [self.passwordTextField addOffset];
    [self.verificationPasswordTextField addOffset];
    
    self.startFrame = self.view.frame;
    [self.view bringSubviewToFront:self.topView];
    [self.view bringSubviewToFront:self.secondTopView];
}

#pragma mark -
#pragma mark - Private

- (void)keyboardWillChange:(NSNotification *)notification {
    self.view.frame = self.startFrame;
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat delta = self.view.superview.bounds.size.height - keyboardRect.origin.y - self.bottomSpaceConstraint.constant;
    
    self.yConstraintLogo.constant = -delta;
    
    CGRect frame = self.view.frame;
    frame.size.height -= delta;
    self.view.frame = frame;
    
    self.yConstraintLogo.constant = -delta;
}

- (void)noticeWillHideKeyboard:(NSNotification *)inNotification
{
    CGRect frame = self.view.frame;
    frame.size.height = self.view.superview.bounds.size.height - frame.origin.y;
    self.view.frame = frame;
    
    self.yConstraintLogo.constant = 0;
}

#pragma mark -
#pragma mark Actions

- (IBAction)createBtnTouch:(id)sender
{
    if (![self.passwordTextField.text isEqualToString:self.verificationPasswordTextField.text])
    {
        [HHUtils alertWithTitle:LS(@"VerifingPasswordError") message:LS(@"PasswordAren'tSame")];
    }
    else if (self.passwordTextField.text.length < 1)
    {
        [HHUtils alertErrorMessage:LS(@"PleaseFillAllOfTheRequiredFields")];
    }
    else if (![HHUtils validateEmail:self.emailTextField.text])
    {
        [HHUtils alertErrorMessage:LS(@"EmailIsn'tValid")];
    }
    else
    {
        if (![HHUtils checkInternetConnection])
        {
            [HHUtils alertErrorMessage:LS(@"NetworkErrorMessageNoInternetConnection")];
            return;
        }
//        __weak __typeof(self) weakSelf = self;
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        [[HHBaseWebServiceClient instance] requestAccountRegisterWithEmail:self.emailTextField.text password:self.passwordTextField.text completionBlock:^(id responseObject, NSError *error) {
//            if ([responseObject[@"success"] boolValue] == YES) {
//                [HHUserSettings sharedSettings].demoMode = NO;
//                [weakSelf goToPinScreen];
//            }
//            else
//            {
//                if (responseObject[@"error"][@"messages"] != nil)
//                    [HHUtils alertErrorMessage:[responseObject[@"error"][@"messages"] firstObject]];
//                else
//                    [HHUtils alertErrorMessage:error.localizedDescription];
//            }
//            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
//        }];
    }
}

#pragma mark -
#pragma mark - UITextFieldDelegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
