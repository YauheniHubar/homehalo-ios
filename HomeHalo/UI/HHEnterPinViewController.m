//
//  HHEnterPinViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/8/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHEnterPinViewController.h"
#import "HHAlertsViewController.h"

static NSString *const kSegueIdToSignUpScreen = @"goToSignSegue";
static NSString *const kAlertsSegue = @"kAlertsSegue";

@interface HHEnterPinViewController () <UIGestureRecognizerDelegate>

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic) IBOutletCollection(UIButton) NSArray *pinIndicators;

@property (nonatomic, weak) IBOutlet UILabel *setPinLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *logo;

@property (nonatomic) NSInteger pinCount;
@property (nonatomic) NSString *pinString;

@end

@implementation HHEnterPinViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoEnterPinScreen");
    } else {
        self.screenName = LS(@"EnterPinScreen");
    }

    self.pinCount = 0;

    if ([HHUserSettings sharedSettings].correctPIN) {
        self.titleLabel.text = LS(@"EnterHHPin");
        [self.setPinLabel setHidden:YES];
        [self.descriptionLabel setHidden:YES];
        [self.logo setHidden:NO];
    } else {
        self.titleLabel.text = LS(@"SetYourPIN");
        [self.setPinLabel setHidden:NO];
        [self.descriptionLabel setHidden:NO];
        [self.logo setHidden:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self deleteBtnTouch:nil];
    [self.navigationController setNavigationBarHidden:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return NO;
}


#pragma mark - Private

- (void)updatePinIndicators {
    for (NSUInteger i = 0; i < self.pinCount; i++) {
        [self.pinIndicators[i] setSelected:YES];
    }

    if (self.pinCount == 4) {
        NSString *correctPin = [HHUserSettings sharedSettings].correctPIN;

        if ([self.pinString isEqualToString:correctPin] || !correctPin) {
            [HHUserSettings sharedSettings].correctPIN = self.pinString;

            if (self.isReturnToPrevVC && ![HHUserSettings sharedSettings].openAlerts) {
                [self homeButtonTouch:nil];

                return;
            }

            if ([HHUserSettings sharedSettings].openAlerts) {
                [HHUserSettings sharedSettings].openAlerts = NO;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.5f * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                    HHAlertsViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:kAlertsSegue];
                    vc.returnToHome = YES;
                    [self.navigationController pushViewController:vc animated:YES];

                });
            } else if ([HHUserSettings sharedSettings].currentUser) {
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.5f * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                    [self homeButtonTouch:nil];

                });
            } else {
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.5f * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                    [self performSegueWithIdentifier:kSegueIdToSignUpScreen sender:nil];

                });
            }
        } else {
            [HHUtils alertErrorMessage:LS(@"PINIncorrect")];
            [self deleteBtnTouch:nil];
        }
    }
}


#pragma mark - Actions

- (IBAction)logoutBtnTouch:(id)sender {
    [self logout];
}

- (IBAction)deleteBtnTouch:(id)sender {
    self.pinString = nil;
    self.pinCount = 0;

    for (UIButton *btn in self.pinIndicators) {
        [btn setSelected:NO];
    }
}

- (IBAction)numberBtnTouch:(UIButton *)sender {
    if (self.pinCount == 4) {
        return;
    }

    self.pinString = [NSString stringWithFormat:@"%@%ld", self.pinString ? self.pinString : @"", (long) sender.tag];
    self.pinCount++;
    [self updatePinIndicators];
}

@end
