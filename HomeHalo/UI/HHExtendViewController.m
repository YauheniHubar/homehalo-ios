//
//  HHExtendViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/27/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHExtendViewController.h"
#import "EFCircularSlider.h"
#import "HHUsersViewController.h"
#import "HHAPI+Alerts.h"

@interface HHExtendViewController () {
    EFCircularSlider* circularSlider;
}

@property (nonatomic, weak) IBOutlet UILabel *navigationLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *blockButton;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UILabel *minutesLabel;

@property (nonatomic, weak) IBOutlet UILabel *hasAccessUntilLabel;
@property (nonatomic, weak) IBOutlet UILabel *hasAccessUntilLabelSecond;

@property (nonatomic, weak) IBOutlet UIButton *applyButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@property (nonatomic) NSInteger currentTime;
@property (nonatomic) NSInteger prevTime;

@end

@implementation HHExtendViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoExtendScreen") : LS(@"ExtendScreen");

    self.navigationLabel.text = [HHUserSettings sharedSettings].currentDeviceOwner[@"title"];
    [self.blockButton setTitle:LS(@"ExtendTimeFor0m") forState:UIControlStateNormal];
    self.currentTime = 1;
    self.prevTime = 0;
    
    [self checkExtendState];
    
    CGRect sliderFrame = CGRectMake(120, 150, 280, 280);
    circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    circularSlider.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    
    circularSlider.unfilledColor = [UIColor lightGrayColor];
    circularSlider.filledColor = [UIColor colorWithRed:115/255.0f green:180/255.0f blue:6/255.0f alpha:1.0f];
    circularSlider.handleType = CircularSliderHandleTypeBigCircle;
    circularSlider.handleColor = [UIColor blueColor];
    circularSlider.lineWidth = 40;
    
    [circularSlider addTarget:self action:@selector(valueDidChange:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:circularSlider];
    [self.view sendSubviewToBack:circularSlider];
    
    [self.view bringSubviewToFront:self.timeLabel];
    [self.view bringSubviewToFront:self.minutesLabel];
    [self calculateTime];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(calculateTime) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
    [self setStartValue];
    
    [self.cancelButton addTarget:self action:@selector(deleteBtnTouch:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.alertData != nil && self.alertID != nil) {
        [self.blockButton setHidden:YES];
        [self.applyButton setHidden:NO];
        [self.cancelButton setHidden:NO];
        [self.hasAccessUntilLabelSecond setHidden:YES];
        [self.hasAccessUntilLabel setHidden:NO];
        [self.cancelButton removeTarget:self action:@selector(deleteBtnTouch:) forControlEvents:UIControlEventTouchUpInside];
        [self.cancelButton addTarget:self action:@selector(cancelAlert) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)setStartValue
{
    NSDate* dateFromString = [[HHUtils dateFormatterUTC] dateFromString:[HHUserSettings sharedSettings].currentDeviceOwner[@"timeExtension"][@"startedAt"][@"dateTime"]];
    NSNumber* durationTime = @([[HHUserSettings sharedSettings].currentDeviceOwner[@"timeExtension"][@"duration"] doubleValue]);
    NSDate* endDate = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];
    
    NSTimeInterval distanceBetweenDates;
    
    if ([dateFromString compare:[[HHUtils dateFormatterUTC] dateFromString:[HHUtils currentDateUTC]]] == NSOrderedDescending)
        distanceBetweenDates = [dateFromString timeIntervalSinceDate:endDate];
    else
        distanceBetweenDates = [[[HHUtils dateFormatterUTC] dateFromString:[HHUtils currentDateUTC]] timeIntervalSinceDate:endDate];
    
    NSInteger distanceBetweenDatesInt = distanceBetweenDates;
    
    if ([endDate compare:[NSDate date]] == NSOrderedAscending)
        distanceBetweenDatesInt = 0;
    if (distanceBetweenDatesInt < 0)
        distanceBetweenDatesInt *= -1;
    NSInteger minutes = distanceBetweenDatesInt / 60;
    if (distanceBetweenDatesInt%60 < 30)
        minutes++;
    
    float value = 0;
    if (minutes <= 30)
    {
        value = minutes*1.1f;
    }
    else if (minutes > 30 && minutes < 120)
    {
        minutes -= 30;
        value = 33.3f*(minutes/90.f) + 33.3f;
        
    }
    else
    {
        minutes -= 120;
        value = 33.3f*(minutes/600.f) + 66.6f;
    }
    [circularSlider setStartValue:value];
}

- (void)checkExtendState
{
    NSInteger value = 0;
    if ([HHUserSettings sharedSettings].currentTimeExtensionID != 0)
        value = [HHUserSettings sharedSettings].currentTimeExtensionID;
    else
        value = [[HHUserSettings sharedSettings].currentDeviceOwner[@"timeExtension"][@"id"] integerValue];

    if (value == 0)
    {
        [self.blockButton setHidden:NO];
        [self.applyButton setHidden:YES];
        [self.cancelButton setHidden:YES];
        [self.hasAccessUntilLabelSecond setHidden:NO];
        [self.hasAccessUntilLabel setHidden:YES];
    }
    else
    {
        [self.blockButton setHidden:YES];
        [self.applyButton setHidden:NO];
        [self.cancelButton setHidden:NO];
        [self.hasAccessUntilLabelSecond setHidden:YES];
        [self.hasAccessUntilLabel setHidden:NO];
    }
}

#pragma mark -
#pragma mark Request

- (void)cancelAlert {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:self.alertData[@"data"]];
    data[@"status"] = @"denied";
    NSMutableDictionary *newData = [NSMutableDictionary new];
    newData[@"data"] = data;
    newData[@"id"] = @"id";
    newData[@"type"] = @"time_extension_request";
    newData[@"title"] = @"title";
    newData[@"isViewed"] = @"1";
    if (self.alertData[@"data"][@"createdAt"])
        newData[@"createdAt"] = self.alertData[@"data"][@"createdAt"];
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestUpdateAlerts:newData notificationID:(self.alertID).integerValue success:^(id response) {

        [weakSelf.navigationController popViewControllerAnimated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (IBAction)extendBtnTouch:(id)sender {
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestExtendTime:[NSString stringWithFormat:@"%ld", self.currentTime*60] success:^(NSDictionary *responseObject) {

        weakSelf.userSettings.currentTimeExtensionID = [responseObject[@"timeExtension"][@"id"] integerValue];

        [weakSelf checkExtendState];
        [weakSelf updateAlerts];

        if (weakSelf.alertData != nil && weakSelf.alertID != nil) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        } else {
            [weakSelf unwindToViewControllerOfClass:HHUsersViewController.class animated:YES];
        }

        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (IBAction)deleteBtnTouch:(id)sender {
    __weak typeof(self) weakSelf = self;
    NSInteger value = 0;
    if ([HHUserSettings sharedSettings].currentTimeExtensionID != 0)
        value = [HHUserSettings sharedSettings].currentTimeExtensionID;
    else
        value = [[HHUserSettings sharedSettings].currentDeviceOwner[@"timeExtension"][@"id"] integerValue];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestDeleteExtendTime:value success:^(NSDictionary *responseObject) {

        weakSelf.userSettings.currentTimeExtensionID = 0;
        [weakSelf checkExtendState];

        [weakSelf updateAlerts];
        [weakSelf unwindToViewControllerOfClass:HHUsersViewController.class animated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark -
#pragma mark Actions

- (void)valueDidChange:(EFCircularSlider*)slider
{
    int time = 0;
    if (slider.currentValue < 33.3f)
    {
        time = (int)(slider.currentValue/1.1f);
        self.timeLabel.text = [NSString stringWithFormat:@"%d", time];
        self.minutesLabel.text = LS(@"minutes");
    }
    else if (slider.currentValue > 33.3f && slider.currentValue < 66.6f)
    {
        int division = (slider.currentValue - 33.3f)/3.6f;
        time = 30 + 10*division;
        if ((time % 60) == 0)
        {
            self.timeLabel.text = [NSString stringWithFormat:@"%d", time/60];
            self.minutesLabel.text = LS(@"hours");
        }
        else if ((time / 60) >= 1)
        {
            self.timeLabel.text = [NSString stringWithFormat:@"%d:%d", time/60, time%60];
            self.minutesLabel.text = LS(@"hours");
        }
        else
        {
            self.timeLabel.text = [NSString stringWithFormat:@"%d", time];
            self.minutesLabel.text = LS(@"minutes");
        }
    }
    else
    {
        time = ((int)((slider.currentValue-66.6f)/3.1f) + 2) * 60;
        self.timeLabel.text = [NSString stringWithFormat:@"%d", time/60];
        self.minutesLabel.text = LS(@"hours");
    }
    [self.blockButton setTitle:[NSString stringWithFormat:@"%@ %@ %@", LS(@"ExtendTime"),self.timeLabel.text, self.minutesLabel.text] forState:UIControlStateNormal];
    self.currentTime = time;
}

- (void)calculateTime
{
    if (self.currentTime == self.prevTime)
        return;
    self.prevTime = self.currentTime;
    
    NSString * newString = [HHUserSettings sharedSettings].currentDeviceOwner[@"timeExtension"][@"startedAt"][@"dateTime"];
    newString = [newString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    __weak typeof(self) weakSelf = self;
    
    self.hasAccessUntilLabel.text = LS(@"HasAccessUntil");
    self.hasAccessUntilLabelSecond.text = LS(@"HasAccessUntil");
    [[HHAPI sharedAPI] requestCalculateExtendTime:newString duration:[NSString stringWithFormat:@"%ld", self.currentTime*60] success:^(NSDictionary *responseObject) {

        if ([responseObject[@"access"] isKindOfClass:NSNull.class]) {
            weakSelf.hasAccessUntilLabel.text = LS(@"UnlimitedAccess");
            weakSelf.hasAccessUntilLabelSecond.text = LS(@"UnlimitedAccess");
            weakSelf.blockButton.enabled = NO;
            weakSelf.blockButton.backgroundColor = [UIColor grayColor];
        } else {
            weakSelf.hasAccessUntilLabel.text = responseObject[@"access"][@"hasAccessUntilLabel"];
            weakSelf.hasAccessUntilLabelSecond.text = responseObject[@"access"][@"hasAccessUntilLabel"];
            weakSelf.blockButton.enabled = YES;
            weakSelf.blockButton.backgroundColor = RGB(115, 180, 6);

            if ([weakSelf.hasAccessUntilLabel.text isEqualToString:@"unlimited access"]) {
                weakSelf.hasAccessUntilLabel.text = LS(@"UnlimitedAccess");
                weakSelf.hasAccessUntilLabelSecond.text = LS(@"UnlimitedAccess");
                weakSelf.blockButton.enabled = NO;
                weakSelf.blockButton.backgroundColor = [UIColor grayColor];
            }
        }

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];

    }];
}

- (void)updateAlerts {
    if (self.alertData != nil && self.alertID != nil) {
        __weak __typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestUpdateAlerts:self.alertData notificationID:(self.alertID).integerValue success:^(id response) {

            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }
}

@end
