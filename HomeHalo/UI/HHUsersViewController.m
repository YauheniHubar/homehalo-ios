//
//  HHUsersViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/23/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHUsersViewController.h"
#import "HHAPI.h"
#import "HHAPI+Devices.h"

static NSString *const kUsersCellIdentifier = @"usersCellIdentifier";
static NSString *const kSegueIdToUserPageScreen = @"goToUserPageSegue";
static NSString *const kSegueIdToAddUserScreen = @"kSegueIdToAddUserScreen";

@interface HHUsersViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *addNewUserButton;
@property (nonatomic) NSArray *dataArray;

@end

@implementation HHUsersViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoUsersScreen") : LS(@"UsersScreen");
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    NSTimer *timerStartValue = [NSTimer scheduledTimerWithTimeInterval:20.0f target:self selector:@selector(reloadTimes) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timerStartValue forMode:NSRunLoopCommonModes];
}

- (void)reloadTimes
{
    [self.tableView reloadData];
}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    [refreshControl endRefreshing];
    [self updateUser];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUser];
}

#pragma mark -
#pragma mark - Private

- (void)updateUser
{
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestDeviceOwnersWithSuccess:^(NSDictionary *responseObject) {

        weakSelf.dataArray = responseObject[@"deviceOwners"];
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = self.tableView.frame.size.height/self.dataArray.count;
    if (cellHeight > 96.f)
        return cellHeight;
    return 96.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kUsersCellIdentifier];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    titleLabel.text = self.dataArray[indexPath.row][@"title"];
    
    UILabel *timeLabel = (UILabel *)[cell.contentView viewWithTag:2];
    [timeLabel setHidden:YES];
    UILabel *accessLabel = (UILabel *)[cell.contentView viewWithTag:3];
    accessLabel.text = [self configureAccessLabel:indexPath];
    accessLabel.hidden = accessLabel.text.length > 0 ? NO : YES;
    
    UIImageView *checkMark = (UIImageView *)[cell.contentView viewWithTag:4];
    checkMark.image = [UIImage imageNamed:@"ok_icon"];
    
    UIImageView *timeMark = (UIImageView *)[cell.contentView viewWithTag:5];
    [timeMark setHidden:YES];
    
    cell.backgroundColor = [UIColor colorWithRed:115/255.0f green:180/255.0f blue:6/255.0f alpha:1.0f];
    
    if ([self.dataArray[indexPath.row][@"access"][@"isNoAccessUnlimited"] integerValue] == 1)
    {
        checkMark.image = [UIImage imageNamed:@"close_red_icon"];
        cell.backgroundColor = [UIColor grayColor];
        accessLabel.text = @"";
    }
    else if ([self.dataArray[indexPath.row][@"access"][@"isAccessUnlimited"] integerValue] == 1)
    {
        checkMark.image = [UIImage imageNamed:@"ok_icon"];
        cell.backgroundColor = [UIColor colorWithRed:115/255.0f green:180/255.0f blue:6/255.0f alpha:1.0f];
        accessLabel.text = @"";
    }
    
    if (![self.dataArray[indexPath.row][@"access"][@"hasNoAccessUntil"] isKindOfClass:NSNull.class])
    {
        if ((self.dataArray[indexPath.row][@"access"])[@"hasNoAccessUntil"])
        {
            checkMark.image = [UIImage imageNamed:@"close_red_icon"];
            cell.backgroundColor = [UIColor grayColor];
        }
    }
    
    //block
    NSDate *dateFromString = [[HHUtils dateFormatterUTC] dateFromString:self.dataArray[indexPath.row][@"blockStartedAt"][@"dateTime"]];
    NSNumber* durationTime = @([self.dataArray[indexPath.row][@"time"] doubleValue]);
    NSDate* date = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];
    
    if ([date compare:[NSDate date]] == NSOrderedDescending) {
        checkMark.image = [UIImage imageNamed:@"close_orange_icon"];
        
        timeMark.image = [UIImage imageNamed:@"block_little"];
        [timeMark setHidden:NO];
        [timeLabel setHidden:NO];
        timeLabel.text = [self getTimeStringWithStartDate:date];
        cell.backgroundColor = [UIColor grayColor];
        return cell;
    }
    
    //homework
    dateFromString = [[HHUtils dateFormatterUTC] dateFromString:self.dataArray[indexPath.row][@"homeworkModeStartedAt"][@"dateTime"]];
    durationTime = @([self.dataArray[indexPath.row][@"homeworkModeDuration"] doubleValue]);
    date = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];
    
    if ([date compare:[NSDate date]] == NSOrderedDescending)
    {
        cell.backgroundColor = [UIColor colorWithRed:115/255.0f green:180/255.0f blue:6/255.0f alpha:1.0f];
        checkMark.image = [UIImage imageNamed:@"ok_orange_icon"];
        timeMark.image = [UIImage imageNamed:@"homework_little"];
        [timeMark setHidden:NO];
        [timeLabel setHidden:NO];
        timeLabel.text = [self getTimeStringWithStartDate:date];
        return cell;
    }
    
    //time extension
    dateFromString = [[HHUtils dateFormatterUTC] dateFromString:self.dataArray[indexPath.row][@"timeExtension"][@"startedAt"][@"dateTime"]];
    durationTime = @([self.dataArray[indexPath.row][@"timeExtension"][@"duration"] doubleValue]);
    date = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];
    if ([date compare:[NSDate date]] == NSOrderedDescending)
    {
        
        cell.backgroundColor = [UIColor colorWithRed:115/255.0f green:180/255.0f blue:6/255.0f alpha:1.0f];
        checkMark.image = [UIImage imageNamed:@"ok_orange_icon"];
        timeMark.image = [UIImage imageNamed:@"extend_little"];
        [timeMark setHidden:NO];
        [timeLabel setHidden:NO];
        
        if ([dateFromString compare:[[HHUtils dateFormatterUTC] dateFromString:[HHUtils currentDateUTC]]] == NSOrderedDescending)
            timeLabel.text = [self getTimeStringWithStartDate:date fromDate:dateFromString];
        else
            timeLabel.text = [self getTimeStringWithStartDate:date fromDate:[[HHUtils dateFormatterUTC] dateFromString:[HHUtils currentDateUTC]]];
        return cell;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:kSegueIdToUserPageScreen sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [HHUserSettings sharedSettings].currentDeviceOwner = (self.dataArray)[indexPath.row];
    [HHUserSettings sharedSettings].currentTimeExtensionID = 0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= self.dataArray.count)
        return NO;
    if ([self.dataArray[indexPath.row][@"isDeletable"] boolValue] == YES)
        return YES;
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        __weak typeof(self) weakSelf = self;
        __weak typeof(NSIndexPath*) weakIndex = indexPath;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestDeleteUser:[self.dataArray[indexPath.row][@"id"] integerValue] success:^(NSDictionary *responseObject) {

            NSMutableArray *newData = [NSMutableArray arrayWithArray:weakSelf.dataArray];
            [newData removeObjectAtIndex:weakIndex.row];
            weakSelf.dataArray = newData;
            [weakSelf.tableView reloadData];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {
            
            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }
}

#pragma mark -
#pragma mark - Private

- (NSString*)getTimeStringWithText:(NSString*)text startDate:(NSDate*)date
{
    unsigned int unitFlags = NSCalendarUnitHour | NSCalendarUnitMinute;
    
    NSCalendar* currCalendar = [NSCalendar currentCalendar];
    NSDateComponents *conversionInfo = [currCalendar components:unitFlags fromDate:[NSDate date] toDate:date  options:0];
    
    NSInteger hours = conversionInfo.hour;
    NSInteger minutes = conversionInfo.minute;
    NSString *descriptionStr;
    if (hours > 2) {
        if (minutes > 30)
            hours++;
        descriptionStr = [NSString stringWithFormat:@"%@: %ld %@\n", text, (long)hours, LS(@"hours")];
    }
    else
    {
        descriptionStr = [NSString stringWithFormat:@"%@: %ld %@\n", text, (long)(hours>0?minutes+60:minutes), LS(@"mins")];
    }
    return descriptionStr;
}

- (NSString*)getTimeStringWithStartDate:(NSDate*)date
{
    NSString *descriptionStr;
    
    NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:date];
    NSInteger distanceBetweenDatesInt = distanceBetweenDates;
    if (distanceBetweenDatesInt < 0)
        distanceBetweenDatesInt *= -1;
    NSInteger minutes = distanceBetweenDatesInt / 60;
    if (distanceBetweenDatesInt%60 > 30)
        minutes++;
    
    if (minutes > 120)
    {
        int hours = (int)minutes/60;
        if (minutes % 60 > 30)
            hours++;
        descriptionStr = [NSString stringWithFormat:@"%d %@", hours, LS(@"hours")];
    }
    else if (minutes <= 120 && minutes > 60)
    {
        int hours = (int)minutes/60;
        descriptionStr = [NSString stringWithFormat:@"%d %@ %d %@", hours, LS(@"hours"), (int)minutes%60, LS(@"mins")];
    }
    else if (minutes == 60)
    {
        descriptionStr = [NSString stringWithFormat:@"1 %@", LS(@"hours")];
    }
    else
    {
        descriptionStr = [NSString stringWithFormat:@"%ld %@", (long)minutes, LS(@"minutes")];
    }
    return descriptionStr;
}

- (NSString*)getTimeStringWithStartDate:(NSDate*)date fromDate:(NSDate*)fromDate
{
    date = [HHUtils toLocalTime:date];
    fromDate = [HHUtils toLocalTime:fromDate];
    
    NSString *descriptionStr;
    
    NSTimeInterval distanceBetweenDates = [fromDate timeIntervalSinceDate:date];
    NSInteger distanceBetweenDatesInt = distanceBetweenDates;
    if (distanceBetweenDatesInt < 0)
        distanceBetweenDatesInt *= -1;
    NSInteger minutes = distanceBetweenDatesInt / 60;
    if (distanceBetweenDatesInt%60 > 30)
        minutes++;
    
    if (minutes > 120)
    {
        int hours = (int)minutes/60;
        if (minutes % 60 > 30)
            hours++;
        descriptionStr = [NSString stringWithFormat:@"%d %@", hours, LS(@"hours")];
    }
    else if (minutes <= 120 && minutes > 60)
    {
        int hours = (int)minutes/60;
        descriptionStr = [NSString stringWithFormat:@"%d %@ %d %@", hours, LS(@"hours"), (int)minutes%60, LS(@"mins")];
    }
    else if (minutes == 60)
    {
        descriptionStr = [NSString stringWithFormat:@"1 %@", LS(@"hours")];
    }
    else
    {
        descriptionStr = [NSString stringWithFormat:@"%ld %@", (long)minutes, LS(@"minutes")];
    }
    return descriptionStr;
}

- (NSString*)configureAccessLabel:(NSIndexPath*)indexPath
{
    NSString *dateStr = nil;
    
    if (![self.dataArray[indexPath.row][@"access"][@"hasAccessUntil"] isKindOfClass:NSNull.class])
    {
        if ((self.dataArray[indexPath.row][@"access"])[@"hasAccessUntil"])
        {
            NSDate *date = [[HHUtils dateFormatter] dateFromString:self.dataArray[indexPath.row][@"access"][@"hasAccessUntil"][@"dateTime"]];
            date = [HHUtils toLocalTime:date];
            if ([date compare:[NSDate date]] != NSOrderedDescending)
                return @"";
            
            if ([[[HHUtils getDay] stringFromDate:date] isEqualToString:[[HHUtils getDay] stringFromDate:[NSDate date]]])
                dateStr = [[HHUtils dayFormatterGreatWithDay:NO] stringFromDate:date];
            else
                dateStr = [[HHUtils dayFormatterGreatWithDay:YES] stringFromDate:date];
            dateStr = dateStr.uppercaseString;
            return dateStr;
        }
    }
    if (![self.dataArray[indexPath.row][@"access"][@"hasNoAccessUntil"] isKindOfClass:NSNull.class])
    {
        if ((self.dataArray[indexPath.row][@"access"])[@"hasNoAccessUntil"])
        {
            NSString *dateStr = self.dataArray[indexPath.row][@"access"][@"hasNoAccessUntil"][@"dateTime"];
            NSDate *date = [[HHUtils dateFormatter] dateFromString:dateStr];
            date = [HHUtils toLocalTime:date];
            
            if ([date compare:[NSDate date]] != NSOrderedDescending)
                return @"";
            
            if ([[[HHUtils getDay] stringFromDate:date] isEqualToString:[[HHUtils getDay] stringFromDate:[NSDate date]]])
                dateStr = [[HHUtils dayFormatterLessWithDay:NO] stringFromDate:date];
            else
                dateStr = [[HHUtils dayFormatterLessWithDay:YES] stringFromDate:date];
            dateStr = dateStr.uppercaseString;
            return dateStr;
        }
    }
    return dateStr;
}


#pragma mark - Actions

- (IBAction)addUserBtnTouch:(id)sender {
    [self performSegueWithIdentifier:kSegueIdToAddUserScreen sender:nil];
}

@end
