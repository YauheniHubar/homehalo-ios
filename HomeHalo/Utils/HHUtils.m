//
//  HHUtils.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHUtils.h"
#import "HHUserSettings.h"
#import "HHUser.h"
#import "HHWedge.h"
#include <netdb.h>

@import MobileCoreServices.UTType;

@implementation UILabel (Localization)

- (void)setLocalizedStringKey:(NSString *)localizedStringKey {
    self.text = LS(localizedStringKey);
}

@end

@implementation UIButton (Localization)

- (void)setLocalizedStringKey:(NSString *)localizedStringKey {
    NSArray *controlStates = @[
            @(UIControlStateNormal),
            @(UIControlStateHighlighted),
            @(UIControlStateSelected)
    ];
    
    for (NSNumber *state in controlStates) {
        [self setTitle:LS(localizedStringKey) forState:(UIControlState)state.integerValue];
    }
}

@end

@implementation UIBarButtonItem (Localization)

- (void)setLocalizedStringKey:(NSString *)localizedStringKey {
    self.title = LS(localizedStringKey);
}

@end

@implementation UINavigationItem (Localization)

- (void)setLocalizedStringKey:(NSString *)localizedStringKey {
    self.title = LS(localizedStringKey);
}

@end

@implementation UITextField (Localization)

- (void)setLocalizedStringKey:(NSString *)localizedStringKey {
    self.placeholder = LS(localizedStringKey);
}

@end

@implementation HHUtils

+ (NSString *)apiUrl {
    return kBaseApiURL;
}

#pragma mark - Validation

+ (BOOL)validateEmail:(NSString *)email {
    NSString *emailRegex = @"^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}

#pragma mark - Messages

+ (void)alertWithTitle:(NSString*) title message:(NSString *)message {
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:LS(@"OK")
                                              otherButtonTitles:nil];
    runOnMainQueueWithoutDeadlocking(^{
        [alertView show];
    });
}

+ (void)alertErrorMessage:(NSString *)message {
	[self alertWithTitle:LS(@"ErrorAlertTitle") message:message];
}

void runOnMainQueueWithoutDeadlocking(void (^block)(void)) {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

#pragma mark - Storyboard

+ (UIStoryboard *)mainStoryboard {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *storyboardName = [bundle objectForInfoDictionaryKey:@"UIMainStoryboardFile"];
    
    return [UIStoryboard storyboardWithName:storyboardName bundle:bundle];
}

#pragma mark -
#pragma mark - Date formatting

+ (NSDate *)toLocalTime:(NSDate *)date {
    NSTimeZone *tz = [NSTimeZone timeZoneWithName:[HHUserSettings sharedSettings].currentUser.wedge.timezone];
    NSInteger seconds = [tz secondsFromGMTForDate:date];
    
    return [NSDate dateWithTimeInterval: seconds sinceDate: date];
}

+ (NSString *)localizedTimeStringFromDate:(NSDate *)date
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    [dateFormatter setDateFormat:nil];
    
    NSString *newDateString = [dateFormatter stringFromDate:date];
    
    return newDateString;
}

+ (NSString *)localizedDateStringFromDate:(NSDate *)date
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.dateStyle = NSDateFormatterShortStyle;
    [dateFormatter setDateFormat:nil];
    
    NSString *newDateString = [dateFormatter stringFromDate:date];
    
    return newDateString;
}

+ (NSString *)currentDate
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:[HHUserSettings sharedSettings].currentUser.wedge.timezone];
    dateFormatter.timeZone = timeZone;
    
    NSString *stringFromDate = [dateFormatter stringFromDate:[NSDate date]];
    return stringFromDate;
}

+ (NSString *)currentDateUTC
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    NSString *stringFromDate = [dateFormatter stringFromDate:[NSDate date]];
    return stringFromDate;
}

+ (NSDateFormatter *)dateFormatter {
    static NSDateFormatter *dateFormatter = nil;

    if (!dateFormatter) {
        dateFormatter = [NSDateFormatter new];
    }
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    dateFormatter.timeZone = timeZone;
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";

    return dateFormatter;
}

+ (NSDateFormatter *)dateFormatterForDevices
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    dateFormatter.timeZone = timeZone;
    dateFormatter.dateFormat = @"HH:mm dd/MM/yy";
    return dateFormatter;
}

+ (NSDateFormatter *)dateFormatterUTC
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    dateFormatter.timeZone = timeZone;
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return dateFormatter;
}

+ (NSDateFormatter *)dayFormatterLessWithDay:(BOOL)isDay
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    dateFormatter.timeZone = timeZone;
    
    if (isDay)
        dateFormatter.dateFormat = @"< HH:mm\nEEE";
    else
        dateFormatter.dateFormat = @"< HH:mm";
    return dateFormatter;
}

+ (NSDateFormatter *)dayFormatterGreatWithDay:(BOOL)isDay
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    dateFormatter.timeZone = timeZone;
    
    if (isDay)
        dateFormatter.dateFormat = @"> HH:mm\nEEE";
    else
        dateFormatter.dateFormat = @"> HH:mm";
    return dateFormatter;
}

+ (NSDateFormatter *)getDay
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:[HHUserSettings sharedSettings].currentUser.wedge.timezone];
    dateFormatter.timeZone = timeZone;
    dateFormatter.dateFormat = @"dd";
    return dateFormatter;
}

+ (NSDateFormatter *)getDateStringForReports:(NSString*)time
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
    }
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:[HHUserSettings sharedSettings].currentUser.wedge.timezone];
    dateFormatter.timeZone = timeZone;
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSString *kHour = @"3600";
    NSString *kDay = @"86400";
    NSString *k7day = @"604800";
    NSString *k30d = @"2592000";
        
    if (time == kHour)
    {
        dateFormatter.dateFormat = @"HH:mm";
    }
    else if (time == kDay)
    {
        dateFormatter.dateFormat = @"HH:mm\nMM-dd";
    }
    else if (time == k7day)
    {
        dateFormatter.dateFormat = @"MM-dd";
    }
    else if (time == k30d)
    {
        dateFormatter.dateFormat = @"MM-dd";
    }
    return dateFormatter;
}

+ (NSString*)getTimeAgo:(NSDate*)date
{
    unsigned int unitFlags = NSCalendarUnitSecond;
    
    NSCalendar* currCalendar = [NSCalendar currentCalendar];
    NSDateComponents *conversionInfo = [currCalendar components:unitFlags fromDate:[NSDate date] toDate:date  options:0];
    
    NSInteger second1 = conversionInfo.second;
    second1 *= -1;
    double deltaSeconds = second1;
    double deltaMinutes = second1/60;
    
    int minutes = 0;
    
    if(deltaSeconds < 5)
    {
        return LS(@"JustNow");
    }
    else if(deltaSeconds < 60)
    {
        return [NSString stringWithFormat:@"%d %@", (int)deltaSeconds, LS(@"SecondsAgo")];
    }
    else if(deltaSeconds < 120)
    {
        return LS(@"AMinuteAgo");
    }
    else if (deltaMinutes < 60)
    {
        return [NSString stringWithFormat:@"%d %@", (int)deltaMinutes, LS(@"MinutesAgo")];
    }
    else if (deltaMinutes < 120)
    {
        return LS(@"AnHourAgo");
    }
    else if (deltaMinutes < (24 * 60))
    {
        minutes = (int)floor(deltaMinutes/60);
        return [NSString stringWithFormat:@"%d %@", minutes, LS(@"HoursAgo")];
    }
    else if (deltaMinutes < (24 * 60 * 2))
    {
        return LS(@"Yesterday");
    }
    else if (deltaMinutes < (24 * 60 * 7))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24));
        return [NSString stringWithFormat:@"%d %@", minutes, LS(@"DaysAgo")];
    }
    else if (deltaMinutes < (24 * 60 * 14))
    {
        return LS(@"LastWeek");
    }
    else if (deltaMinutes < (24 * 60 * 31))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24 * 7));
        return [NSString stringWithFormat:@"%d %@", minutes, LS(@"WeeksAgo")];
    }
    else if (deltaMinutes < (24 * 60 * 61))
    {
        return LS(@"LastMonth");
    }
    else if (deltaMinutes < (24 * 60 * 365.25))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24 * 30));
        return [NSString stringWithFormat:@"%d %@", minutes, LS(@"MonthsAgo")];
    }
    else if (deltaMinutes < (24 * 60 * 731))
    {
        return LS(@"LastYear");
    }
    
    minutes = (int)floor(deltaMinutes/(60 * 24 * 365));
    return [NSString stringWithFormat:@"%d %@", minutes, LS(@"YearsAgo")];
}

#pragma mark - Other

+ (BOOL)checkInternetConnection
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname(hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!\n");
        return YES;
    }
}

+ (NSMutableAttributedString *)underlineString:(NSString *)str withRange:(NSRange)range
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:range];
    return attributedString;
}

@end
