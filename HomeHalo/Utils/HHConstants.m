//
//  HHConstants.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHConstants.h"

NSString *const kBaseURL = @"https://www.homehalo.net";
NSString *const kBaseApiURL = @"https://www.homehalo.net/api/";//@"https://www.staging.homehalo.net/api/"

NSString *const kBuyHomeHaloRedirectURL = @"http://homehalo.net/homehaloproducts";
NSString *const kBuyHomeHaloURL = @"https://www.homehalo.net/service/login";

NSString *const kAuthenticationApiKey = @"authentication";
NSString *const kResetPasswordApiKey = @"password-reset";
NSString *const kUsersApiKey = @"users";
NSString *const kHelpApiKey = @"help-requests";
NSString *const kContentProfileApiKey = @"content-profiles";
NSString *const kContentCategoriesApiKey = @"content-categories";
NSString *const kDeviceOwnersApiKey = @"device-owners";
NSString *const kTimeExtensionsApiKey = @"time-extensions";
NSString *const kHomeworkCategoriesApiKey = @"homework-categories";
NSString *const kRestartApiKey = @"wedge/restart";
NSString *const kWhitelistApiKey = @"url-whitelists";
NSString *const kBlacklistApiKey = @"url-blacklists";
NSString *const kDevicesWithSortApiKey = @"devices";//?sortByRelevance=true";
NSString *const kGetBrowsingHistoryApiKey = @"device-owners/browsing-traffics";//@"device-owner-browsing-summary";
NSString *const kGetAlertsApiKey = @"notifications?isViewed=0";
NSString *const kNotificationsApiKey = @"notifications";
NSString *const kDevicesApiKey = @"devices";
NSString *const kPasswordApiKey = @"password";
NSString *const kSecretTokenKey = @"X-Secret-Token";
NSString *const kAccessTokenKey = @"X-Access-Token";
NSString *const kDemoModApiKey = @"demo-users";

// Notifications
NSString *const HHLogoutNotification  = @"com.homehalo.app.notification.logout";
