//
//  HHUtils.h
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

// Color
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

// localization
#define LS(key) NSLocalizedString(key, nil)

// idiom
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define IS_IPHONE UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone
#define IS_IPHONE_4 [UIScreen mainScreen].bounds.size.height == 480.0 && IS_IPHONE

@interface HHUtils : NSObject

+ (NSString *)apiUrl;

#pragma mark - Validation

+ (BOOL)validateEmail:(NSString *)email;

#pragma mark - Messages

+ (void)alertWithTitle:(NSString*)title message:(NSString*)message;
+ (void)alertErrorMessage:(NSString*) message;

#pragma mark - Storyboard

+ (UIStoryboard *)mainStoryboard;

#pragma mark - Date formatting

+(NSDate *)toLocalTime:(NSDate*)date;

+ (NSString *)localizedTimeStringFromDate:(NSDate *)date;
+ (NSString *)localizedDateStringFromDate:(NSDate *)date;
+ (NSString *)currentDate;
+ (NSString *)currentDateUTC;

+ (NSDateFormatter *)dateFormatter;
+ (NSDateFormatter *)dateFormatterUTC;
+ (NSDateFormatter *)dateFormatterForDevices;
+ (NSDateFormatter *)dayFormatterLessWithDay:(BOOL)isDay;
+ (NSDateFormatter *)dayFormatterGreatWithDay:(BOOL)isDay;
+ (NSDateFormatter *)getDay;
+ (NSDateFormatter *)getDateStringForReports:(NSString*)time;

+ (NSString*)getTimeAgo:(NSDate*)date;

#pragma mark - Other

+ (BOOL)checkInternetConnection;

+ (NSMutableAttributedString *)underlineString:(NSString *)str withRange:(NSRange)range;

@end
