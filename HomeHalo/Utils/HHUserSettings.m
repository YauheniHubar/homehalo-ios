//
//  HHUserSettings.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHUserSettings.h"
#import "HHUser.h"

static NSString *const kDemoMode = @"isDemoMode";
static NSString *const kCurrentUser = @"currentUser";
static NSString *const kKeyAppAccessToken = @"accessToken";
static NSString *const kUserDefaultPinKey = @"UserDefaultPinKey";
static NSString *const kIsHelpShown = @"isHelpShown";

@interface HHUserSettings ()

@property (nonatomic, readonly) NSUserDefaults *userDefaults;

@end

@implementation HHUserSettings

@synthesize currentUser = _currentUser;

+ (instancetype)sharedSettings {
    static dispatch_once_t once;
    static HHUserSettings *__sharedSettings = nil;

    dispatch_once(&once, ^{
        __sharedSettings = [self new];
    });

    return __sharedSettings;
}

- (instancetype)init {
    self = [super init];

    if (!self) {
        return nil;
    }

    _userDefaults = [NSUserDefaults standardUserDefaults];

    return self;
}

- (void)setDemoMode:(BOOL)demoMode {
    [self.userDefaults setBool:demoMode forKey:kDemoMode];
}

- (BOOL)isDemoMode {
    return [self.userDefaults boolForKey:kDemoMode];
}

- (void)setCurrentUser:(HHUser *)currentUser {
    if (!currentUser) {
        [self.userDefaults removeObjectForKey:kCurrentUser];
        _currentUser = nil;

        return;
    }

    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:currentUser];
    [self.userDefaults setObject:userData forKey:kCurrentUser];
    _currentUser = currentUser;
}

- (HHUser *)currentUser {
    if (!_currentUser) {
        NSData *userData = [self.userDefaults objectForKey:kCurrentUser];
        _currentUser = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    }

    return _currentUser;
}

- (void)setAccessToken:(NSString *)accessToken {
    if (!accessToken) {
        [self.userDefaults removeObjectForKey:kKeyAppAccessToken];

        return;
    }

    [self.userDefaults setObject:accessToken forKey:kKeyAppAccessToken];
}

- (NSString *)accessToken {
    return [self.userDefaults objectForKey:kKeyAppAccessToken];
}

- (void)setCorrectPIN:(NSString *)correctPIN {
    if (!correctPIN) {
        [self.userDefaults removeObjectForKey:kUserDefaultPinKey];

        return;
    }

    [self.userDefaults setObject:correctPIN forKey:kUserDefaultPinKey];
}

- (NSString *)correctPIN {
    return [self.userDefaults objectForKey:kUserDefaultPinKey];
}

- (void)setIsHelpShown:(BOOL)isHelpShown {
    [self.userDefaults setBool:isHelpShown forKey:kIsHelpShown];
}

- (BOOL)isHelpShown {
    return [self.userDefaults boolForKey:kIsHelpShown];
}

@end
