//
//  HHUserSettings.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HHUser;

@interface HHUserSettings : NSObject

@property (nonatomic) NSString *deviceToken;
@property (nonatomic) NSString *accessToken;

@property (nonatomic) NSString *correctPIN;

@property (nonatomic) BOOL isHelpShown;
@property (nonatomic, getter=isDemoMode) BOOL demoMode;
@property (nonatomic) HHUser *currentUser;

@property (nonatomic) BOOL openAlerts;
@property (nonatomic) NSArray *deviceOwnerArray;
@property (nonatomic) NSDictionary *currentDeviceOwner;
@property (nonatomic) NSInteger currentTimeExtensionID;

+ (instancetype)sharedSettings;

@end
