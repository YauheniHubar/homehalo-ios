//
//  HHConstants.h
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

extern NSString *const kBaseURL;
extern NSString *const kBaseApiURL;

extern NSString *const kBuyHomeHaloRedirectURL;
extern NSString *const kBuyHomeHaloURL;

extern NSString *const kAuthenticationApiKey;
extern NSString *const kResetPasswordApiKey;
extern NSString *const kUsersApiKey;
extern NSString *const kHelpApiKey;
extern NSString *const kContentProfileApiKey;
extern NSString *const kContentCategoriesApiKey;
extern NSString *const kDeviceOwnersApiKey;
extern NSString *const kTimeExtensionsApiKey;
extern NSString *const kHomeworkCategoriesApiKey;
extern NSString *const kRestartApiKey;
extern NSString *const kWhitelistApiKey;
extern NSString *const kBlacklistApiKey;
extern NSString *const kDevicesWithSortApiKey;
extern NSString *const kGetBrowsingHistoryApiKey;
extern NSString *const kGetAlertsApiKey;
extern NSString *const kNotificationsApiKey;
extern NSString *const kDevicesApiKey;
extern NSString *const kPasswordApiKey;
extern NSString *const kSecretTokenKey;
extern NSString *const kAccessTokenKey;
extern NSString *const kDemoModApiKey;

// Notifications
extern NSString *const HHLogoutNotification;
