//
//  HHAlertsViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 2/6/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHAlertsViewController.h"
#import "HHAlertsDetailsViewController.h"
#import "HHAssignDeviceViewController.h"
#import "HHExtendViewController.h"
#import "HHUser.h"
#import "HHAPI+Alerts.h"
#import "HHNotificationsResponse.h"
#import "HHNotification.h"
#import "HHAPI+Devices.h"
#import <CoreText/CoreText.h>

static NSString *const kAlertCell = @"kAlertCell";
static NSString *const kSegueIdToAlertsDetailsScreen = @"kSegueIdToAlertsDetailsScreen";
static NSString *const kTimeExtensionRequestType = @"time_extension_request";
static NSString *const kWhitelistRequestType = @"whitelist_request";
static NSString *const kNewDeviceAttachedRequestType = @"new_device_attached";

static NSString *const kAssignSegue = @"kAssignSegue";
static NSString *const kTimeExtendSegue = @"kTimeExtendSegue";
static NSString *const kHomeSegue = @"kHomeSegue";

@interface HHAlertsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *noAlertsLabel;

@property (nonatomic, getter=isBackToAlerts) BOOL backToAlerts;
@property (nonatomic) NSMutableArray<HHNotification *> *notifications;

@end

@implementation HHAlertsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.backToAlerts = NO;
    self.screenName = self.userSettings.isDemoMode ? LS(@"DemoAlertsScreen") : LS(@"AlertsScreen");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if (!self.userSettings.deviceOwnerArray || self.userSettings.deviceOwnerArray.count == 0) {
        __weak __typeof(self) weakSelf = self;

        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HHAPI sharedAPI] requestDeviceOwnersWithSuccess:^(NSDictionary *responseObject) {

            [weakSelf.tableView reloadData];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        } failure:^(NSError *error) {

            [HHUtils alertErrorMessage:error.localizedDescription];
            [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

        }];
    }

    self.navigationController.navigationBarHidden = YES;
    [self updateAlerts];
}

- (void)updateAlerts {
    __weak __typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestAlertsWithSuccess:^(HHNotificationsResponse *notificationsResponse) {

        weakSelf.notifications = [notificationsResponse.notifications mutableCopy];
        NSUInteger notificationsCount = weakSelf.notifications.count;

        if (weakSelf.isBackToAlerts && notificationsCount == 0) {
            [weakSelf backAlertBtnTouch:nil];
        } else {
            weakSelf.backToAlerts = YES;
        }

        [UIApplication sharedApplication].applicationIconBadgeNumber = notificationsCount;
        [HHUserSettings sharedSettings].currentUser.notificationCount = @(notificationsCount);
        weakSelf.noAlertsLabel.hidden = (notificationsCount > 0);
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSegueIdToAlertsDetailsScreen]) {
        HHNotification *notification = sender;
        HHAlertsDetailsViewController* vc = segue.destinationViewController;
        vc.notification = notification;
    }
}


#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.notifications.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAlertCell];

    HHNotification *notification = self.notifications[(NSUInteger) indexPath.row];
    
    UILabel *title = [cell.contentView viewWithTag:1];
    UILabel *secontStr = [cell.contentView viewWithTag:2];
    UILabel *thirdStr = [cell.contentView viewWithTag:3];
    UILabel *descript = [cell.contentView viewWithTag:4];
    UIImageView *image = [cell.contentView viewWithTag:5];

    thirdStr.text = @"";
    descript.text = [HHUtils getTimeAgo:notification.createdAt];

    if ([notification.type isEqualToString:kTimeExtensionRequestType]) {
        cell.backgroundColor = RGB(255, 75, 0);
        image.image = [UIImage imageNamed:@"extend_user_icon.png"];
        title.text = notification.data[@"deviceOwner"][@"title"];
        secontStr.text = @"Please extend my time";
    } else if ([notification.type isEqualToString:kNewDeviceAttachedRequestType]) {
        cell.backgroundColor = RGB(255, 175, 0);
        image.image = [UIImage imageNamed:@"devices_user_icon.png"];
        
        const CGFloat fontSize = 17;
        UIFont *boldFont = [UIFont boldSystemFontOfSize:fontSize];
        UIFont *regularFont = [UIFont systemFontOfSize:fontSize];
        UIColor *foregroundColor = [UIColor whiteColor];
        
        NSDictionary *attrs = @{
                NSFontAttributeName : regularFont,
                NSForegroundColorAttributeName : foregroundColor
        };
        NSDictionary *subAttrs = @{ NSFontAttributeName : boldFont };
        const NSRange range = NSMakeRange(0, 10);
        
        NSString *deviceName = notification.data[@"title"];

        if (deviceName.length > 15) {
            deviceName = [deviceName substringToIndex:15];
            deviceName = [NSString stringWithFormat:@"%@...", deviceName];
        }

        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"New Device %@", deviceName] attributes:attrs];
        [attributedText setAttributes:subAttrs range:range];
        title.attributedText = attributedText;

        if (![notification.data[@"manufacturer_title"] isKindOfClass:NSNull.class]) {
            secontStr.text = notification.data[@"manufacturer_title"];
        } else {
            secontStr.text = @"n/a";
        }

        thirdStr.text = [NSString stringWithFormat:@"MAC: %@", notification.data[@"mac_address"]];
    } else if ([notification.type isEqualToString:kWhitelistRequestType]) {
        cell.backgroundColor = [UIColor orangeColor];
        image.image = [UIImage imageNamed:@"whitelist_user_icon.png"];
        title.text = [NSString stringWithFormat:@"%@ - please unblock", notification.data[@"deviceOwner"][@"title"]];
        
        NSString *siteAddress = notification.data[@"url"];

        if ([notification.data[@"url"] containsString:@"http://"] || [notification.data[@"url"] containsString:@"https://"] || [notification.data[@"url"] containsString:@"www."]) {
            siteAddress = [siteAddress stringByReplacingOccurrencesOfString:@"https://" withString:@""];
            siteAddress = [siteAddress stringByReplacingOccurrencesOfString:@"http://" withString:@""];
            siteAddress = [siteAddress stringByReplacingOccurrencesOfString:@"www." withString:@""];
        }

        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:siteAddress];
        [attString addAttribute:(NSString*)kCTUnderlineStyleAttributeName
                          value:@(kCTUnderlineStyleSingle)
                          range:(NSRange){0, (attString.length)}];
        secontStr.attributedText = attString;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HHNotification *notification = self.notifications[(NSUInteger) indexPath.row];

    for (NSDictionary *deviceOwner in self.userSettings.deviceOwnerArray) {
        if ([deviceOwner[@"title"] isEqualToString:notification.data[@"deviceOwner"][@"title"]]) {
            self.userSettings.currentDeviceOwner = deviceOwner;

            break;
        }
    }

    if ([notification.type isEqualToString:kTimeExtensionRequestType]) {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:notification.data];
        data[@"status"] = @"accepted";
        NSMutableDictionary *newData = [NSMutableDictionary new];
        newData[@"data"] = data;
        newData[@"isViewed"] = @"1";
        HHExtendViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:kTimeExtendSegue];
        vc.alertData = newData;
        vc.alertID = notification.uid;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([notification.type isEqualToString:kNewDeviceAttachedRequestType]) {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:notification.data];
        data[@"status"] = @"accepted";
        NSMutableDictionary *newData = [NSMutableDictionary new];
        newData[@"data"] = data;
        newData[@"isViewed"] = @"1";
        __weak __typeof(self) weakSelf = self;
        
        HHAssignDeviceViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:kAssignSegue];
        vc.deviceInfo = notification.data;
        vc.alertData = newData;
        vc.alertID = notification.uid;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    } else if ([notification.type isEqualToString:kWhitelistRequestType]) {
        [self performSegueWithIdentifier:kSegueIdToAlertsDetailsScreen sender:notification];
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)backAlertBtnTouch:(id)sender {
    if (self.returnToHome) {
        CATransition *transition = [CATransition animation];
        transition.duration = .45;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;

        HHBaseViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:kHomeSegue];
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self backButtonTouch:nil];
    }
}

@end
