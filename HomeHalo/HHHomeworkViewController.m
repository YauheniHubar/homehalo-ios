//
//  HHHomeworkViewController.m
//  HomeHalo
//
//  Created by naumov.kirill on 2/2/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHHomeworkViewController.h"
#import "EFCircularSlider.h"
#import "HHUsersViewController.h"
#import "HHAPI.h"

static NSString *const kHomeworkCell = @"kHomeworkCell";

@interface HHHomeworkViewController () <UITableViewDelegate, UITableViewDataSource>
{
    EFCircularSlider* circularSlider;
}

@property (nonatomic, weak) IBOutlet UILabel *navigationLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *homeButton;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *minutesLabel;

@property (nonatomic, weak) IBOutlet UIButton *applyButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@property (nonatomic, weak) IBOutlet UILabel *homeworkActivLabel;

@property (nonatomic, weak) IBOutlet UIView *lastView;

@property (nonatomic) NSArray *categoriesArray;

@property (nonatomic) NSMutableArray *selectCategoriesArray;

@property (nonatomic) NSInteger currentTime;

@end

@implementation HHHomeworkViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([HHUserSettings sharedSettings].isDemoMode) {
        self.screenName = LS(@"DemoHomeworkScreen");
    } else {
        self.screenName = LS(@"HomeworkScreen");
    }

    self.navigationLabel.text = [HHUserSettings sharedSettings].currentDeviceOwner[@"title"];
    [self.tableView setHidden:YES];
    [self.homeworkActivLabel setHidden:YES];
    
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestHomeworkCategoriesWithSuccess:^(NSDictionary *responseObject) {

        weakSelf.categoriesArray = [NSArray arrayWithArray:responseObject[@"categories"]];
        weakSelf.selectCategoriesArray = [NSMutableArray arrayWithArray:[HHUserSettings sharedSettings].currentDeviceOwner[@"homeworkCategories"]];

        NSDate *dateFromString = [[HHUtils dateFormatterUTC] dateFromString:[HHUserSettings sharedSettings].currentDeviceOwner[@"homeworkModeStartedAt"][@"dateTime"]];
        NSNumber* durationTime = @([[HHUserSettings sharedSettings].currentDeviceOwner[@"homeworkModeDuration"] doubleValue]);
        NSDate *date = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];

        if ([date compare:[NSDate date]] == NSOrderedDescending) {
            if (durationTime != nil && durationTime.integerValue != 0) {
                NSString *homeworkString = [NSString stringWithFormat:@"%@ %@", LS(@"HomeworkModeActive:"), [weakSelf getTimeStringWithStartDate:date]];
                NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:homeworkString];
                [text addAttribute:NSForegroundColorAttributeName value:RGB(115, 180, 6) range:NSMakeRange(13, text.length-13)];
                weakSelf.homeworkActivLabel.attributedText = text;
                weakSelf.homeworkActivLabel.hidden = NO;
            } else {
                weakSelf.selectCategoriesArray = [NSMutableArray new];
            }
        }

        [weakSelf.tableView reloadData];
        weakSelf.tableView.hidden = NO;
        [weakSelf checkSwitchState];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
    
    self.currentTime = 0;
    
    CGRect sliderFrame = CGRectMake(120, 150, 270, 270);
    circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    circularSlider.center = CGPointMake(self.view.frame.size.width/2, self.lastView.frame.origin.y + self.lastView.frame.size.height + 146);
    circularSlider.lineWidth = 40;
    
    if(IS_IPHONE_4) {
        CGRect sliderFrame = CGRectMake(120, 150, 200, 200);
        circularSlider.frame = sliderFrame;
        circularSlider.center = CGPointMake(self.view.frame.size.width/2, self.lastView.frame.origin.y + self.lastView.frame.size.height + 100);
        circularSlider.lineWidth = 30;
    }
    
    circularSlider.unfilledColor = [UIColor lightGrayColor];
    circularSlider.filledColor = [UIColor colorWithRed:115/255.0f green:180/255.0f blue:6/255.0f alpha:1.0f];
    circularSlider.handleType = CircularSliderHandleTypeBigCircle;
    circularSlider.handleColor = [UIColor blueColor];
    
    [circularSlider addTarget:self action:@selector(valueDidChange:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:circularSlider];
    [self.view sendSubviewToBack:circularSlider];
    
    [self.view bringSubviewToFront:self.timeLabel];
    [self.view bringSubviewToFront:self.minutesLabel];
    [self.view bringSubviewToFront:self.tableView];
    [self.view bringSubviewToFront:self.homeworkActivLabel];
    
    
    [self.homeButton setHidden:NO];
    [self.applyButton setHidden:YES];
    [self.cancelButton setHidden:YES];
    [self setStartValue];
    
    NSTimer *timerStartValue = [NSTimer scheduledTimerWithTimeInterval:20.0f target:self selector:@selector(setStartValue) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timerStartValue forMode:NSRunLoopCommonModes];
}

- (void)setStartValue
{
    NSDate* dateFromString = [[HHUtils dateFormatterUTC] dateFromString:[HHUserSettings sharedSettings].currentDeviceOwner[@"homeworkModeStartedAt"][@"dateTime"]];
    NSNumber* durationTime = @([[HHUserSettings sharedSettings].currentDeviceOwner[@"homeworkModeDuration"] doubleValue]);
    NSDate* date = [NSDate dateWithTimeInterval:durationTime.doubleValue sinceDate:dateFromString];
    
    NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:date];
    NSInteger distanceBetweenDatesInt = distanceBetweenDates;
    
    if ([date compare:[NSDate date]] == NSOrderedAscending)
        distanceBetweenDatesInt = 0;
    if (distanceBetweenDatesInt < 0)
        distanceBetweenDatesInt *= -1;
    NSInteger minutes = distanceBetweenDatesInt / 60;
    if (distanceBetweenDatesInt%60 > 30)
        minutes++;
    
    if (minutes > 0 && minutes < 720)
    {
        [self.homeButton setHidden:YES];
        [self.applyButton setHidden:NO];
        [self.cancelButton setHidden:NO];
    }
    
    float value = 0;
    if (minutes <= 30)
    {
        value = minutes*1.1f;
    }
    else if (minutes > 30 && minutes < 120)
    {
        minutes -= 30;
        value = 33.3f*(minutes/90.f) + 33.3f;
        
    }
    else
    {
        minutes -= 120;
        value = 5.5f*(minutes/10.f) + 66.6f;
    }
    [circularSlider setStartValue:value];
}

#pragma mark -
#pragma mark Actions

- (IBAction)applyBtnTouch:(id)sender
{
    [self homeworkBtnTouch:nil];
}

- (IBAction)cancelBtnTouch:(id)sender {
    NSMutableDictionary *request = [self configureRequest];
    [request setValue:[NSString stringWithFormat:@"%d", 0] forKey:@"homeworkModeDuration"];
    
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestBlockUser:request success:^(NSDictionary *responseObject) {

        weakSelf.userSettings.currentDeviceOwner = responseObject[@"deviceOwner"];
        [weakSelf unwindToViewControllerOfClass:HHUsersViewController.class animated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (IBAction)homeworkBtnTouch:(id)sender
{
    NSMutableDictionary *request = [self configureRequest];
    [request setValue:[NSString stringWithFormat:@"%d", (int)self.currentTime*60] forKey:@"homeworkModeDuration"];
    
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HHAPI sharedAPI] requestBlockUser:request success:^(NSDictionary *responseObject) {

        weakSelf.userSettings.currentDeviceOwner = responseObject[@"deviceOwner"];
        [weakSelf unwindToViewControllerOfClass:HHUsersViewController.class animated:YES];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    } failure:^(NSError *error) {

        [HHUtils alertErrorMessage:error.localizedDescription];
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];

    }];
}

- (NSMutableDictionary*)configureRequest
{
    NSMutableDictionary *request = [NSMutableDictionary new];
    NSDictionary *data = [HHUserSettings sharedSettings].currentDeviceOwner;
    
    [request setValue:data[@"title"] forKey:@"title"];
    [request setValue:data[@"id"] forKey:@"id"];
    [request setValue:data[@"status"] forKey:@"status"];
    [request setValue:data[@"createdAt"] forKey:@"createdAt"];
    
    [request setValue:data[@"access"] forKey:@"access"];
    [request setValue:data[@"urlBlacklists"] forKey:@"urlBlacklists"];
    [request setValue:data[@"urlWhitelists"] forKey:@"urlWhitelists"];
    [request setValue:data[@"deviceOnlineCount"] forKey:@"deviceOnlineCount"];
    [request setValue:data[@"isDefault"] forKey:@"isDefault"];
    [request setValue:data[@"isDeletable"] forKey:@"isDeletable"];
    [request setValue:data[@"newNotificationCount"] forKey:@"newNotificationCount"];
    [request setValue:data[@"status"] forKey:@"status"];
    
    [request setValue:[HHUtils currentDateUTC] forKey:@"homeworkModeStartedAt"];
    [request setValue:data[@"contentProfile"][@"id"] forKey:@"contentProfile"];
    [request setValue:self.selectCategoriesArray forKey:@"homeworkCategories"];
    
    NSMutableArray *timeProfilesArray = [NSMutableArray new];
    for(NSDictionary *timeData in data[@"timeProfiles"])
    {
        NSMutableDictionary *obj = [NSMutableDictionary new];
        [obj setValue:timeData[@"dayOfWeek"] forKey:@"dayOfWeek"];
        [obj setValue:timeData[@"startTime"][@"time"] forKey:@"startTime"];
        [obj setValue:timeData[@"endTime"][@"time"] forKey:@"endTime"];
        [timeProfilesArray addObject:obj];
    }
    [request setValue:timeProfilesArray forKey:@"timeProfiles"];
    return request;
}

- (void)valueDidChange:(EFCircularSlider*)slider
{
    int time = 0;
    if (slider.currentValue < 33.3f)
    {
        time = (int)(slider.currentValue/1.1f);
        self.timeLabel.text = [NSString stringWithFormat:@"%d", time];
        self.minutesLabel.text = LS(@"minutes");
    }
    else if (slider.currentValue > 33.3f && slider.currentValue < 66.6f)
    {
        int division = (slider.currentValue - 33.3f)/3.6f;
        time = 30 + 10*division;
        if ((time % 60) == 0)
        {
            self.timeLabel.text = [NSString stringWithFormat:@"%d", time/60];
            self.minutesLabel.text = LS(@"hours");
        }
        else if ((time / 60) >= 1)
        {
            self.timeLabel.text = [NSString stringWithFormat:@"%d:%d", time/60, time%60];
            self.minutesLabel.text = LS(@"hours");
        }
        else
        {
            self.timeLabel.text = [NSString stringWithFormat:@"%d", time];
            self.minutesLabel.text = LS(@"minutes");
        }
    }
    else
    {
        int division = (slider.currentValue-66.6f)/5.f;
        time = 10*division + 2*60;
        if ((time % 60) == 0)
            self.timeLabel.text = [NSString stringWithFormat:@"%d", time/60];
        else
            self.timeLabel.text = [NSString stringWithFormat:@"%d:%d", time/60, time%60];
        self.minutesLabel.text = LS(@"hours");
    }
    self.currentTime = time;
}

#pragma mark -
#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.categoriesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHomeworkCell];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    titleLabel.text = self.categoriesArray[indexPath.row][@"title"];
    
    UISwitch *img = (UISwitch *)[cell.contentView viewWithTag:2];
    img.backgroundColor = [UIColor redColor];
    img.layer.cornerRadius = 16.0;
    [img setOn:YES];
    for (NSDictionary *dict in self.selectCategoriesArray) {
        if ([dict[@"categoryId"] integerValue] == [self.categoriesArray[indexPath.row][@"categoryId"] integerValue])
        {
            img.on = !img.on;
        }
    }
    img.tag = indexPath.row+2;
    [img addTarget:self action:@selector(switchBtnTouch:) forControlEvents:UIControlEventValueChanged];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UISwitch *img = (UISwitch *)[cell.contentView viewWithTag:indexPath.row+2];
    if ([self.selectCategoriesArray indexOfObject:self.categoriesArray[indexPath.row]] < 10)
    {
        [self.selectCategoriesArray addObject:self.categoriesArray[indexPath.row]];
        [img setOn:YES animated:YES];
    }
    else
    {
        [self.selectCategoriesArray removeObject:self.categoriesArray[indexPath.row]];
        [img setOn:NO animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark - Actions

- (void)switchBtnTouch:(UISwitch*)switchBtn
{
    NSInteger index = switchBtn.tag - 2;
    if (!switchBtn.isOn)
    {
        [self checkSwitchState];
        [self.selectCategoriesArray addObject:self.categoriesArray[index]];
    }
    else
    {
        [self.selectCategoriesArray removeObject:self.categoriesArray[index]];
    }
    [self checkSwitchState];
}

- (void)checkSwitchState
{
    NSInteger count = self.categoriesArray.count;
    for (NSInteger i = 0; i < self.categoriesArray.count; i++)
    {
        NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:0];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:path];
        UISwitch *img = (UISwitch *)[cell.contentView viewWithTag:path.row+2];
        if (img.on)
            count--;
    }
    if (count == 0)
    {
        [self.homeButton setEnabled:NO];
        (self.homeButton).backgroundColor = [UIColor grayColor];
        
        [self.applyButton setEnabled:NO];
        (self.applyButton).backgroundColor = [UIColor grayColor];
        
        [self.homeButton setTitle:LS(@"PleaseBlockAtLeastOneCategory") forState:UIControlStateNormal];
    }
    else
    {
        [self.homeButton setEnabled:YES];
        [self.homeButton setBackgroundColor:RGB(115, 180, 6)];
        
        [self.applyButton setEnabled:YES];
        [self.applyButton setBackgroundColor:RGB(115, 180, 6)];
        
        [self.homeButton setTitle:LS(@"StartHomework") forState:UIControlStateNormal];
    }
}

- (NSString*)getTimeStringWithStartDate:(NSDate*)date
{
    NSString *descriptionStr;
    
    NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:date];
    NSInteger distanceBetweenDatesInt = distanceBetweenDates;
    if (distanceBetweenDatesInt < 0)
        distanceBetweenDatesInt *= -1;
    NSInteger minutes = distanceBetweenDatesInt / 60;
    if (distanceBetweenDatesInt%60 > 30)
        minutes++;
    
    if (minutes > 120)
    {
        if ((minutes % 60) == 0)
            descriptionStr = [NSString stringWithFormat:@"%d", (int)minutes/60];
        else
            descriptionStr = [NSString stringWithFormat:@"%d:%d", (int)minutes/60, (int)minutes%60];
        self.minutesLabel.text = LS(@"hours");
    }
    else if (minutes <= 120 && minutes > 60)
    {
        int hours = (int)minutes/60;
        descriptionStr = [NSString stringWithFormat:@"%d %@ %d %@", hours, LS(@"hrs"), (int)minutes%60, LS(@"mins")];
    }
    else if (minutes == 60)
    {
        descriptionStr = [NSString stringWithFormat:@"1 %@", LS(@"hours")];
    }
    else
    {
        descriptionStr = [NSString stringWithFormat:@"%ld %@", (long)minutes, LS(@"minutes")];
    }
    return descriptionStr;
}

@end

