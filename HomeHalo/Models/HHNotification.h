//
//  HHNotification.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/10/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/MTLModel.h>
#import <Mantle/MTLJSONAdapter.h>

@interface HHNotification : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSNumber *uid;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSDictionary *data;
@property (nonatomic, copy) NSDate *createdAt;

@end
