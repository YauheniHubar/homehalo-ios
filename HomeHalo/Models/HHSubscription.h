//
//  HHSubscription.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/MTLModel.h>
#import <Mantle/MTLJSONAdapter.h>

@interface HHSubscription : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) BOOL isActive;

@end
