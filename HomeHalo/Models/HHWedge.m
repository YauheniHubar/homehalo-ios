//
//  HHWedge.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/NSValueTransformer+MTLPredefinedTransformerAdditions.h>
#import "HHWedge.h"

@implementation HHWedge

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
            @"ssid": @"ssid",
            @"password": @"password",
            @"firmware": @"firmware",
            @"isOnline": @"isOnline",
            @"timezone": @"timezone"
    };
}

+ (NSValueTransformer *)isOnlineJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

@end
