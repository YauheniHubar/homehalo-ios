//
//  HHWedge.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/MTLModel.h>
#import <Mantle/MTLJSONAdapter.h>

@interface HHWedge : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *ssid;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *firmware;
@property (nonatomic, readonly) BOOL isOnline;
@property (nonatomic, copy) NSString *timezone;

@end
