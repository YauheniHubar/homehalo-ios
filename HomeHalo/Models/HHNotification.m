//
//  HHNotification.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/10/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/MTLValueTransformer.h>
#import "HHNotification.h"

@implementation HHNotification

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
            @"uid": @"id",
            @"type": @"type",
            @"data": @"data",
            @"createdAt": @"createdAt"
    };
}

+ (NSValueTransformer *)createdAtJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSDictionary *createdAt, BOOL *success, NSError *__autoreleasing *error) {

        return [[HHUtils dateFormatter] dateFromString:createdAt[@"dateTime"]];

    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {

        return nil; //[HHUtils timeStringFromDate:date];

    }];
}

@end
