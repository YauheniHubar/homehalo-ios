//
//  HHSubscription.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/NSValueTransformer+MTLPredefinedTransformerAdditions.h>
#import "HHSubscription.h"

@implementation HHSubscription

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"isActive": @"isActive",
             };
}

+ (NSValueTransformer *)isActiveJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

@end
