//
//  HHUser.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/MTLModel.h>
#import <Mantle/MTLJSONAdapter.h>

@class HHWedge;
@class HHSubscription;

@interface HHUser : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSNumber *uid;
@property (nonatomic, copy) NSNumber *notificationCount;

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSNumber *emailStepSkipButtonFlag;

@property (nonatomic, copy) HHSubscription *subscription;
@property (nonatomic, copy) HHWedge *wedge;

@property (nonatomic) BOOL hasFullAccess;

@end
