//
//  HHUser.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/NSValueTransformer+MTLPredefinedTransformerAdditions.h>
#import "HHUser.h"
#import "HHWedge.h"
#import "HHSubscription.h"

@implementation HHUser

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
            @"uid": @"id",
            @"notificationCount": @"newNotificationCount",
            @"email": @"email",
            @"emailStepSkipButtonFlag" : @"emailStepSkipButtonFlag",
            @"subscription": @"subscription",
            @"wedge": @"wedge",
            @"hasFullAccess": @"hasFullAccess"
             };
}

+ (NSValueTransformer *)subscriptionJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:HHSubscription.class];
}

+ (NSValueTransformer *)wedgeJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:HHWedge.class];
}

+ (NSValueTransformer *)hasFullAccessJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

@end
