//
//  HHNotificationsResponse.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/10/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/MTLModel.h>
#import <Mantle/MTLJSONAdapter.h>

@class HHNotification;

@interface HHNotificationsResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSNumber *code;
@property (nonatomic, readonly) BOOL isSuccess;
@property (nonatomic, copy) NSArray<HHNotification *> *notifications;

@end
