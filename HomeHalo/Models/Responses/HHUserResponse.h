//
//  HHUserResponse.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/MTLModel.h>
#import <Mantle/MTLJSONAdapter.h>

@class HHUser;

@interface HHUserResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, copy) NSNumber *code;
@property (nonatomic, copy) NSString *serverTimezone;
@property (nonatomic, readonly) BOOL isSuccess;
@property (nonatomic, copy) HHUser *user;

@end
