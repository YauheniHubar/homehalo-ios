//
//  HHUserResponse.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/NSValueTransformer+MTLPredefinedTransformerAdditions.h>
#import "HHUserResponse.h"
#import "HHUser.h"

@implementation HHUserResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
            @"accessToken": @"accessToken",
            @"code": @"code",
            @"serverTimezone": @"serverTimezone",
            @"isSuccess": @"success",
            @"user": @"user",
    };
}

+ (NSValueTransformer *)isSuccessJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

+ (NSValueTransformer *)userJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:HHUser.class];
}

@end
