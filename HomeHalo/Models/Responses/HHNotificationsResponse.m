//
//  HHNotificationsResponse.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/10/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <Mantle/NSValueTransformer+MTLPredefinedTransformerAdditions.h>
#import "HHNotificationsResponse.h"
#import "HHNotification.h"

@implementation HHNotificationsResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"code": @"code",
             @"isSuccess": @"success",
             @"notifications": @"notifications",
             };
}

+ (NSValueTransformer *)isSuccessJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

+ (NSValueTransformer *)notificationsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:HHNotification.class];
}

@end
