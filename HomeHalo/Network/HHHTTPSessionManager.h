//
//  HHHTTPSessionManager.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface HHHTTPSessionManager : AFHTTPSessionManager

- (NSURLSessionDataTask *)hh_GET:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(id responseObject))success
                         failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)hh_HEAD:(NSString *)URLString
                       parameters:(id)parameters
                          success:(void (^)(id responseObject))success
                          failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)hh_POST:(NSString *)URLString
                       parameters:(id)parameters
                          success:(void (^)(id responseObject))success
                          failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)hh_POST:(NSString *)URLString
                       parameters:(id)parameters
        constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                          success:(void (^)(id responseObject))success
                          failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)hh_PUT:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(id responseObject))success
                         failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)hh_PATCH:(NSString *)URLString
                        parameters:(id)parameters
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)hh_DELETE:(NSString *)URLString
                         parameters:(id)parameters
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))failure;

@end
