//
//  HHAPI.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHHTTPSessionManager.h"

@class HHNotification;

typedef NS_ENUM(NSUInteger, PeriodType) {
    PeriodTypeDay,
    PeriodTypeWeek,
    PeriodTypeMonth
};

typedef NS_ENUM(NSUInteger, StatusType) {
    StatusTypeNotBlocked,
    StatusTypeCatBlock,
    StatusTypeBlacklisted
};

@interface HHAPI : HHHTTPSessionManager

+ (HHAPI *)sharedAPI;

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSMutableURLRequest *requestForWebView;

- (NSURLSessionDataTask *)requestHelpWithEmail:(NSString *)email
                                       message:(NSString *)message
                                       success:(void (^)(NSDictionary *responseObject))success
                                       failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestContentProfileWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                   failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestContentCategoriesProfileWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                             failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestAddContentProfile:(NSString *)title
                                           success:(void (^)(NSDictionary *responseObject))success
                                           failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestSaveChanges:(NSDictionary *)param
                                   profileID:(NSInteger)profileID
                                     success:(void (^)(NSDictionary *responseObject))success
                                     failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestRestartWithSuccess:(void (^)(NSDictionary *responseObject))success
                                            failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestChangePassword:(NSDictionary *)data
                                        success:(void (^)(NSDictionary *responseObject))success
                                        failure:(void (^)(NSError *error))failure;


#pragma mark - User Actions

- (NSURLSessionDataTask *)requestBlockUser:(NSDictionary *)data
                                   success:(void (^)(NSDictionary *responseObject))success
                                   failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestExtendTime:(NSString *)time
                                    success:(void (^)(NSDictionary *responseObject))success
                                    failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestDeleteExtendTime:(NSInteger)extensionId
                                          success:(void (^)(NSDictionary *responseObject))success
                                          failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestCalculateExtendTime:(NSString *)date
                                            duration:(NSString *)duration
                                             success:(void (^)(NSDictionary *responseObject))success
                                             failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestHomeworkCategoriesWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                       failure:(void (^)(NSError *error))failure;


#pragma mark - Whitelists request

- (NSURLSessionDataTask *)requestAddUrl:(NSString *)url
                                success:(void (^)(NSDictionary *responseObject))success
                                failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestUpdateUrl:(NSString *)url
                                     urlID:(NSInteger)urlID
                                   success:(void (^)(NSDictionary *responseObject))success
                                   failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestDeclineUrl:(HHNotification *)notification
                                    success:(void (^)(NSDictionary *responseObject))success
                                    failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestDeleteUrl:(NSInteger)urlID
                                   success:(void (^)(NSDictionary *responseObject))success
                                   failure:(void (^)(NSError *error))failure;


#pragma mark - Blacklists request

- (NSURLSessionDataTask *)requestAddToBlacklist:(NSString *)url
                                        success:(void (^)(NSDictionary *responseObject))success
                                        failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestUpdateBlacklist:(NSString *)url
                                           urlID:(NSInteger)urlID
                                         success:(void (^)(NSDictionary *responseObject))success
                                         failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestDeleteFromBlacklist:(NSInteger)urlID
                                             success:(void (^)(NSDictionary *responseObject))success
                                             failure:(void (^)(NSError *error))failure;


#pragma mark - Devices

- (NSURLSessionDataTask *)requestGetDevicesWithSuccess:(void (^)(NSDictionary *responseObject))success
                                               failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestUpdateDevice:(NSDictionary *)param
                                     deviceID:(NSInteger)deviceID
                                      success:(void (^)(NSDictionary *responseObject))success
                                      failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestDeleteDevice:(NSInteger)deviceID
                                      success:(void (^)(NSDictionary *responseObject))success
                                      failure:(void (^)(NSError *error))failure;


#pragma mark - Browsing history

- (NSURLSessionDataTask *)requestGetBrowsingHistory:(NSString *)second
                                            success:(void (^)(NSDictionary *responseObject))success
                                            failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestGetBrowsingHistoryForOwner:(NSString *)second
                                                    success:(void (^)(NSDictionary *responseObject))success
                                                    failure:(void (^)(NSError *error))failure;

#pragma mark - Users

- (NSURLSessionDataTask *)requestAddUser:(NSDictionary *)userInfo
                                 success:(void (^)(NSDictionary *responseObject))success
                                 failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestDeleteUser:(NSInteger)userId
                                    success:(void (^)(NSDictionary *responseObject))success
                                    failure:(void (^)(NSError *error))failure;


#pragma mark - Browsing traffic

- (NSURLSessionDataTask *)requestGetTraffics:(PeriodType)period
                                      status:(NSString *)status
                                     success:(void (^)(NSDictionary *responseObject))success
                                     failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestAddCategories:(NSDictionary *)data
                                       success:(void (^)(NSDictionary *responseObject))success
                                       failure:(void (^)(NSError *error))failure;


#pragma mark - Demo mod

- (NSURLSessionDataTask *)requestEmailSignUpDemoMode:(NSString *)email
                                       success:(void (^)(NSDictionary *responseObject))success
                                       failure:(void (^)(NSError *error))failure;


#pragma mark - Blacklist

- (NSURLSessionDataTask *)requestGetBlacklistCategoryWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                         failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)requestSendBlacklistCategories:(NSArray *)categories
                                                 success:(void (^)(NSDictionary *responseObject))success
                                                 failure:(void (^)(NSError *error))failure;

@end
