//
//  HHAPI+Auth.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHAPI.h"

@class HHUserResponse;
@class HHUser;

@interface HHAPI (Auth)

- (NSURLSessionDataTask *)requestDemoModeWithDeviceToken:(NSString *)deviceToken
                                                 success:(void (^)(HHUserResponse *userResponse))success
                                                 failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestLoginWithUsername:(NSString *)username
                                          password:(NSString *)password
                                       deviceToken:(NSString *)deviceToken
                                           success:(void (^)(HHUserResponse *userResponse))success
                                           failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestUserWithUser:(HHUser *)user
                                      success:(void (^)(HHUserResponse *userResponse))success
                                      failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestResetPasswordWithEmail:(NSString *)email
                                                success:(void (^)(id responseObject))success
                                                failure:(void (^)(NSError *error))failure;

@end
