//
//  HHAPI+Alerts.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/9/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHAPI.h"

@class HHNotificationsResponse;

@interface HHAPI (Alerts)

- (NSURLSessionDataTask *)requestAlertsWithSuccess:(void (^)(HHNotificationsResponse *notificationsResponse))success
                                           failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)requestUpdateAlerts:(id)alerts
                               notificationID:(NSInteger)notificationID
                                      success:(void (^)(id response))success
                                      failure:(void (^)(NSError *error))failure;

@end
