//
//  HHAPI+Alerts.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/9/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHAPI+Alerts.h"
#import "HHUserSettings.h"
#import "HHUser.h"
#import "HHNotificationsResponse.h"

@implementation HHAPI (Alerts)

- (NSURLSessionDataTask *)requestAlertsWithSuccess:(void (^)(HHNotificationsResponse *notificationsResponse))success
                                           failure:(void (^)(NSError *error))failure {
    NSString *resourceString = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kGetAlertsApiKey];
    NSString *URLString = [NSURL URLWithString:resourceString relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (!success) {
            return;
        }

        NSError *parserError = nil;
        HHNotificationsResponse *parsedNotificationsResponse = [MTLJSONAdapter modelOfClass:HHNotificationsResponse.class fromJSONDictionary:responseObject error:&parserError];

        // TODO check error

        success(parsedNotificationsResponse);

    } failure:failure];
}

- (NSURLSessionDataTask *)requestUpdateAlerts:(id)alerts
                               notificationID:(NSInteger)notificationID
                                      success:(void (^)(id response))success
                                      failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kNotificationsApiKey, (int)notificationID];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_PUT:URLString parameters:alerts success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

@end
