//
//  HHAPI.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <AFNetworking/AFURLResponseSerialization.h>
#import "HHAPI.h"
#import "HHJSONResponseSerializer.h"
#import "HHUserSettings.h"
#import "HHUser.h"
#import "HHNotification.h"

static NSString *const kSecretToken = @"2622a9c8314df4eff283969fedbb5ba42b52bd6253591c447d93019e90a8e1a0d5748e892ed5f97ae5c2e1a3a1601f7312f70a41b745d5aa4348313d285bdf88";

@implementation HHAPI

+ (instancetype)sharedAPI {
    static dispatch_once_t once;
    static HHAPI *__sharedAPI = nil;

    dispatch_once(&once, ^{
        __sharedAPI = [self new];
    });

    return __sharedAPI;
}

- (instancetype)init {
    self = [super initWithBaseURL:[NSURL URLWithString:kBaseApiURL]];

    if (!self) {
        return nil;
    }

    [self setupSerializers];

    return self;
}

- (void)setupSerializers {
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    [self.requestSerializer setValue:kSecretToken forHTTPHeaderField:kSecretTokenKey];
    self.responseSerializer = [HHJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
}


#warning TODO Refactor

- (NSMutableURLRequest *)requestForWebView {
    NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?accessToken=%@&redirectUrl=%@", kBuyHomeHaloURL, [HHUserSettings sharedSettings].accessToken, kBuyHomeHaloRedirectURL]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:theURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20.0f];
    theRequest.HTTPMethod = @"GET";
    [theRequest addValue:[HHUserSettings sharedSettings].accessToken forHTTPHeaderField:kAccessTokenKey];

    return theRequest;
}

- (NSURLSessionDataTask *)requestHelpWithEmail:(NSString *)email
                                       message:(NSString *)message
                                       success:(void (^)(NSDictionary *responseObject))success
                                       failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{
            @"email": email,
            @"message": message,
    };
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kHelpApiKey];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

- (NSURLSessionDataTask *)requestContentProfileWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                   failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kContentProfileApiKey];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

- (NSURLSessionDataTask *)requestContentCategoriesProfileWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                             failure:(void (^)(NSError *error))failure {
    NSString *URLString = [NSURL URLWithString:kContentCategoriesApiKey relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

- (NSURLSessionDataTask *)requestAddContentProfile:(NSString *)title
                                           success:(void (^)(NSDictionary *responseObject))success
                                           failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{ @"title": title };
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kContentProfileApiKey];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

- (NSURLSessionDataTask *)requestSaveChanges:(NSDictionary *)param
                                   profileID:(NSInteger)profileID
                                     success:(void (^)(NSDictionary *responseObject))success
                                     failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kContentProfileApiKey, (int)profileID];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_PUT:URLString parameters:param success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

- (NSURLSessionDataTask *)requestRestartWithSuccess:(void (^)(NSDictionary *responseObject))success
                                            failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kRestartApiKey];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

- (NSURLSessionDataTask *)requestChangePassword:(NSDictionary *)data
                                        success:(void (^)(NSDictionary *responseObject))success
                                        failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kPasswordApiKey];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_PUT:URLString parameters:data success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}


#pragma mark - User Actions

- (NSURLSessionDataTask *)requestBlockUser:(NSDictionary *)data
                                   success:(void (^)(NSDictionary *responseObject))success
                                   failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"]];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_PUT:URLString parameters:data success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestExtendTime:(NSString *)time
                                    success:(void (^)(NSDictionary *responseObject))success
                                    failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{
            @"status" : @"accepted",
            @"duration" : time
    };

    if ([HHUserSettings sharedSettings].currentDeviceOwner[@"timeExtension"] != nil) {
        NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kTimeExtensionsApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"timeExtension"][@"id"]];
        NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

        return [self hh_PUT:URLString parameters:parameters success:^(id responseObject) {

            if (success) {
                success(responseObject);
            }

        } failure:failure];
    } else {
        NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kTimeExtensionsApiKey];
        NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

        return [self hh_POST:URLString parameters:parameters success:^(id responseObject) {

            if (success) {
                success(responseObject);
            }

        } failure:failure];
    }
}
- (NSURLSessionDataTask *)requestDeleteExtendTime:(NSInteger)extensionId
                                          success:(void (^)(NSDictionary *responseObject))success
                                          failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kTimeExtensionsApiKey, (int)extensionId];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_DELETE:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestCalculateExtendTime:(NSString *)date
                                            duration:(NSString *)duration
                                             success:(void (^)(NSDictionary *responseObject))success
                                             failure:(void (^)(NSError *error))failure {
    NSMutableString *urlFormat = [NSMutableString stringWithFormat:@"%@/%@/%@/%@/access?duration=%@&terId=0&status=accepted", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], duration];

    if (date) {
        [urlFormat appendString:[NSString stringWithFormat:@"&startedAt=%@", date]];
    }

    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestHomeworkCategoriesWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                       failure:(void (^)(NSError *error))failure {
    NSString *URLString = [NSURL URLWithString:kHomeworkCategoriesApiKey relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}


#pragma mark - Whitelists request

- (NSURLSessionDataTask *)requestAddUrl:(NSString *)url
                                success:(void (^)(NSDictionary *responseObject))success
                                failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{
            @"status": @"accepted",
            @"url": url,
    };
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kWhitelistApiKey];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestUpdateUrl:(NSString *)url
                                     urlID:(NSInteger)urlID
                                   success:(void (^)(NSDictionary *responseObject))success
                                   failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{
            @"status": @"accepted",
            @"url": url,
    };
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kWhitelistApiKey, (int)urlID];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_PUT:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestDeclineUrl:(HHNotification *)notification
                                    success:(void (^)(NSDictionary *responseObject))success
                                    failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{
            @"status" : @"denied",
            @"url" : notification.data[@"url"],
            @"id" : notification.data[@"id"],
            @"deviceOwner" : notification.data[@"deviceOwner"][@"id"],
            @"createdAt" : (notification.createdAt).description
    };
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kWhitelistApiKey, (int)[notification.data[@"id"] integerValue]];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_PUT:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestDeleteUrl:(NSInteger)urlID
                                   success:(void (^)(NSDictionary *responseObject))success
                                   failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kWhitelistApiKey, (int)urlID];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_DELETE:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}


#pragma mark - Blacklists request

- (NSURLSessionDataTask *)requestAddToBlacklist:(NSString *)url
                                        success:(void (^)(NSDictionary *responseObject))success
                                        failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{
            @"url" : url
    };
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kBlacklistApiKey];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestUpdateBlacklist:(NSString *)url
                                           urlID:(NSInteger)urlID
                                         success:(void (^)(NSDictionary *responseObject))success
                                         failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{
            @"url" : url
    };
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kBlacklistApiKey, (int)urlID];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_PUT:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestDeleteFromBlacklist:(NSInteger)urlID
                                             success:(void (^)(NSDictionary *responseObject))success
                                             failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], kBlacklistApiKey, (int)urlID];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_DELETE:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}


#pragma mark - Devices

- (NSURLSessionDataTask *)requestGetDevicesWithSuccess:(void (^)(NSDictionary *responseObject))success
                                               failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDevicesWithSortApiKey];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestUpdateDevice:(NSDictionary *)param
                                     deviceID:(NSInteger)deviceID
                                      success:(void (^)(NSDictionary *responseObject))success
                                      failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDevicesApiKey, (int)deviceID];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_PUT:URLString parameters:param success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestDeleteDevice:(NSInteger)deviceID
                                      success:(void (^)(NSDictionary *responseObject))success
                                      failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDevicesApiKey, (int)deviceID];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_DELETE:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}


#pragma mark - Browsing history

- (NSURLSessionDataTask *)requestGetBrowsingHistory:(NSString *)second
                                            success:(void (^)(NSDictionary *responseObject))success
                                            failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@?time=%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kGetBrowsingHistoryApiKey, second];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestGetBrowsingHistoryForOwner:(NSString *)second
                                                    success:(void (^)(NSDictionary *responseObject))success
                                                    failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/browsing-traffic?time=%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"], second];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}


#pragma mark - Users

- (NSURLSessionDataTask *)requestAddUser:(NSDictionary *)userInfo
                                 success:(void (^)(NSDictionary *responseObject))success
                                 failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:userInfo success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

- (NSURLSessionDataTask *)requestDeleteUser:(NSInteger)userId
                                    success:(void (^)(NSDictionary *responseObject))success
                                    failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%d", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, (int)userId];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_DELETE:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}


#pragma mark - Browsing traffic

- (NSURLSessionDataTask *)requestGetTraffics:(PeriodType)period
                                      status:(NSString *)status
                                     success:(void (^)(NSDictionary *responseObject))success
                                     failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/urls-browsing-traffic?period=%luu&status=%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"],(unsigned long) (unsigned long)period, status];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestAddCategories:(NSDictionary *)data
                                       success:(void (^)(NSDictionary *responseObject))success
                                       failure:(void (^)(NSError *error))failure {
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/add-categories", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"]];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:data success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}


#pragma mark - Demo mod

- (NSURLSessionDataTask *)requestEmailSignUpDemoMode:(NSString *)email
                                             success:(void (^)(NSDictionary *responseObject))success
                                             failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{
            @"email": email
    };
    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, @"email"];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_PUT:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}


#pragma mark - Blacklist

- (NSURLSessionDataTask *)requestGetBlacklistCategoryWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                         failure:(void (^)(NSError *error))failure {
    NSString *URLString = [NSURL URLWithString:@"custom-content-categories" relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}
- (NSURLSessionDataTask *)requestSendBlacklistCategories:(NSArray *)categories
                                                 success:(void (^)(NSDictionary *responseObject))success
                                                 failure:(void (^)(NSError *error))failure {

    NSMutableArray *parameters = [NSMutableArray new];
    NSNumber *deviceOwnerID = [HHUserSettings sharedSettings].currentDeviceOwner[@"id"];

    for (NSDictionary *category in categories) {
        [parameters addObject:@{
                @"deviceOwner": deviceOwnerID,
                @"contentCategory": category[@"id"]
        }];
    }

    NSString *urlFormat = [NSString stringWithFormat:@"%@/%@/%@/%@/blacklisted-categories", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey, [HHUserSettings sharedSettings].currentDeviceOwner[@"id"]];
    NSString *URLString = [NSURL URLWithString:urlFormat relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

@end
