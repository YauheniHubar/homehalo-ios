//
//  HHJSONResponseSerializer.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface HHJSONResponseSerializer : AFJSONResponseSerializer

@end
