//
//  HHAPI+Auth.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHAPI+Auth.h"
#import "HHUserResponse.h"
#import "HHUser.h"

static NSString *const kKeyAppAccessToken = @"accessToken";

@implementation HHAPI (Auth)

// Demo Mode

- (NSURLSessionDataTask *)requestDemoModeWithDeviceToken:(NSString *)deviceToken
                                                 success:(void (^)(HHUserResponse *userResponse))success
                                            failure:(void (^)(NSError *error))failure {
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"deviceType"] = @"rms_push_notifications.os.ios";

    if (deviceToken) {
        parameters[@"deviceToken"] = deviceToken;
    }

    NSString *URLString = [NSURL URLWithString:kDemoModApiKey relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:[parameters copy] success:^(id responseObject) {

        [self.requestSerializer setValue:responseObject[kKeyAppAccessToken] forHTTPHeaderField:kAccessTokenKey];

        if (success) {
            NSError *parserError = nil;
            HHUserResponse *parsedUserResponse = [MTLJSONAdapter modelOfClass:HHUserResponse.class fromJSONDictionary:responseObject error:&parserError];
            
            // TODO check error
            
            success(parsedUserResponse);
        }

    } failure:failure];
}

// Login

- (NSURLSessionDataTask *)requestLoginWithUsername:(NSString *)username
                                          password:(NSString *)password
                                       deviceToken:(NSString *)deviceToken
                                           success:(void (^)(HHUserResponse *userResponse))success
                                           failure:(void (^)(NSError *error))failure {

    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"deviceType"] = @"rms_push_notifications.os.ios";
    parameters[@"username"] = username;
    parameters[@"password"] = password;

    if (deviceToken) {
        parameters[@"deviceToken"] = deviceToken;
    }

    NSString *URLString = [NSURL URLWithString:kAuthenticationApiKey relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:[parameters copy] success:^(id responseObject) {

        [self.requestSerializer setValue:responseObject[kKeyAppAccessToken] forHTTPHeaderField:kAccessTokenKey];

        if (success) {
            NSError *parserError = nil;
            HHUserResponse *parsedUserResponse = [MTLJSONAdapter modelOfClass:HHUserResponse.class fromJSONDictionary:responseObject error:&parserError];

            // TODO check error

            success(parsedUserResponse);
        }

    } failure:failure];
}

- (NSURLSessionDataTask *)requestUserWithUser:(HHUser *)user
                                      success:(void (^)(HHUserResponse *userResponse))success
                                      failure:(void (^)(NSError *error))failure {

    NSString *URLString = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?webpanel=1", kUsersApiKey, user.uid.stringValue] relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        if (success) {
            NSError *parserError = nil;
            HHUserResponse *parsedUserResponse = [MTLJSONAdapter modelOfClass:HHUserResponse.class fromJSONDictionary:responseObject error:&parserError];

            // TODO check error

            success(parsedUserResponse);
        }

    } failure:failure];
}

- (NSURLSessionDataTask *)requestResetPasswordWithEmail:(NSString *)email
                                      success:(void (^)(id responseObject))success
                                      failure:(void (^)(NSError *error))failure {

    NSDictionary *parameters = @{ @"email": email };
    NSString *URLString = [NSURL URLWithString:kResetPasswordApiKey relativeToURL:self.baseURL].absoluteString;

    return [self hh_POST:URLString parameters:parameters success:^(id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:failure];
}

@end
