//
//  HHAPI+Devices.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/10/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHAPI+Devices.h"
#import "HHUserSettings.h"
#import "HHUser.h"

@implementation HHAPI (Devices)

- (NSURLSessionDataTask *)requestDeviceOwnersWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                 failure:(void (^)(NSError *error))failure {
    NSString *resourceString = [NSString stringWithFormat:@"%@/%@/%@", kUsersApiKey, [HHUserSettings sharedSettings].currentUser.uid.stringValue, kDeviceOwnersApiKey];
    NSString *URLString = [NSURL URLWithString:resourceString relativeToURL:self.baseURL].absoluteString;

    return [self hh_GET:URLString parameters:nil success:^(id responseObject) {

        [HHUserSettings sharedSettings].deviceOwnerArray = responseObject[@"deviceOwners"];

        if (!success) {
            return;
        }

        success(responseObject);

    } failure:failure];
}

@end
