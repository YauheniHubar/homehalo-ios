//
//  HHHTTPSessionManager.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHHTTPSessionManager.h"

@implementation HHHTTPSessionManager

- (NSURLSessionDataTask *)hh_GET:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(id responseObject))success
                         failure:(void (^)(NSError *error))failure {
    return [super GET:URLString parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {

        if (failure) {
            failure(error);
        }

    }];
}

- (NSURLSessionDataTask *)hh_HEAD:(NSString *)URLString
                       parameters:(id)parameters
                          success:(void (^)(id responseObject))success
                          failure:(void (^)(NSError *error))failure {
    return [super HEAD:URLString parameters:parameters success:^(NSURLSessionDataTask *task) {

        if (success) {
            success(nil);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {

        if (failure) {
            failure(error);
        }

    }];
}

- (NSURLSessionDataTask *)hh_POST:(NSString *)URLString
                       parameters:(id)parameters
                          success:(void (^)(id responseObject))success
                          failure:(void (^)(NSError *error))failure {
    return [self POST:URLString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        if (failure) {
            failure(error);
        }

    }];
}

- (NSURLSessionDataTask *)hh_POST:(NSString *)URLString
                       parameters:(id)parameters
        constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                          success:(void (^)(id responseObject))success
                          failure:(void (^)(NSError *error))failure {
    return [super POST:URLString parameters:parameters constructingBodyWithBlock:block progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {

        if (failure) {
            failure(error);
        }

    }];
}

- (NSURLSessionDataTask *)hh_PUT:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(id responseObject))success
                         failure:(void (^)(NSError *error))failure {
    return [super PUT:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {

        if (failure) {
            failure(error);
        }

    }];
}

- (NSURLSessionDataTask *)hh_PATCH:(NSString *)URLString
                        parameters:(id)parameters
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))failure {
    return [super PATCH:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {

        if (failure) {
            failure(error);
        }

    }];
}

- (NSURLSessionDataTask *)hh_DELETE:(NSString *)URLString
                         parameters:(id)parameters
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))failure {
    return [super DELETE:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {

        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {

        if (failure) {
            failure(error);
        }

    }];
}

@end
