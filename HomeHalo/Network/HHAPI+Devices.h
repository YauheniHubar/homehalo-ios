//
//  HHAPI+Devices.h
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/10/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHAPI.h"

@interface HHAPI (Devices)

- (NSURLSessionDataTask *)requestDeviceOwnersWithSuccess:(void (^)(NSDictionary *responseObject))success
                                                 failure:(void (^)(NSError *error))failure;

@end
