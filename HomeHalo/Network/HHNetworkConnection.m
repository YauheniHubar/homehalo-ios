//
//  HHNetworkConnection.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/9/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHNetworkConnection.h"
#import <SystemConfiguration/SystemConfiguration.h>


static HHNetworkConnection *_Instance = nil;
static dispatch_once_t onceTokenPredicate;

@interface HHNetworkConnection()

@end

@implementation HHNetworkConnection

//+ (HHNetworkConnection *)Instance
//{
//    return _Instance;
//}

+ (id)Instance
{
    static id _Instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _Instance = [[[self class] alloc] init];
    });
    
    return _Instance;
}


- (void)cleanDBForCompletionHandler:(void (^)(id response, NSError *error))completionHandler
{
    NSURL *url = [NSURL URLWithString:SERVER_CLEAN];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url
                                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval:TIMEOUT_DEFAULT];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (!error)
         {
             completionHandler(response, nil);
         }
         else
         {
             completionHandler(nil, error);
         }
     }];
}

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self)
    {
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [self setDefaultHeader:@"Accept" value:@"application/json"];
        [self setParameterEncoding:AFJSONParameterEncoding];
        
        NSString *userPath = [NSString stringWithFormat:@"Documents/temp"];
        _tempDataPath = [[NSHomeDirectory() stringByAppendingPathComponent:userPath] retain];
        
        NSFileManager* fileManager = [NSFileManager defaultManager];
        BOOL directory;
        NSError* error;
        if( ![fileManager fileExistsAtPath:_tempDataPath isDirectory:&directory])
        {
            [fileManager createDirectoryAtPath:_tempDataPath
                   withIntermediateDirectories:YES
                                    attributes:nil
                                         error:&error
             ];
        }
    }
    
    return self;
}
//-------------------------------------------------------------------------------------------
- (void)checkUser:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock
{
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    [requestDictionary setObject:CHECK_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:@""//SERVER_URL
           method:GET
       parameters:requestDictionary
  timeOutInterval:TIMEOUT_UPDATE_GAME
          success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
          failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         responseBlock(NO, error, nil);
     }];
    [requestDictionary release];
}

//-------------------------------------------------------------------------------------------
- (void)checkRandom:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock {
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    [requestDictionary setObject:CHECK_RANDOM_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:@""//SERVER_URL
           method:GET
       parameters:requestDictionary
  timeOutInterval:TIMEOUT_UPDATE_GAME
          success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
          failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         responseBlock(NO, error, nil);
     }];
    [requestDictionary release];
}

//-------------------------------------------------------------------------------------------
- (void)loginByEmail:(NSDictionary*)requestInfo withResponse:(ResponseBlock)responseBlock {
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    
    [requestDictionary setObject:LOGIN_TYPE forKey:@"type"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:SOURCE_TYPE_EMAIL forKey:@"source"];
    [requestDictionary setObject: [[PropertyProvider Instance] bwapnsid] forKey:@"bwapnsid"];
    
    [self request:@""
            method:POST
        parameters:requestDictionary
   timeOutInterval:TIMEOUT_LOGIN
           success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
           failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         NSLog(@"ERROR: %@", [error localizedDescription]);
         responseBlock(NO, error, nil);
     }];
    
    [requestDictionary release];
}

- (void)autoLogin:(ResponseBlock)responseBlock
{
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] init];
    
    [requestDictionary setObject:LOGIN_TYPE forKey:@"type"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [self request:@""
           method:POST
       parameters:requestDictionary
  timeOutInterval:TIMEOUT_GAMES
          success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else
         {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
          failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         NSLog(@"ERROR: %@", [error localizedDescription]);
         responseBlock(NO, error, nil);
     }];
    
    [requestDictionary release];
}

//-------------------------------------------------------------------------------------------
- (void)updateGameStatus:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock
{
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    [requestDictionary setObject:UPDATE_GAME_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:SERVER_URL
            method:POST
        parameters:requestDictionary
   timeOutInterval:TIMEOUT_UPDATE_GAME
           success:^(AFHTTPRequestOperation* operation, id response)
           {
//               BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//               isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
               BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
               if(isValid)
                   responseBlock(YES, response, nil);
               else {
                   NSMutableDictionary *details = [NSMutableDictionary dictionary];
                   [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
                   responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
               }
           }
           failure:^(AFHTTPRequestOperation* operation,  NSError *error)
           {
               responseBlock(NO, error, nil);
           }];
}
 
//-------------------------------------------------------------------------------------------
- (void)updateCurrencyStatus:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock
{
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    [requestDictionary setObject:UPDATE_CURRENCY_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:@""//SERVER_URL
            method:POST
        parameters:requestDictionary
   timeOutInterval:TIMEOUT_UPDATE_GAME
           success:^(AFHTTPRequestOperation* operation, id response)
           {
//               BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//               isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
               BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
               if(isValid)
                   responseBlock(YES, response, nil);
               else {
                   NSMutableDictionary *details = [NSMutableDictionary dictionary];
                   [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
                   responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
               }
           }
           failure:^(AFHTTPRequestOperation* operation,  NSError *error)
           {
               responseBlock(NO, error, nil);
           }];
    [requestDictionary release];
}

//-------------------------------------------------------------------------------------------
- (void)stimulate:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock {
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    [requestDictionary setObject:STIMULATE_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:@""//SERVER_PUSH_URL
           method:POST
       parameters:requestDictionary
  timeOutInterval:TIMEOUT_UPDATE_GAME
          success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
          failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         responseBlock(NO, error, nil);
     }];
    [requestDictionary release];
}

//-------------------------------------------------------------------------------------------
- (void)changePassword:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock
{
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    [requestDictionary setObject:CHANGE_PASSWORD_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:@""//SERVER_URL
           method:POST
       parameters:requestDictionary
  timeOutInterval:TIMEOUT_UPDATE_GAME
          success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
          failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         responseBlock(NO, error, nil);
     }];
    [requestDictionary release];
}

//-------------------------------------------------------------------------------------------
- (void)getCategory:(ResponseBlock)responseBlock
{
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] init];
    [requestDictionary setObject:GET_CATEGORY_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:@""//SERVER_URL
           method:GET
       parameters:requestDictionary
  timeOutInterval:TIMEOUT_UPDATE_GAME
          success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
          failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         responseBlock(NO, error, nil);
     }];
    [requestDictionary release];
}
//-------------------------------------------------------------------------------------------
- (void)updateCategory:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock
{
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    [requestDictionary setObject:BUY_CATEGORY_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:@""//SERVER_URL
           method:POST
       parameters:requestDictionary
  timeOutInterval:TIMEOUT_UPDATE_GAME
          success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
          failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         responseBlock(NO, error, nil);
     }];
    [requestDictionary release];
}

//-------------------------------------------------------------------------------------------
- (void)declineGame:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock
{
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    [requestDictionary setObject:DECLINE_GAME_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:SERVER_URL
            method:POST
        parameters:requestDictionary
   timeOutInterval:TIMEOUT_UPDATE_GAME
           success:^(AFHTTPRequestOperation* operation, id response)
           {
//               BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//               isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
               BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
               if(isValid)
                   responseBlock(YES, response, nil);
               else {
                   NSMutableDictionary *details = [NSMutableDictionary dictionary];
                   [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
                   responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
               }
           }
           failure:^(AFHTTPRequestOperation* operation,  NSError *error)
           {
               responseBlock(NO, error, nil);
           }];
}

//-------------------------------------------------------------------------------------------
- (void)setIsIAdShow:(ResponseBlock)responseBlock {
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] init];
    [requestDictionary setObject:IAD_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:@""
           method:POST
       parameters:requestDictionary
  timeOutInterval:TIMEOUT_UPDATE_GAME
          success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
          failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         responseBlock(NO, error, nil);
     }];
}

- (void)getIsIAdShow:(ResponseBlock)responseBlock {
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] init];
    [requestDictionary setObject:IAD_TYPE forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    [self request:@""
           method:GET
       parameters:requestDictionary
  timeOutInterval:TIMEOUT_UPDATE_GAME
          success:^(AFHTTPRequestOperation* operation, id response)
     {
//         BOOL isValid = [[BWValidator Instance] isValidValue:deviceID params:VALIDATE_STRING];
//         isValid = [[BWValidator Instance] isValidValue:appKey params:VALIDATE_STRING];
         BOOL isValid = [[BWValidator Instance] isValidValue:[requestDictionary objectForKey:@"type"] params:VALIDATE_STRING];
         if(isValid)
             responseBlock(YES, response, nil);
         else {
             NSMutableDictionary *details = [NSMutableDictionary dictionary];
             [details setValue:@"uncorrect value" forKey:NSLocalizedDescriptionKey];
             responseBlock(NO, [NSError errorWithDomain:SERVER_URL code:0 userInfo:details], nil);
         }
     }
          failure:^(AFHTTPRequestOperation* operation,  NSError *error)
     {
         responseBlock(NO, error, nil);
     }];
    [requestDictionary release];
}

#pragma mark - Refactor
//-------------------------------------------------------------------------------------------
- (void)requestOfType:(NSString *)type
             withInfo:(NSDictionary *)requestInfo
 forCompletionHandler:(void (^)(NSError *error, id response))completionHandler
{
    NSMutableDictionary *requestDictionary = [NSMutableDictionary dictionaryWithDictionary:requestInfo];
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    [requestDictionary setObject:type forKey:@"type"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
//    NSLog(@"Request FACEBOOK:\n%@", requestDictionary);
    
    [self requestToServer:@""//SERVER_URL
                   method:POST
           withParameters:requestDictionary
          timeoutInterval:TIMEOUT_DEFAULT
                  success:^(AFHTTPRequestOperation *operation, id response)
                  {
                      completionHandler(nil, response);
                  }
                  failure:^(AFHTTPRequestOperation *operation,  NSError *error)
                  {
                      completionHandler(error, nil);
                  }];
}

//-------------------------------------------------------------------------------------------
- (void)logInFacebook:(NSString *)facebookId forCompletionHandler:(void (^)(NSError *error, id response))completionHandler
{
    NSMutableDictionary *requestInfo = [[NSMutableDictionary alloc] init];
    [requestInfo setObject:LOGIN_TYPE forKey:@"type"];
    [requestInfo setObject:SOURCE_TYPE_FACEBOOK forKey:@"source"];
    [requestInfo setObject:[NSString stringWithFormat:@"id%@",facebookId] forKey:@"fbid"];
    [requestInfo setObject: [[PropertyProvider Instance] bwapnsid] forKey:@"bwapnsid"];
    
    [self requestOfType:LOGIN_TYPE
               withInfo:requestInfo
   forCompletionHandler:^(NSError *error_, id response_)
   {
       completionHandler(error_, response_);
   }];
    
   [requestInfo release];
}

//-------------------------------------------------------------------------------------------
- (void)createGameWithInfo:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock
{
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    
    NSMutableURLRequest *request = [self requestWithMethod:@"GET" path:SERVER_URL parameters:requestDictionary];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response_, NSData *soundData_, NSError *error_)
                           {
                               if (!error_)
                                   responseBlock(YES, response_, [NSDictionary dictionaryWithObject:soundData_ forKey:@"soundData"]);
                               else
                               {
                                   responseBlock(NO, error_, nil);
                                   NSLog(@"%@", [error_ localizedDescription]);
                               }
                           }];
    
    [requestDictionary release];
}

- (void)soundFileWithInfo:(NSDictionary *)requestInfo forCompletionHandler:(void (^)(NSURLResponse *response, NSData *soundData))completionHandler
{
    NSMutableDictionary *requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:requestInfo];
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *appKey = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    NSNumber *gameid = [requestInfo objectForKey:@"game_id"];
    [requestDictionary setObject:deviceID forKey:@"bwdevid"];
    [requestDictionary setObject:appKey forKey:@"bwappkey"];
    [requestDictionary setObject:GET_SOUND forKey:@"type"];
    if(gameid != nil)
        [requestDictionary setObject:gameid forKey:@"game_id"];
    
    NSMutableURLRequest *request = [self requestWithMethod:@"GET" path:SERVER_URL parameters:requestDictionary];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response_, NSData *soundData_, NSError *error_)
     {
         if (!error_)
             completionHandler(response_, soundData_);
         else
             NSLog(@"%@", [error_ localizedDescription]);
     }];
    
    [requestDictionary release];
}


//===========================================================================================
- (void)requestToServer:(NSString *)server
                 method:(RequestMethod)method
         withParameters:(NSDictionary *)requestInfo
        timeoutInterval:(NSTimeInterval)timeoutInterval
                success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *urlFull = [SERVER_URL stringByAppendingString:server];
    if([[requestInfo objectForKey:@"type"] isEqualToString:NOTIFICATION_REGISTER]) urlFull = SERVER_PUSH_REGISTER_URL;
    if([[requestInfo objectForKey:@"type"] isEqualToString:STIMULATE_TYPE]) urlFull = SERVER_PUSH_URL;
    
    NSMutableURLRequest *request = nil;
    request = [self requestWithMethod:(method==POST?@"POST":@"GET") path:urlFull parameters:requestInfo];
    [request setMainDocumentURL:[NSURL URLWithString:urlFull]];
    
    if(timeoutInterval>TIMEOUT_DEFAULT)
        [request setTimeoutInterval:timeoutInterval];
    
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    [self enqueueHTTPRequestOperation:operation];
}

- (void)request:(NSString *)server
         method:(RequestMethod)method
     parameters:(NSDictionary *)requestInfo
timeOutInterval:(NSTimeInterval)timeoutInterval
        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [self requestToServer:server
                   method:method
           withParameters:requestInfo
          timeoutInterval:timeoutInterval
                  success:success
                  failure:failure];
}

- (BOOL)ping
{
    bool success = false;
    const char *hostName = [@"google.com"
                               cStringUsingEncoding:NSASCIIStringEncoding];
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL,
    																			hostName);
    SCNetworkReachabilityFlags flags;
    success = SCNetworkReachabilityGetFlags(reachability, &flags);
    bool isAvailable = success && (flags & kSCNetworkFlagsReachable) &&
    !(flags & kSCNetworkFlagsConnectionRequired);
    return isAvailable;
}

- (void)infoByFBID:(NSString *)FDID withResponse:(ResponseBlock)responseBlock {
    NSString *urlFull = [@"https://graph.facebook.com/" stringByAppendingString:FDID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlFull]];
    [request setHTTPMethod:@"GET"];
    AFHTTPRequestOperation *operation = [[[AFHTTPRequestOperation alloc] initWithRequest:request] autorelease];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        [responseString release];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        responseBlock(YES, json, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        responseBlock(NO, error, nil);
    }];
    [operation start];
}

@end
