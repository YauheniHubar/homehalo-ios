//
//  HHJSONResponseSerializer.m
//  HomeHalo
//
//  Created by Dmitry Putintsev on 7/4/16.
//  Copyright © 2016 HomeHalo Ltd. All rights reserved.
//

#import "HHJSONResponseSerializer.h"

static const NSString *kError = @"error";
static const NSString *kCode = @"code";
static const NSString *kMessages = @"messages";

@implementation HHJSONResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error {
    id responseObject = [super responseObjectForResponse:response data:data error:error];

    if (*error) {
        NSInteger httpErrorCode = [(*error).userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
        NSString *errorDomain = @"com.homehalo.app.network.response";
        NSMutableDictionary *errorUserInfo = [NSMutableDictionary new];

        if (responseObject[kError][kMessages]) {
            errorUserInfo[NSLocalizedDescriptionKey] = [responseObject[kError][kMessages] firstObject];
        }

        if (errorUserInfo[NSLocalizedDescriptionKey]) {
            (*error) = [NSError errorWithDomain:errorDomain code:httpErrorCode userInfo:errorUserInfo];
        }

        NSLog(@"ERROR: %@", (*error).localizedDescription);
    }

    return (responseObject);
}

@end
