//
//  HHNetworkConnection.h
//  HomeHalo
//
//  Created by naumov.kirill on 1/9/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^__block ResponseBlock) (BOOL isSuccess, id response, NSDictionary *info);

@interface HHNetworkConnection
{
    @private
    NSString *_tempDataPath;
}

@property (nonatomic, strong, readonly) NSString *deviceUniqueID;
@property (nonatomic, strong, readonly) NSString *appKey;

+ (id)Instance;

//- (void)cleanDBForCompletionHandler:(void (^)(id response, NSError *error))completionHandler;
//- (void)loginByEmail:(NSDictionary*)requestInfo withResponse:(ResponseBlock)responseBlock;
//- (void)autoLogin:(ResponseBlock)responseBlock;
//- (void)updateCurrencyStatus:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock;
//- (void)updateCategory:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock;
//- (void)getCategory:(ResponseBlock)responseBlock;
//- (void)changePassword:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock;
//- (void)stimulate:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock;
//- (void)checkUser:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock;
//- (void)checkRandom:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock;
//
//- (void)setIsIAdShow:(ResponseBlock)responseBlock;
//- (void)getIsIAdShow:(ResponseBlock)responseBlock;
//
//- (void)logInFacebook:(NSString *)facebookId forCompletionHandler:(void (^)(NSError *error, id response))completionHandler;
//- (void)soundFileWithInfo:(NSDictionary *)requestInfo forCompletionHandler:(void (^)(NSURLResponse *response, NSData *soundData))completionHandler;
//- (void)createGameWithInfo:(NSDictionary *)requestInfo withResponse:(ResponseBlock)responseBlock;
//- (void)requestOfType:(NSString *)type withInfo:(NSDictionary *)requestInfo forCompletionHandler:(void (^)(NSError *error, id response))completionHandler;
//- (BOOL)ping;
//
//- (void)infoByFBID:(NSString *)FDID withResponse:(ResponseBlock)responseBlock;

@end

