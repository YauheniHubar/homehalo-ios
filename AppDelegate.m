//
//  AppDelegate.m
//  HomeHalo
//
//  Created by naumov.kirill on 1/5/15.
//  Copyright (c) 2015 HomeHalo Ltd. All rights reserved.
//

#import "HHBaseViewController.h"
#import "HHEnterPinViewController.h"
#import "HHUserSettings.h"
#import "GAI.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Branch/Branch.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "HHAPI.h"

static NSString *const kStoryboardIdSignUpViewController = @"HHSignUpViewController";
static NSString *const kStoryboardIdEnterPinViewController = @"HHEnterPinViewController";
static NSString *const kStoryboardIdHomeViewController = @"kHomeSegue";
static NSString *const kAlertsSegue = @"kAlertsSegue";

@interface AppDelegate ()

@property (nonatomic) UINavigationController *navigationController;
@property (nonatomic) HHUserSettings *userSettings;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];

//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;

    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;

    // Optional: set Logger to VERBOSE for debug information.
    [GAI sharedInstance].logger.logLevel = kGAILogLevelNone;

    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-55373081-3"];

    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // params are the deep linked params associated with the link that the user clicked before showing up.
        NSLog(@"deep link data: %@", params.description);
    }];

    [HHUserSettings sharedSettings].openAlerts = NO;
    NSString *accessToken = [HHUserSettings sharedSettings].accessToken;

    if (accessToken) {
        [[HHAPI sharedAPI].requestSerializer setValue:accessToken forHTTPHeaderField:kAccessTokenKey];
    }

    if ([HHUserSettings sharedSettings].currentUser) {
        if ([HHUserSettings sharedSettings].isDemoMode) {
            [self goToHomeScreen];
        } else {
            [self goToEnterPinScreen];
        }
    } else {
        [self goToSignUpScreen];
    }
    
    [Fabric with:@[ Crashlytics.class, Branch.class ]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performLogout) name:HHLogoutNotification object:nil];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [MBProgressHUD showHUDAddedTo:self.window animated:YES];
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    if (![HHUserSettings sharedSettings].correctPIN) return;
    
    if (![(self.navigationController.viewControllers).lastObject isKindOfClass:[HHEnterPinViewController class]]) {
        NSString *segueID = IS_IPHONE_4 ? @"HHEnterPinViewController4" : kStoryboardIdEnterPinViewController;
        HHEnterPinViewController *vc = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:segueID];
        vc.isReturnToPrevVC = YES;
        [self.navigationController pushViewController:vc animated:NO];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    if (userInfo[@"notification_id"]) {
        [HHUserSettings sharedSettings].openAlerts = YES;
    }
    
    if (userInfo[@"aps"]) {
        if ((userInfo[@"aps"])[@"badgecount"]) {
            [UIApplication sharedApplication].applicationIconBadgeNumber = [(userInfo[@"aps"])[@"badgecount"] intValue];
        }
    }
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *str = [NSString stringWithFormat:@"deviceToken %@",deviceToken];
    NSLog(@"%@", str);
    
    const unsigned *tokenBytes = deviceToken.bytes;
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    
    NSLog(@"%@", hexToken);
    [HHUserSettings sharedSettings].deviceToken = hexToken;
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSString *str = [NSString stringWithFormat:@"Error: %@", err];
    NSLog(@"%@", str);
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[Branch getInstance] handleDeepLink:url];
}


#pragma mark - Private

- (void)performLogoutWithErrorString:(NSString *)errorString {
    [self performLogout];
    
    if (errorString) {
        [HHUtils alertErrorMessage:errorString];
    }
}

- (void)performLogout {
    self.userSettings.correctPIN = nil;
    self.userSettings.accessToken = nil;
    self.userSettings.currentUser = nil;
    self.userSettings.deviceOwnerArray = nil;
    self.userSettings.currentDeviceOwner = nil;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[HHAPI sharedAPI].requestSerializer setValue:nil forHTTPHeaderField:kAccessTokenKey];
    
    [self goToSignUpScreen];
}

- (void)goToEnterPinScreen {
    NSString *identifier = IS_IPHONE_4 ? @"HHEnterPinViewController4" : kStoryboardIdEnterPinViewController;
    [self gotoViewControllerWithIdentifier:identifier];
}

- (void)goToSignUpScreen {
    [self gotoViewControllerWithIdentifier:kStoryboardIdSignUpViewController];
}

- (void)goToHomeScreen {
    [self gotoViewControllerWithIdentifier:kStoryboardIdHomeViewController];
}

- (void)gotoViewControllerWithIdentifier:(NSString *)controllerIdentifier {
    UIViewController *requiredController = [[HHUtils mainStoryboard] instantiateViewControllerWithIdentifier:controllerIdentifier];
    
    if ([self checkNavigationControllerHasRootController:requiredController]) {
        [_navigationController popToRootViewControllerAnimated:YES];
    } else  {
        [self resetNavigationControllerWithRootViewController:requiredController];
    }
}

- (BOOL)checkNavigationControllerHasRootController:(UIViewController *)controller {
    if (controller && _navigationController.viewControllers.count > 0) {
        UIViewController *vc = (_navigationController.viewControllers).firstObject;
        
        if ([vc isKindOfClass:controller.class]) {
            return YES;
        }
    }
    
    return NO;
}

- (void)resetNavigationControllerWithRootViewController:(UIViewController *)rootViewController {
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    self.window.rootViewController = _navigationController;
}

@end
